package PP;

import java.util.Scanner;

public class PP1 {
	static Scanner scanner=new Scanner(System.in);
	static String [][]users=new String[10][4];
	static String book[][]=new String[10][6];
	static String chubanshe[][]=new String[10][3];
	public static void main(String[] args) {
		System.out.println("欢迎来到闽大图书管理系统！");
		System.out.println("请选择： 1，登录    2，注册");
		//采用二维数组存放用户信息（部门、用户名、密码、用户角色）,
	
		
        users[0][0]= "软件部";
        users[0][1]="pp1";
        users[0][2]="1234561";
        users[0][3]="管理员";
        
        users[1][0]= "软件部";
        users[1][1]="pp2";
        users[1][2]="1234562";
        users[1][3]="普通员工";
        
        users[2][0]= "软件部";
        users[2][1]="pp3";
        users[2][2]="1234563";
        users[2][3]="普通员工";
        //采用二维数组存放书籍信息（编码（ISBN）、书籍名称、出版社、作者,价格）
        book[0][0]="97871010";
        book[0][1]="史记";
        book[0][2]="中华书局";
        book[0][3]="司马迁";
        book[0][4]="125.0";
        		
        book[1][0]="97875063";
        book[1][1]="活着";
        book[1][2]="作家出版社";
        book[1][3]="余华";
        book[1][4]="20.0";		
        
        book[2][0]="97872290";
        book[2][1]="三体全集";
        book[2][2]="重庆出版社";
        book[2][3]="刘慈欣";
        book[2][4]=" 168.0";	
        
        book[3][0]="97875442";
        book[3][1]="白夜行";
        book[3][2]="南海出版公司";
        book[3][3]="[日]东野圭吾";
        book[3][4]=" 39.5";	
        
        //采用二位数组存放出版社信息（出版社名称、地址、联系人）
        chubanshe[0][0]="中华书局";
        chubanshe[0][1]="北京市王府井大街36号";
        chubanshe[0][2]="张三";
        
        chubanshe[1][0]="作家出版社";
        chubanshe[1][1]="香港九龙荷李活商业中心8楼";
        chubanshe[1][2]="李四";
        
        chubanshe[2][0]="重庆出版社";
        chubanshe[2][1]="茶园新区开拓路6号";
        chubanshe[2][2]="王五";
        
        chubanshe[3][0]="南海出版公司";
        chubanshe[3][1]="海秀中路51-1号星华大厦5楼";
        chubanshe[3][2]="周毅";
        		
        		
        		
        int key=scanner.nextInt();
        switch (key) {
		case 1:
			//登录
			denglu();
		  
			break;
        case 2:
			//注册       部门、用户名、密码、用户角色
        	zhuce();
        	
        	System.out.println("登录");
        	tiaozhuanxitong();
        	
			break;
			
		default:
			break;
		}
}
	private static void tiaozhuanxitong() {
		System.out.println("请输入你的用户名：");
		String userName=scanner.next();
		
		System.out.println("请输入你的密码：");
		String password=scanner.next();
		
		boolean flag=denglupanduan(userName,password);
		
		if (flag) {
			System.out.println("登录成功！欢迎使用！");
	        xitong();
			
		}else {
			System.out.println("该用户不存在或者密码错误！请重新登录！");
			denglu();
		}
		
		}
	
	private static void xitong() {
		System.out.println("进入系统菜单，请选择：“1 图书管理     2 出版社管理      3 退出     4,退出登录");
		int kk=scanner.nextInt();
		switch (kk) {
		case 1: {
			//图书管理
			tushuguanli();
			
		}
        case 2: {
			//出版社管理
        	chubansheguanli();
			break;
		}
        case 3: {
			//退出
        	System.out.println("系统退出！再见！");
        	System.exit(0);
        	
			break;
		}
        case 4:{
        	//退出登录
        	System.out.println("已重新回到登录页面：");
        	denglu();
        	break;
        }

		default:
		break;	
		}
		
	}
	
	private static void chubansheguanli() {
		 //1	增加
	     //2	删除：出版社有关联的图书不能删除；
	     //3	更新 
	     //4	根据出版社名称查询
	     //5	查询所有出版社
	     //6	返回上一级菜单
		//出版社信息（出版社名称、地址、联系人）
		System.out.println("进入出版社管理");
		System.out.println("请选择：1,增加  2，删除  3，更新  4，查询   5，返回上一级菜单");
		int ss=scanner.nextInt();
		switch (ss) {
		case 1:
			//增加
			System.out.println("增加");
			chubanshezengjia();
			break;
        case 2:	
			//删除
        	System.out.println("删除");
        	chubansheshanchu();
			break;
        case 3:
        	//更新
        	System.out.println("更新");
        	chubanshegengxin();
	        break;
        case 4:
	  //根据出版社名称查询	  查询所有出版社
        	System.out.println("查询");
        	chubanchaxun();
	        break;
        case 5:
        	//返回上一级菜单
        	System.out.println("返回上一级菜单");
        	xitong();
	        break;

		default:
			break;
		}

		
	}
	private static void chubanshegengxin() {
		System.out.print("出版社名称"+"\t"+"地址"+"\t"+"联系人");//出版社名称、地址、联系人
		System.out.println("  ");
		for (int i = 0; i < book.length; i++) {
                if (book[i][0]!=null) {
	    System.out.println(chubanshe[i][0]+"\t"+chubanshe[i][1]+"\t"+chubanshe[i][2]);
                }
		}
		System.out.println("请输入要更改的出版社名称：");
		String ming=scanner.next();
		
		int index=-1;
		for (int i = 0; i < book.length; i++) {
			index=i;
			break;
		}
		
		System.out.println("出版社名称为：");
		chubanshe[index][0]=scanner.next();
		System.out.println("地址：");
		chubanshe[index][1]=scanner.next();
		System.out.println("联系人：");
		chubanshe[index][2]=scanner.next();
		
		System.out.println("更新成功！");
		chubansheguanli();
	}
	private static void chubansheshanchu() {
		System.out.println("请输入要删除的出版社名称：");
		String chubanmingchen=scanner.next();
		int index=-1;
		for (int i = 0; i < chubanshe.length; i++) {
			if (chubanmingchen.equals(chubanshe[i][0])) {
				index=i;
				chubanshe[index][0]=null;
				chubanshe[index][1]=null;
				chubanshe[index][2]=null;
	
				System.out.println("删除成功！");
				break;
			}
		}
		chubansheguanli();
		
		
	}

	private static void chubanchaxun() {
		System.out.println("请选择：1，根据出版社名称查询    2，查询所有    3,返回上一级菜单（出版社管理）");
		int xx=scanner.nextInt();
		switch (xx) {
		case 1:
			//根据出版社名称查询
			chubanshemingchengchaxun();
			break;
        case 2:
			//查询所有
        	chubanshechaxunsuoyou();
			break;
        case 3:
        	//返回上一级菜单
        	chubansheguanli();
        	break;
		default:
			break;
		}
	}
	private static void chubanshechaxunsuoyou() {
		System.out.println("进入查询所有出版社");
		System.out.println("出版社名称"+"\t"+"\t"+"地址"+"\t"+"联系人"+"\t");
		for (int i = 0; i < book.length; i++) {
			if (chubanshe[i][0]!=null) {
		    System.out.println(chubanshe[i][0]+"\t"+chubanshe[i][1]+"\t"+chubanshe[i][2]);
		    
			}
		}
	
		chubanchaxun();
	}
	private static void chubanshemingchengchaxun() {//出版社名称、地址、联系人
		System.out.println("进入出版社名称查询");
		System.out.println("请输入出版社名称：");
		String chubanshemingcheng=scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (chubanshemingcheng.equals(chubanshe[i][0])) {
				System.out.println("出版社名称"+"\t"+"\t"+"地址"+"\t"+"联系人"+"\t");
				System.out.println(chubanshe[i][0]+"\t"+chubanshe[i][1]+"\t"+chubanshe[i][2]);
				
			}
			chubanchaxun();
		}
		
		
	}
	private static void chubanshezengjia() {
		int index=-1;
		for (int i = 0; i < book.length; i++) {
			if (chubanshe[i][0]==null) {
				index=i;
				break;
			}
			
		}
		System.out.println("请输入添加的出版社：");
		chubanshe[index][0]=scanner.next();
		
		System.out.println("添加成功！");
		chubansheguanli();
				
	}
	private static void tushuguanli() {
		System.out.println("进入图书管理");
		System.out.println("请选择：1,增加  2，删除  3，更新  4，查询  5，返回上一级菜单");
		while (true) {
			
		
		int zz=scanner.nextInt();
		switch (zz) {
		case 1: {
			//增加
			System.out.println("增加：");
		    zengjia();
		}
        case 2: {
			//删除
        	System.out.println("删除：");
            shanchu();
		}
        case 3: {
        	//更新
        	System.out.println("更新：");
            gengxin();
        break;
       }
       case 4: {
    	   //查询  
    	   //a	根据ISBN查询
    	   //b	根据书名查询（模糊）
           //c	根据出版社查询
           //d	根据作者查询
           //e	根据价格范围查询
           //f	查询所有书籍信息
           //g	返回上一级菜单
    	   chaxun();
    	   break;
       }
       case 5: {
    	   //返回上一级菜单
    	   
    	   xitong();
       }
		default:
		
		}
	}
	}
	private static void chaxun() {
		 System.out.println("查询：");
		 System.out.println("选择查询方式：1,ISBN查询         2，书名查询（模糊）        3，出版社查询          4，作者查询          5，价格范围查询          6，查询所有         7，返回上一级菜单"); 

  	   int cc=scanner.nextInt();
  	   switch (cc) {
		case 1:
			//根据ISBN查询
			isbnchaxun();
			break;
        case 2:
			//根据书名查询（模糊）
      	    shumingchaxun();
			break;
		case 3:
			//根据出版社查询
			chubanshechaxun();
			break;
		case 4:
			//根据作者查询
			zuozhechaxun();
			break;
		case 5:
			//根据价格范围查询
			jiagechaxun();			
			break;
      case 6:
			//查询所有书籍信息
      	    suoyoushujichaxun();
			break;
      case 7:
	        //返回上一级菜单
    	    tushuguanli();
	        break;
		default:
			break;
		}
	}
	private static void suoyoushujichaxun() {
		System.out.println("所有书籍记录：");
		System.out.print("编号（isbn）"+"\t");
		System.out.print("书籍名称"+"\t");
		System.out.print("出版社"+"\t");
		System.out.print("作者"+"\t");
		System.out.print("价格"+"\t");
		System.out.println(" ");
	
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]!=null) {
			System.out.print(book[i][0]+"\t");
			System.out.print(book[i][1]+"\t");
			System.out.print(book[i][2]+"\t");
			System.out.print(book[i][3]+"\t");
			System.out.print(book[i][4]+"\t");
			System.out.println(" ");
			
			}
		}
		chaxun();
	}
	private static void jiagechaxun() {
		System.out.println("进入价格查询");
		System.out.println("请输入价格：");
		String jiage=scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (jiage.equals(book[i][4])) {
				System.out.print("isbn"+"\t"+"\t");
				System.out.println("书籍名称"+"\t");
				System.out.print("出版社"+"\t");
				System.out.print("作者"+"\t");
				System.out.print("价格"+"\t");
				System.out.println(" ");
			System.out.println(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
			System.out.println(" ");
			chaxun();
			}
		}
	}
	private static void zuozhechaxun() {
		System.out.println("进入作者查询");
		System.out.println("请输入作者：");
		String zuozhe=scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (zuozhe.equals(book[i][3])) {
				System.out.print("isbn"+"\t"+"\t");
				System.out.println("书籍名称"+"\t");
				System.out.print("出版社"+"\t");
				System.out.print("作者"+"\t");
				System.out.print("价格"+"\t");
				System.out.println(" ");
			System.out.println(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
			System.out.println(" ");
			chaxun();
			}
		}
	}
	private static void shumingchaxun() {
		System.out.println("进入书名查询");
		System.out.println("请输入书名：");
		String shuming=scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (book[i][1].indexOf(shuming)!=-1) {
				System.out.print("isbn"+"\t"+"\t");
				System.out.print("书籍名称"+"\t");
				System.out.print("出版社"+"\t"+"\t");
				System.out.print("作者"+"\t");
				System.out.print("价格"+"\t");
				System.out.println(" ");
			System.out.println(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
			System.out.println(" ");
			chaxun();
			}
		}
	}
	private static void chubanshechaxun() {
		System.out.println("进入出版社查询");
		System.out.println("请输入出版社：");
		String chubansheString=scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (chubansheString.equals(book[i][2])) {
				System.out.print("isbn"+"\t"+"\t");
				System.out.println("书籍名称"+"\t");
				System.out.print("出版社"+"\t");
				System.out.print("作者"+"\t");
				System.out.print("价格"+"\t");
				System.out.println(" ");
				System.out.println(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
			System.out.println(" ");
			chaxun();
			}
		}
		
	}
	private static void isbnchaxun() {
		System.out.println("进入isbn查询");
		System.out.println("请输入isnb：");
		String isnb=scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (isnb.equals(book[i][0])) {
				System.out.print("isbn"+"\t"+"\t");
				System.out.println("书籍名称"+"\t");
				System.out.print("出版社"+"\t");
				System.out.print("作者"+"\t");
				System.out.print("价格"+"\t");
				System.out.println(" ");
				System.out.println(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
			
			System.out.println(" ");
			chaxun();
			}
		}
		
	}
	private static void gengxin() {
//编码（ISBN）唯一 ，如果存在提示“图书已存在”；不存在才进行新增书籍信息、书籍名称、出版社、作者，价格
//查询所有书籍信息
		System.out.println("所有书籍记录：");
		System.out.print("编号（isbn）"+"\t");
		System.out.print("书籍名称"+"\t");
		System.out.print("出版社"+"\t");
		System.out.print("作者"+"\t");
		System.out.print("价格"+"\t");
		System.out.println(" ");
	
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]!=null) {
			System.out.print(book[i][0]+"\t");
			System.out.print(book[i][1]+"\t");
			System.out.print(book[i][2]+"\t");
			System.out.print(book[i][3]+"\t");
			System.out.print(book[i][4]+"\t");
			System.out.println(" ");
			}
		}
		System.out.println("输入要更改的isbn:");
		String isbn=scanner.next();
		int index=-1;
		for (int i = 0; i < book.length; i++) {
			if (isbn.equals(book[i][0])) {
				index=i;
				break;
			}
		}
		System.out.println("请输入书籍名称：");
		book[index][1]=scanner.next();
		System.out.println("请输入出版社：");
		book[index][2]=scanner.next();
		System.out.println("请输入作者：");
		book[index][3]=scanner.next();
		System.out.println("请输入价格：");
		book[index][4]=scanner.next();
	
		System.out.println("更新成功！");
		tushuguanli();
	}
	private static void shanchu() {
		System.out.println("请输入isbn:");
    	String isbn=scanner.next();
		int index=-1;
		for (int i = 0; i < book.length; i++) {
			if (isbn.equals(book[i][0])) {
				index=i;
				book[index][0]=null;
				book[index][1]=null;
				book[index][2]=null;
				book[index][3]=null;
				book[index][4]=null;
				System.out.println("删除成功！");
				break;
			}
		}
		tushuguanli();
	}
	
	
	private static void zengjia() {//编码（ISBN）唯一 ，如果存在提示“图书已存在”；不存在才进行新增书籍信息、书籍名称、出版社、作者，价格
		int index=-1;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]==null) {
				index=i;
				break;
			}
		}
	
		System.out.println("输入isbn：");
		book[index][0]=scanner.next();
        System.out.println("输入书名：");
        book[index][1]=scanner.next();
		System.out.println("输入出版社：");
		book[index][2]=scanner.next();
		System.out.println("输入作者：");
		book[index][3]=scanner.next();
		System.out.println("输入价格范围：");
		book[index][4]=scanner.next();
		System.out.println("添加成功！");
		tushuguanli();
		}
		
	
	private static void zhuce() {
		System.out.println("请输入部门：");
    	String Depart=scanner.next();
    	
    	System.out.println("请输入用户名：");
    	String userName=scanner.next();
    	
    	System.out.println("请输入密码：");
    	String password=scanner.next();
    	
    	System.out.println("请输入用户角色：");
    	String Character=scanner.next();
   for (int i = 0; i < users.length; i++) {
	if (users[i][0]==null) {
		users[i][0] = Depart;
		users[i][1] = userName;
		users[i][2] = password;
		users[i][3] =Character;
	}
}
	}

	private static void denglu() {
		for (int i = 0; i <3; i++) {
			
			System.out.println("请输入你的用户名：");
			String userName=scanner.next();
			
			System.out.println("请输入你的密码：");
			String password=scanner.next();
			
			boolean flag=denglupanduan(userName,password);
			
			if (flag) {
				System.out.println("登录成功！欢迎使用！");
	            xitong();
			
				break;
				
			}else {
				System.out.println("该用户不存在或者密码错误！请重新登录！");
			}
			
			}
		
	}

	private static boolean denglupanduan(String userName, String password) {
         boolean flag=false;
         for (int j = 0; j < users.length; j++) {
         if (userName.equals(users[j][1])&&password.equals(users[j][2])) {
			flag=true;
			break;
		}
	}
         return flag;
	}
}
