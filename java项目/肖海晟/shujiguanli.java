package com.ma.lianxi;

import java.util.Scanner;

public class shujiguanli {
	static String arr [][] = new String [20][4];
	static Scanner scanner = new Scanner (System.in);
	public static void main(String[] args) {
		System.out.println("书籍管理系统");
		System.out.println("1.登录 2.注册");
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			loginView();
			break;
		case 2:
			register();
			break;
		default:
			break;
		}
	}
	public static void loginView() {
		System.out.println("请输入用户名：");
		String userName = scanner.next();
		System.out.println("请输入密码：");
		String password = scanner.next();
		info();
		boolean a = login(userName,password);
		if (a) {
			System.out.println("登录成功");
			system();
		}else {
			System.out.println("登录失败");
		}
	}
	public static void register() {
		System.out.println("请输入所属部门：");
		String department = scanner.next();
		System.out.println("请输入用户名：");
		String userName = scanner.next();
		System.out.println("请输入密码：");
		String password = scanner.next();
		System.out.println("请输入角色：");
		String role = scanner.next();
		for (int i = 0; i < arr.length; i++) {
			arr[i][0] = department;
			arr[i][1] = userName;
			arr[i][2] = password;
			arr[i][3] = role;
			System.out.println("注册成功");
			loginView();
		}
	}
	public static boolean login(String userName , String password) {
		boolean a = false ;
		for (int i = 0; i < arr.length; i++) {
			if (userName.equals(arr[i][1])&&password.equals(arr[i][2])) {
				a = true;
			}
		}
		return a ;
	}
	public static void info() {
		arr[0][0] = "科技部";
		arr[0][1] = "123";
		arr[0][2] = "123";
		arr[0][3] = "员工";
	}
	public static void system() {
		System.out.println("1.图书管理 2.出版社管理 3.退出登录 4.退出系统");
		int sys = scanner.nextInt();
		switch (sys) {
		case 1:
			management();
			break;
		case 2:
			publishing();
			break;
		case 3:
			
			break;
		case 4:
	
			break;
		default:
			break;
		}
	}
	public static void management() {
		System.out.println("1.增加 2.删除 3.更新 4.查询菜单 5.返回上一级菜单");
		int man = scanner.nextInt();
		switch (man) {
		case 1:
			
			break;
		case 2:
			
			break;
		case 3:
			
			break;
		case 4:
			inquire();
			break;
		case 5:
			
			break;
		default:
			break;
		}
	}
	public static void inquire() {
		System.out.println("a.根据ISBN查询 b.根据书名查询（模糊） c.根据出版社查询 d.根据作者查询 e.根据价格范围查询 f.查询所有书籍信息 g.返回上一级菜单");
		int inq = scanner.nextInt();
		switch (inq) {
		case 1:
			
			break;
		case 2:
			
			break;
		case 3:
			
			break;
		case 4:
			
			break;
		case 5:
			
			break;
		case 6:
			
			break;
		case 7:
			
			break;
		default:
			break;
		}
	}
	public static void publishing () {
		System.out.println("1.增加 2.删除：出版社有关联的图书不能删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int pub = scanner.nextInt();
		switch (pub) {
		case 1:
			
			break;
		case 2:
			
			break;
		case 3:
			
			break;
		case 4:
			
			break;
		case 5:
			
			break;
		case 6:
				
			break;
				
		default:
			break;
		}
	}
}
