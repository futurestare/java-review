import java.util.Scanner;

public class Library {
	static String users[][] = new String[20][4];
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		System.out.println("欢迎使用闽大图书管理系统!");
		System.out.println("1.登录     2.注册");
		users();
		int key = sc.nextInt();
		switch (key) {
		case 1:
			loginview();
			break;
		case 2:
			register();
			break;

		default:
			break;
		}
	}
	public static void register() {
		System.out.println("请输入所属部门");
		String Depart = sc.next();
		System.out.println("请输入用户名");
		String Username = sc.next();
		System.out.println("请输入密码");
		String Password = sc.next();
		System.out.println("请输入用户角色");
		String Character = sc.next();
		int index = getFirstNullUserIndex();
		for (int i = index; i < users.length; i++) {
			users[i][0]=Depart;
			users[i][1]=Username;
			users[i][2]=Password;
			users[i][3]=Character;
			break;
		}
		System.out.println("注册成功！");
		System.out.println("欢迎使用闽大图书管理系统!");
		System.out.println("1.登录     2.注册");
		int key = sc.nextInt();
		switch (key) {
		case 1:
			loginview();
			break;
		case 2:
			register();
			break;
		default:
			break;
		}
	}
	private static int getFirstNullUserIndex() {
		int index = -1;
		for (int i = 0; i < users.length; i++) {
			if (users[i][0]==null) {
				index = i;
			}
		}
		return index;
	}
	public static void users() {
		
		users[0][0]="软件部";
		users[0][1]="admin";
		users[0][2]="123";
		users[0][3]="管理员";

	}
	public static boolean login(String Username,String Password) {
		boolean flag = false;
		for (int i = 0; i < 3; i++) {
			if (Username.equals(users[i][1])&&Password.equals(users[i][2])) {
				flag = true;
			}
		}
		return flag;
	}
	public static void loginview() {
		System.out.println("请输入用户名");
		String Username = sc.next();
		System.out.println("请输入密码");
		String Password = sc.next();
		boolean flag = login(Username,Password);
		if (flag) {
			System.out.println(Username+"登录成功! 欢迎使用闽大书籍管理系统！");
			menu();
		}else {
			System.out.println("输入错误，请重新输入");
			loginview();
		}
	}
}
