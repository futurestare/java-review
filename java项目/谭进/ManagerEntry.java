package com.day.demo;

import java.util.Arrays;
import java.util.Scanner;

public class ManagerEntry {
	static String loginUser="";
	static Scanner sc = new Scanner(System.in);
//	用户信息（部门、用户名、密码、用户角色）
	static String[][] users = new String[5][4];
//	书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）
	static String[][] books = new String[5][5];
//	出版社信息（出版社名称、地址、联系人）
	static String[][] press = new String[5][3];

	public static void main(String[] args) {
		setup();
//		System.out.println(Arrays.toString(users[0]));
		lo: while (true) {

			System.out.println("欢迎使用 图书管理系统！");
			System.out.println("1.登录\t2.注册\t3.退出");

			String choice = sc.next();
			switch (choice) {
			case "1":
//				System.out.println("登录");
				login();
				break;
			case "2":
				System.out.println("注册");
				break;
			case "3":
				System.out.println("感谢您的使用，再见！");
				break lo;
			default:
				System.out.println("输入错误，请重新输入！");
				break;
			}
		}
	}

	private static void login() {
		String userName;
		while (true) {
			System.out.println("请输入用户名");
			userName = sc.next();
			boolean flag = isExists(userName);
			if (flag) {
				break;
			} else {
				System.out.println("用户名不存在，请重新输入！");
			}
		}
		int Index = getIndex(userName);
		String userPassword;
		for (int i = 1; i <= 3; i++) {
			System.out.println("请输入密码，您还有" + (4 - i) + "次机会");
			userPassword = sc.next();
			if (userPassword.equals(users[Index][2])) {
				System.out.println("登录成功！");
				loginUser=userName;
				menu();
				return;
			} else {
				System.out.println("密码错误，请重新输入！");
			}
		}
		System.out.println("错误次数过多，系统自动退出，再见！-_-");
		System.exit(0);
	}

	private static void menu() {
		lo:while (true) {
			System.out.println(loginUser+"欢迎您使用图书管理系统");
			System.out.println("1.图书管理\t2. 出版社管理\t3. 退出系统");
			String choice=sc.next();
			switch (choice) {
			case "1":
				System.out.println("图书管理");
				break;
			case "2":
				System.out.println("出版社管理");
				break;
			case "3":
				System.out.println("感谢您的使用，再见！");
				break lo;
			default:
				System.out.println("输入错误请重新输入");
				break;
			}
		}
	}

	private static int getIndex(String userName) {
		int index = -1;
		for (int i = 0; i < users.length; i++) {
			if (users[i][1] != null && userName.equals(users[i][1])) {
				index = i;
				break;
			}
		}
		return index;
	}

	private static boolean isExists(String userName) {
		// TODO Auto-generated method stub
		boolean flag = false;
		for (int i = 0; i < users.length; i++) {
			if (users[i][0] != null && users[i][1].equals(userName)) {
				flag = true;
				break;
			}
		}
		return flag;
	}

//	初始化用户信息、出版社信息、书籍信息；（设置基础信息，赋值；）
	private static void setup() {
		users[0][0] = "Test";
		users[0][1] = "admin";
		users[0][2] = "123";
		users[0][3] = "admin";

		books[0][0] = "12345678";
		books[0][1] = "Java基础";
		books[0][2] = "9.9";
		books[0][3] = "闽大出版社";
		books[0][4] = "大老";

		press[0][0] = "闽大出版社";
		press[0][1] = "闽大门口";
		press[0][2] = "大老";
	}

}
