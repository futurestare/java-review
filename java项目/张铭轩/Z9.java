package zmx;

import java.util.Scanner;

public class Z9 {

	static String[][] user = new String[100][4];
	static String[][] book = new String[100][5];
	static String[][] press = new String[100][3];

	static Scanner sc = new Scanner(System.in);
	static String loginuser = " ";

	public static void main(String[] args) {
		users();
		System.out.println("        图书管理系统");
		System.out.println("1.登录" + "\t" + "2.注册");

		int key = sc.nextInt();
		switch (key) {
		case 1:
			login();
			break;
		case 2:
			registr();
			break;

		default:
			System.out.println("系统退出！");
			System.exit(0);
			break;
		}
	}

	private static boolean login() {
		boolean logFlag = false;
		while (!logFlag) {
			System.out.println("用户名");
			String userName = sc.next();
			System.out.println("密码");
			String password = sc.next();
			
			boolean result = loginCheck(userName,password);
			if (result) {
				System.out.println("登录成功");
				homepage();
				menu();
				logFlag = true;
			}else {
				System.out.println("登录失败");
			}
		}
		return false;
	}

	private static boolean loginCheck(String userName, String password) {
		boolean logFlag = false;
		for (int i = 0; i < user.length; i++) {
			if (userName.equals(user[i][1]) && password.equals(user[i][2])) {
				logFlag=true;
				break;
			}
		}
		return logFlag;
	}

	private static void registr() {

		while (true) {
			System.out.println("请输入部门");
			String departName = sc.next();
			System.out.println("请输入用户名");
			String userName = sc.next();
			System.out.println("请输入密码");
			String password = sc.next();
			System.out.println("确认密码");
			String password1 = sc.next();
			for (int i = 0; i < user.length; i++) {
				if (user[i][1] == null) {
					user[i][0] = departName;
					user[i][1] = userName;
					user[i][2] = password;
				}
			}
			if (password.equals(password1)) {
				System.out.println("注册成功");
				login();
				
				break;
			} else {
				System.out.println("两次密码不一致，请重新登录");
			}
		}
	}
	private static void homepage() {
		System.out.println("欢迎 " + loginuser + "欢迎来到图书管理系统");

	}

	private static void users() {

		user[0][0] = "技术部";
		user[0][1] = "admin";
		user[0][2] = "123";
		user[0][3] = "管理员";

		user[1][0] = "技术部";
		user[1][1] = "as";
		user[1][2] = "123";
		user[1][3] = "管理";

	}
	public static void menu() {
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		int choose= sc.nextInt();
		switch (choose) {
		case 1:
			bookmanage();
			break;
        case 2:
			press();
			break;
        case 3:
        	main(null);
	break;
          case 4:
	     System.out.println("退出系统");
	    System.exit(0);
	
	break;

		default:
			break;
		}
		
	}
	private static void press() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int k=sc.nextInt();
		switch (k) {
		case 1:
			pressadd();
			break;
		case 2:
			pressdelete();
			break;
		case 3:
			pressupdate();
			break;
		case 4:
			pressnameinquire();
			break;
		case 5:
			pressall();
			break;
		case 6:
			menu();
			break;
			

		default:
			break;
		}
		
	}
	private static void pressall() {
		System.out.println("出版社名称"+'\t'+"地址"+'\t'+"姓名");
		for (int i = 0; i < book.length; i++) {
			if (press[i][0]!=null) {
				System.out.println("出版社名称"+press[i][0]+"地址"+'\t'+press[i][1]+"姓名"+press[i][2]);
			}
		}
		press();
	}

	private static void pressnameinquire() {
		System.out.println("请输入出版社名称");
		String pressname= sc.next();
		for (int i = 0; i < book.length; i++) {
			if (pressname.equals(press[i][0])) {
				System.out.println("出版社名称："+"press[i][0]");
				System.out.println("出版社名称："+"press[i][1]");
				System.out.println("出版社名称："+"press[i][2]");
			}
		}
		System.out.println("输入有误");
		press();
	}

	public static void pressupdate() {
		System.out.println("请输入出版社名称：");
		String a = sc.next();
		for (int i = 0; i < press.length; i++) {
			if(a.equals(press[i][0])) {
				System.out.println("出版社名称"+'\t'+"地址"+'\t'+"联系人");
				System.out.println(press[i][0]+'\t'+press[i][1]+'\t'+press[i][2]);
				System.out.println("请输入要更新的出版社名称：");
				String pressname = sc.next();
				System.out.println("请输入要更新的地址：");
				String pressaddress = sc.next();
				System.out.println("请输入要更新的联系人姓名：");
				String presslinkman = sc.next();
				System.out.println("更新完成！");
				press[i][0] = pressname;
				press[i][1] = pressaddress;
				press[i][2] = presslinkman;
				System.out.println("出版社名称"+'\t'+"地址"+'\t'+"联系人");
				System.out.println(press[i][0]+'\t'+press[i][1]+'\t'+press[i][2]);
				press();
			}
		}
		System.out.println("你是输入有误！请重新输入！");
		press();
	}
	private static void pressdelete() {
		System.out.println("请输入要删除的出版社名称：");
		String pressname = sc.next();
		for (int i = 0; i < book.length; i++) {
			if(book[i][3]!=null && book[i][3].equals(pressname)) {
				System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！");
				press();
			}
		}
		for (int i = 0; i < press.length; i++) {
            if (press[i][0] != null && pressname.equals(press[i][0])) {
                for (int j = 0; j < press[i].length; j++) {
                    press[i][j]=null;
                    System.out.println(i);
                    System.out.println("删除成功");
                    press();
                }
            }
        }
		
	}
	private static void pressadd() {
		for (int i = 0; i < press.length; i++) {
			if (press[i][0]==null) {
				System.out.println("请输入出版社名称：");
				press[i][0] = sc.next();
				System.out.println("请输入出版社地址：");
				press[i][1] = sc.next();
				System.out.println("请输入联系人：");
				press[i][2] = sc.next();
				System.out.println("出版社添加成功");
					
			}
		}
		
		
		menu();
		
		
	}

	public static void bookmanage() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
		int key = sc.nextInt();
		switch (key) {
		case 1:
		bookadd();
			break;
		case 2:
		bookdelete();
			break;
		case 3:
			bookgx();
			break;
		case 4:
			bookcx();
	 
			break;
		case 5:
	        menu();
			break;
		

		default:
			break;
		}
		
		
		
	}
	public static void PressManagement() {
		
	}
	public static void bookadd() {
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]==null) {
				index = i;
				break;
			}
		}
		System.out.println("请输入图书ISBN：");
		book[index][0]=sc.next();
		System.out.println("请输入书名：");
		book[index][1]=sc.next();
		System.out.println("请输入价格：");
		book[index][2]=sc.next();
	    System.out.println("请输入出版社");
		book[index][3]=sc.next();
		System.out.println("请输入作者");
		book[index][4]=sc.next();
		System.out.println("添加成功");
		
	}
	public static void bookdelete() {
		System.out.println("请输入要删除图书：");
		String bookName =sc.next();
		
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if (bookName.equals(book[i][1])) {
				book[i][0] = null;
				book[i][1] = null;
				book[i][2] = null;
				book[i][3] = null;
				book[i][4] = null;
				System.out.println("删除成功");
				bookmanage();
				
			}
		}
		
		
		
	
		
	}
	public static void bookgx() {
		book();
		System.out.println("请输入要更新书籍isbn");
		String isbn= sc.next();
		int index =-1;
		for (int i = 0; i < book.length; i++) {
			if (isbn.equals(book[i][0])) {
				index=i;
				break;
			}
		}
		if (index!=-1) {
			System.out.println("请输入书名");
			book[index][1]=sc.next();
			System.out.println("请输入出版社");
			book[index][2]=sc.next();
			System.out.println("请输入作者");
			book[index][3]=sc.next();
			System.out.println("请输入价格范围");
			book[index][4]=sc.next();
			System.out.println("请输入更新成功");
			
		}else {
			System.out.println("不存在");
		}
		
		
	}
	public static void bookcx() {
		book();
		System.out.println("请输入要查询书籍");
		String bookname=sc.next();
		System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < book.length; i++) {
			
			if (book[i][1] != null) {
				int a = book[i][1].indexOf(bookname);
				int b = bookname.indexOf(book[i][1]);
				if (a != b) {
					System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]);
					
				}
			}
		}
		
	}
	public static void book() {
		book[0][0]="01";
		book[0][1]="java";
		book[0][2]="1000";
		book[0][3]="闽西出版社";
		book[0][4]="小闽";
		
		book[1][0]="01";
		book[1][1]="c++";
		book[1][2]="100";
		book[1][3]="闽西";
		book[1][4]="无名";
		
	}
	public static void pressinfo() {
		press[0][0]="闽西出版社";
		press[0][1]="闽西路";
		press[0][2]="小闽";
		press[1][0]="龙岩出版社";
		press[1][1]="龙岩路";
		press[1][2]="小龙";
		press[2][0]="福建出版社";
		press[2][1]="福建路";
		press[2][2]="小福";
				
		
	}

}




