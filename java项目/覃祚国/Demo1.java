package 书籍管理.md.com;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
public class Demo1 {
	static Scanner scanner= new Scanner(System.in);
	static String[][] user=new String[20][4];
	static String[][] book=new String[20][5];
	static String[][] press=new String[20][3];
	public static void main(String[] args) {
		name1();
		Main();
	}
	public static void name1() {//信息
		user[0][0]="蜀国部";user[0][1]="刘备";user[0][2]="a";user[0][3]="主公";
		user[1][0]="魏国部";user[1][1]="曹操";user[1][2]="a";user[1][3]="主公";
		user[2][0]="吴国部";user[2][1]="孙权";user[2][2]="a";user[2][3]="主公";
		user[3][0]="a部";user[3][1]="a";user[3][2]="a";user[3][3]="a";
		book[0][0]="111";book[0][1]="春秋";book[0][2]="10";book[0][3]="蜀国部出版社";book[0][4]="诸葛亮";
		book[1][0]="222";book[1][1]="史记";book[1][2]="10";book[1][3]="魏国部出版社";book[1][4]="司马懿";
		book[2][0]="333";book[2][1]="大学";book[2][2]="10";book[2][3]="吴国部出版社";book[2][4]="陆逊";
		press[0][0]="蜀国部出版社";press[0][1]="巴蜀";press[0][2]="刘备";
		press[1][0]="魏国部出版社";press[1][1]="北魏";press[1][2]="曹操";
		press[2][0]="吴国部出版社";press[2][1]="东吴";press[2][2]="孙权";
	}
	public static void logout() {//退出系统
		System.out.println("你确定要退出系统吗？");
		System.out.println("1 确定   2 取消");
		int b=scanner.nextInt();
		switch (b) {
		case 1:
			System.out.println("你已退出系统，感谢你的使用！");
			break;
		case 2:
			menu();
			break;
		default:
			break;
		}
	}
	public static void Main() {//首页
		System.out.println("欢迎来到闽大图书管理系统！");
		System.out.println("1.登录 2.注册");
		int key =scanner.nextInt();
		switch (key) {
		case 1:
			login();
			break;
		case 2:
			register();
			break;
		default:
			break;
		}
	}
	private static void login() {//登录
		for (int i = 1; i < 4; i++) {
			System.out.println("请输入用户名：");
			String username = scanner.next(); 
			System.out.println("请输入密码：");
			String password = scanner.next();
			boolean receive1 = logfd(username,password);
			if (receive1) {
				System.out.println("登录成功");
				System.out.println(username+"欢迎你");
				menu();
				break;
			}else {
				System.out.println("该用户不存在或者密码错误！请重新登录！");
				System.out.println("你还有"+(3-i)+"次机会");
				if (i>=3) {
					System.out.println("系统退出！！！");
				}
			}
		}
	}
	private static boolean logfd(String username,String password) {//登录判断
		boolean fole = false;
		for (int i = 0; i < user.length; i++) {
			if (username.equals(user[i][1])&&password.equals(user[i][2])) {
				fole=true;
			}
		}
		return fole;
	}
	private static void register() {//注册
		System.out.println("请输入所属部门：");
		String department=scanner.next();
		System.out.println("请输入用户名：");
		String usemame=scanner.next();
		System.out.println("请输入密码：");
		String cipher=scanner.next();
		System.out.println("请输入用户类型：");
		String usert=scanner.next();
		int a = 0;
		for (int i = 0; i < user.length; i++) {
			if (user[i][0]=="null") {
				a=i;
			}
		}
		user[a][0]=department;
		user[a][1]=usemame;
		user[a][2]=cipher;
		user[a][3]=usert;
		System.out.println("注册成功，即将返回登录！");
		login();
	}
	private static void menu() {//菜单
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出系统  ");
		int select=scanner.nextInt();
		switch (select) {
		case 1:
			LibrarySystem();
			break;
		case 2:
			PressManagement();
			break;
		case 3:
			logout();
			break;
		default:
			break;
		}
	}
	private static void LibrarySystem() {//图书管理
		System.out.println("请输入：1 增加 2 删除 3 更新 4 查询 5 返回上一级菜单");
		int c=scanner.nextInt();
		switch (c) {
		case 1:
			increase();
			break;
		case 2:
			delete();
			break;
		case 3:
			update();
			break;
		case 4:
			inquire();
			break;
		case 5:
			menu();
			break;
		default:
			break;
		}
	}
	private static void increase() {//增加(图)
		System.out.println("请输入图书ISBN:");
		String ISBN = scanner.next();
		System.out.println("请输入书名:");
		String bookname = scanner.next();
		System.out.println("请输入价格:");
		String price = scanner.next();
		System.out.println("请输入出版社:");
		String press = scanner.next();
		System.out.println("请输入作者:");
		String author = scanner.next();
		int d=0;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]==null) {
				d=i;
				book[d][0]=ISBN;
				book[d][1]=bookname;
				book[d][2]=price;
				book[d][3]=press;
				book[d][4]=author;
				System.out.println("添加成功！！！");
				LibrarySystem();
				break;
			}
		}
	}
	private static void delete() {//删除(图)
		System.out.println("请输入要删除的书本名称：");
		String deletebook=scanner.next();
		int e=0;
		for (int i = 0; i < book.length; i++) {
			if (deletebook.equals(book[i][1])) {
				e=i;
				book[e][0]=null;
				book[e][1]=null;
				book[e][2]=null;
				book[e][3]=null;
				book[e][4]=null;
				System.out.println("删除成功");
				LibrarySystem();
				break;
			}else {
				System.out.println("未找到该书籍或删除失败！");
				System.out.println("是否重新尝试");
				System.out.println("1 是 2 否");
				int f=scanner.nextInt();
				switch (f) {
				case 1:
					delete();
					break;
				case 2:
					LibrarySystem();
					break;
				default:
					break;
				}
			}
		}
	}
	private static void update() {//更新(图)
		System.out.println("请输入ISBN号：");
		String ISBN=scanner.next();
		System.out.println("请输入新的书名:");
		String bookname=scanner.next();
		System.out.println("请输入新的价格:");
		String price=scanner.next();
		System.out.println("请输入新的出版社:");
		String press=scanner.next();
		System.out.println("请输入新的作者：");
		String author=scanner.next();
		int g=scanner.nextInt();
		for (int i = 0; i < book.length; i++) {
			if (ISBN.equals(book[i][0])) {
				g=i;
				book[g][1]=bookname;
				book[g][2]=price;
				book[g][3]=press;
				book[g][4]=author;
				System.out.println("更新成功！！！");
				LibrarySystem();
				break;
			}else {
				System.out.println("该ISBN号不存在！！！");
				System.out.println("是否重新尝试");
				System.out.println("1 是 2 否");
				int h=scanner.nextInt();
				switch (h) {
				case 1:
					update();
					break;
				case 2:
					LibrarySystem();
					break;
				default:
					break;
				}
			}
		}
	}
	private static void inquire() {//查询(图)
		System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		int p = scanner.nextInt();
		switch (p) {
		case 1:
			isbn();
			break;
		case 2:
			title();
			break;
		case 3:
			press();
			break;
		case 4:
			admin();
			break;
		case 5:
			DV();
			break;
		case 6:
			query();
			break;
		case 7:
			LibrarySystem();
			break;
		default:
			break;
		}
	}
	private static void isbn() {//isbn(查)
		System.out.println("请输入ISBN号：");
		String ISBN= scanner.next();
		int y=0;
		for (int i = 0; i < book.length; i++) {
			if (ISBN.equals(book[i][0])) {
				y=i;
				System.out.println("该书信息为以下内容");
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+'\t'+"作者"+'\t');
				System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]+'\t');
				System.out.println("是否继续查询");
				System.out.println("1 是 2 否");
				int q=scanner.nextInt();
				switch (q) {
				case 1:
					inquire();
					break;
				case 2:
					LibrarySystem();
					break;
				default:
					break;
				}
			}else {
				System.out.println("未查询到该书，是否继续？");
				System.out.println("1 是 2 否");
				int w=scanner.nextInt();
				switch (w) {
				case 1:
					isbn();
					break;
				case 2:
					inquire();
					break;
				default:
					break;
				}
			}
		}
	}
	private static void title() {//书名（模糊）(查)
		System.out.println("请输入书名：");
		String title = scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (book[i][1]!=null&&book[i][0].indexOf(title)!=-1) {
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+'\t'+"作者"+'\t');
				System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]+'\t');
				System.out.println("是否继续查询");
				System.out.println("1 是 2 否");
				int q=scanner.nextInt();
				switch (q) {
				case 1:
					inquire();
					break;
				case 2:
					LibrarySystem();
					break;
				default:
					break;
				}
			}else {
				System.out.println("未查询到该书，是否继续？");
				System.out.println("1 是 2 否");
				int w=scanner.nextInt();
				switch (w) {
				case 1:
					isbn();
					break;
				case 2:
					inquire();
					break;
				default:
					break;
				}
			}
		}
	}
	private static void press() {//出版社(查)
		System.out.println("请输入出版社名称：");
		String press= scanner.next();
		int y=0;
		for (int i = 0; i < book.length; i++) {
			if (press.equals(book[i][3])) {
				y=i;
				System.out.println("该书信息为以下内容");
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+'\t'+"作者"+'\t');
				System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]+'\t');
				System.out.println("是否继续查询");
				System.out.println("1 是 2 否");
				int q=scanner.nextInt();
				switch (q) {
				case 1:
					inquire();
					break;
				case 2:
					LibrarySystem();
					break;
				default:
					break;
				}
			}else {
				System.out.println("未查询到该书，是否继续？");
				System.out.println("1 是 2 否");
				int w=scanner.nextInt();
				switch (w) {
				case 1:
					isbn();
					break;
				case 2:
					inquire();
					break;
				default:
					break;
				}
			}
		}
	}
	private static void admin() {//作者(查)
		System.out.println("请输入出版社名称：");
		String admin= scanner.next();
		int y=0;
		for (int i = 0; i < book.length; i++) {
			if (admin.equals(book[i][4])) {
				y=i;
				System.out.println("该书信息为以下内容");
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+'\t'+"作者"+'\t');
				System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]+'\t');
				System.out.println("是否继续查询");
				System.out.println("1 是 2 否");
				int q=scanner.nextInt();
				switch (q) {
				case 1:
					inquire();
					break;
				case 2:
					LibrarySystem();
					break;
				default:
					break;
				}
			}else {
				System.out.println("未查询到该书，是否继续？");
				System.out.println("1 是 2 否");
				int w=scanner.nextInt();
				switch (w) {
				case 1:
					isbn();
					break;
				case 2:
					inquire();
					break;
				default:
					break;
				}
			}
		}
	}
	private static void DV() {//价格范围(查)
		System.out.println("请输入出版社名称：");
		String DV= scanner.next();
		int y=0;
		for (int i = 0; i < book.length; i++) {
			if (DV.equals(book[i][2])) {
				y=i;
				System.out.println("该书信息为以下内容");
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+'\t'+"作者"+'\t');
				System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]+'\t');
				System.out.println("是否继续查询");
				System.out.println("1 是 2 否");
				int q=scanner.nextInt();
				switch (q) {
				case 1:
					inquire();
					break;
				case 2:
					LibrarySystem();
					break;
				default:
					break;
				}
			}else {
				System.out.println("未查询到该书，是否继续？");
				System.out.println("1 是 2 否");
				int w=scanner.nextInt();
				switch (w) {
				case 1:
					isbn();
					break;
				case 2:
					inquire();
					break;
				default:
					break;
				}
			}
		}
	}
	private static void query() {//查询所有(查)
		for (int i = 0; i < book.length; i++) {
			for (int j = 0; j < book.length; j++) {
				if (book[i][0]!=null) {
					System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+'\t'+"作者"+'\t');
					System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]+'\t');
					System.out.println("是否继续查询");
					System.out.println("1 是 2 否");
					int q=scanner.nextInt();
					switch (q) {
					case 1:
						inquire();
						break;
					case 2:
						LibrarySystem();
						break;
					default:
						break;
					}
				}
			}
		}
	}
	private static void PressManagement() {//出版社管理
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int g=scanner.nextInt();
		switch (g) {
		case 1:
			incre();
			break;
		case 2:
			delet();
			break;
		case 3:
			updat();
			break;
		case 4:
			PressName();
			break;
		case 5:
			allpress();
			break;
		case 6:
			menu();
			break;
		default:
			break;
		}
	}
	private static void incre() {//增加(图)
		System.out.println("请输入出版社名称：");
		String pressname=scanner.next();
		System.out.println("请输入出版社地址：");
		String presssite=scanner.next();
		System.out.println("请输入出版社联系人：");
		String pressAttn=scanner.next();
		int u=0;
		for (int i = 0; i < press.length; i++) {
			if (press[i][0]==null) {
				u=i;
				press[i][0]=pressname;
				press[i][1]=presssite;
				press[i][2]=pressAttn;
				System.out.println("出版社添加成功");
				PressManagement();
				break;
			}
		}
		
	}
	private static void delet() {//删除(出)
		System.out.println("请输入要删除的出版社名称：");
		String hhu=scanner.next();
		int gr=0;
		for (int i = 0; i < press.length; i++) {
			if (hhu.equals(press[i][1])) {
				gr=i;
				press[gr][0]=null;
				press[gr][1]=null;
				press[gr][2]=null;
				System.out.println("删除成功");
				PressManagement();
				break;
			}else {
				System.out.println("未找到该出版社或删除失败！");
				System.out.println("是否重新尝试");
				System.out.println("1 是 2 否");
				int f=scanner.nextInt();
				switch (f) {
				case 1:
					delet();
					break;
				case 2:
					PressManagement();
					break;
				default:
					break;
				}
			}
		}
	}
	private static void updat() {//更新(出)
		System.out.println("请输入要更新的出版社名称：");
		String Presse=scanner.next();
		int g=scanner.nextInt();
		for (int i = 0; i < press.length; i++) {
			if (Presse.equals(press[i][0])) {
				g=i;
				System.out.println("出版社名称"+'\t'+"地址"+'\t'+" 联系人");
				System.out.println(press[g][0]+'\t'+press[g][1]+'\t'+press[g][2]);
				System.out.println("请输入要更新的地址：");
				String site=scanner.next();
				System.out.println("请输入要更新的联系人姓名：");
				String Attn=scanner.next();
				press[g][1]=site;
				press[g][2]=Attn;
				System.out.println("更新成功");
				System.out.println("出版社名称"+'\t'+"地址"+'\t'+" 联系人");
				System.out.println(press[g][0]+'\t'+press[g][1]+'\t'+press[g][2]);
				PressManagement();
				break;
			}else {
				System.out.println("该出版社不存在！！！");
				System.out.println("是否重新尝试");
				System.out.println("1 是 2 否");
				int h=scanner.nextInt();
				switch (h) {
				case 1:
					updat();
					break;
				case 2:
					PressManagement();
					break;
				default:
					break;
				}
			}
		}
	}
	private static void PressName() {//出版社名称查询(出)
		System.out.println("请输入出版社名称：");
		String hskk=scanner.next();
		for (int i = 0; i < press.length; i++) {
			if (hskk.equals(press[i][0])) {
				System.out.println("出版社名称："+press[i][0]);
				System.out.println("出版社地址："+press[i][1]);
				System.out.println("出版社联系人："+press[i][2]);
				PressManagement();
				break;
			}else {
				System.out.println("出错了！！！");
				PressName();
			}
		}
	}
	private static void allpress() {//所有出版社(出)
		System.out.println("出版社名称"+'\t'+"地址"+'\t'+" 联系人");
		for (int i = 0; i < press.length; i++) {
			if (press[i][0]!=null) {
				System.out.println(press[i][0]+'\t'+press[i][1]+'\t'+press[i][2]);
			}
		}
		PressManagement();
	}
}
