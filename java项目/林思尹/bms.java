package bms.com.cn;


	//做到模糊出版社查询
	import java.util.ArrayList;
	import java.util.List;
	import java.util.Scanner;

	public class bms {
	    static int usersDefaultNum = 50;
	    static int booksDefaultNum = 100;
	    static int publishersDefaultNum = 30;
	    static String[][] users = new String[usersDefaultNum][4];
	    static String[][] books = new String[booksDefaultNum][5];
	    static String[][] publishers = new String[publishersDefaultNum][3];
	    static Scanner s = new Scanner(System.in);
	    static String nowUserName;

	    public static void main(String[] args) {
	        //初始化信息
	        initUsers(users);
	        initBooks(books);
	        initPublishers(publishers);
	        welcome();
	        managementSysChoose();

	    }

	    //选择功能
	    public static void managementSysChoose() {
	        System.out.println( nowUserName + ",欢迎您使用闽大书籍管理系统！！！");

	        while (true) {
	            System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
	            int choose = s.nextInt();
	            if (choose == 1){
	                booksManage();
	                break;
	            }else if(choose == 2){
	                break;
	            }else if(choose == 3){

	                break;
	            }else if(choose == 4){
	                System.exit(0);
	            }else{
	                System.out.println("请重新输入");
	                continue;
	            }
	        }

	    }

	    //图书管理功能
	    public static void booksManage() {
	        System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
	        int choose = s.nextInt();

	        boolean flag;
	        while(true){
	            flag = false;
	            switch (choose) {
	                case 1:
	                    booksManageAdd();
	                    break;
	                case 2:
	                    booksManageDelete();
	                    break;
	                case 3:
	                    booksUpdate();
	                    break;
	                case 4:
	                    booksQuery();
	                    break;
	                case 5:
	                    managementSysChoose();
	                    break;
	                default:
	                    flag = true;
	                    System.out.println("请重新输入");
	                    choose = s.nextInt();
	                    break;
	            }
	            if(flag == false){
	                break;
	            }
	        }
	    }

	    //查询
	    public static void booksQuery() {
	        System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
	        int choose = s.nextInt();
	        boolean flag;
	        int index = -1;

	        while(true){
	            flag = false;
	            switch (choose) {
	                case 1:
	                    System.out.println("请输入ISBN号");
	                    String iNum = s.next();
	                    index = findIndexISBN(iNum);
	                    if (index != -1) {
	                        System.out.print("ISBN" + " \t" + "书名" + " \t" + "价格" + " \t" + "出版社" + " \t" + "作者");
	                        System.out.println();
	                        for (int i = 0; i < books[index].length; i++) {
	                            System.out.print(books[index][i] + " \t");
	                        }
	                        System.out.println();
	                    }else{
	                        System.out.print("ISBN" + "\t" + "书名" + "\t" + "价格" + "\t" + "出版社" + "\t" + "作者");
	                        System.out.println();
	                    }
	                    break;
	                case 2:
	                    System.out.println("请输入书名关键字");
	                    String bookName = s.next();
	                    ArrayList<Integer> arrayIndex = findIndexName(bookName);
	                    if(arrayIndex.size() != 0){
	                        System.out.print("ISBN" + " \t" + "书名" + " \t" + "价格" + " \t" + "出版社" + " \t" + "作者");
	                        System.out.println();
	                        for (int i = 0; i < arrayIndex.size(); i++) {
	                            for (int j = 0; j < books[arrayIndex.get(i)].length; j++) {
	                                System.out.print(books[arrayIndex.get(i)][j] + " \t");
	                            }
	                            System.out.println();
	                        }
	                    }else{
	                        System.out.print("ISBN" + " \t" + "书名" + " \t" + "价格" + " \t" + "出版社" + " \t" + "作者");
	                        System.out.println();
	                    }
	                    break;
	                case 3:
	                    System.out.println("请输入出版社信息");
	                    String bookPublisher = s.next();
	                    break;
	                case 4:
	                    System.out.println("请输入作者");
	                    String bookAuthor = s.next();
	                    break;
	                case 5:
	                    System.out.println("请输入价格");
	                    String price = s.next();
	                    break;
	                case 6:
	                    for(int i = 0;i < books.length;i++){
	                        for(int j = 0;j < books[i].length;j++){
	                            System.out.print(books[i][j] + "|");
	                        }
	                        System.out.println();
	                    }
	                    break;
	                case 7:
	                    managementSysChoose();
	                    break;
	                default:
	                    flag = true;
	                    System.out.println("请重新输入");
	                    choose = s.nextInt();
	                    break;
	            }
	            if(flag == false){
	                break;
	            }
	        }

	        booksQuery();

	    }

	    //根据ISBN查找数组下标识
	    public static int findIndexISBN(String s) {
	        for (int i = 0; i < books.length; i++) {
	            if (s.equals(books[i][0])) {
	                return i;
	            }
	        }
	        return -1;
	    }

	    //通过书名模糊查询
	    public static ArrayList<Integer> findIndexName(String bookName) {
	        ArrayList<Integer> arrayIndex = new ArrayList<>();
	        for (int i = 0; i < books.length; i++) {
	            if(books[i][1] != null && books[i][1].contains(bookName)){
	                arrayIndex.add(i);
	            }
	        }
	        return arrayIndex;
	    }

	    //图书更新
	    public static void booksUpdate() {
	        System.out.println("请输入ISBN号：");
	        String iNum = s.next();
	        //查找下标,-1提示没有找到该编号
	        int index = findIndexISBN(iNum);
	        if(index != -1){
	            System.out.println("请输入新书籍名称：");
	            String bookName = s.next();

	            System.out.println("请输入新书籍价格：");
	            String bookPrice = s.next();

	            System.out.println("请输入新书籍出版社：");
	            String bookPublisher = s.next();

	            System.out.println("请输入新书籍作者");
	            String author = s.next();

	            books[index][1] = bookName;
	            books[index][2] = bookPrice;
	            books[index][3] = bookPublisher;
	            books[index][4] = author;
	            System.out.println("更新成功");

	        }else{
	            System.out.println("没有找到该编号");
	        }

	        for(int i = 0;i < books.length;i++){
	            for(int j = 0;j < books[i].length;j++){
	                System.out.print(books[i][j] + "|");
	            }
	            System.out.println();
	        }

	        booksManage();
	    }

	    //图书管理（删除）
	    public static void booksManageDelete() {
	        System.out.println("请输入要删除的书本编号");
	        String deleteNum = s.next();
	        //查找下标,-1提示没有找到该编号
	        int index = findIndexISBN(deleteNum);
	        if(index != -1){
	            //从要删除的目录开始，后一个数组赋给前一个数组
	            for (int i = index; i < books.length - 1; i++) {
	                books[i] = books[i + 1];
	            }
	            //最后的数组全部元素为空
	            for(int i = 0;i < books[books.length - 1].length;i++){
	                books[books.length - 1][i] = null;
	            }
	        }else{
	            System.out.println("没有找到该编号");
	        }

	        for(int i = 0;i < books.length;i++){
	            for(int j = 0;j < books[i].length;j++){
	                System.out.print(books[i][j] + "|");
	            }
	            System.out.println();
	        }
	        booksManage();

	    }


	    //图书馆里(增加)
	    public static void booksManageAdd() {
	        //编码（ISBN）、书籍名称、价格、出版社、作者
	        System.out.println("请输入书籍编码（ISBN）：");
	        String ISBN = s.next();
	        boolean flag;
	        //判断编码是否相同
	        while (true) {
	            flag = false;
	            for (int i = 0; i < books.length; i++) {
	                if (ISBN == books[i][0]) {
	                    flag = true;
	                    System.out.println("本编号已存在，请重新输入");
	                    ISBN = s.next();
	                    break;
	                }
	            }
	            if(flag == false){
	                break;
	            }
	        }

	        System.out.println("请输入书籍名称：");
	        String bookName = s.next();

	        System.out.println("请输入价格：");
	        String bookPrice = s.next();

	        System.out.println("请输入出版社：");
	        String bookPublisher = s.next();

	        System.out.println("请输入作者");
	        String author = s.next();

	        //判断书本数组的空位
	        int index = isNull(books);

	        if(index != -1) {
	            books[index][0] = ISBN;
	            books[index][1] = bookName;
	            books[index][2] = bookPrice;
	            books[index][3] = bookPublisher;
	            books[index][4] = author;
	        }else {
	            booksDefaultNum += 50;
	            String[][] newBooks = new String[booksDefaultNum][5];

	            newBooks = copyArrays(books,newBooks);

	            newBooks[books.length][0] = ISBN;
	            newBooks[books.length][1] = bookName;
	            newBooks[books.length][2] = bookPrice;
	            newBooks[books.length][3] = bookPublisher;
	            newBooks[books.length][4] = author;

	            //更新旧数组
	            books = newBooks;
	        }
	        System.out.println("新增成功");

	        for(int i = 0;i < books.length;i++){
	            for(int j = 0;j < books[i].length;j++){
	                System.out.print(books[i][j] + "|");
	            }
	            System.out.println();
	        }

	        booksManage();
	    }

	    //欢迎方法
	    public static void welcome() {
	        System.out.println("欢迎使用图书管理系统：");
	        System.out.println("1.登陆   2.注册");

	        //判断用户的选择
	        int choose = s.nextInt();
	        switch (choose) {
	            case 1:
	                login(users);
	                break;
	            case 2:
	                int index = isNull(users);
	                Register(index);
	                welcome();
	                break;

	            default:
	                System.out.println("退出系统！！");
	                System.exit(0);
	                break;
	        }
	    }

	    //判断数组哪里有空
	    public static int isNull(String[][] strs) {
	        int index = -1;

	        for(int i = 0;i < strs.length;i++) {
	            if(strs[i][1] == null) {
	                index = i;
	                return index;
	            }
	        }

	        return index;
	    }

	    //注册方法
	    public static void Register(int index) {

	        System.out.println("请输入注册用户部门：");
	        String userDepart = s.next();

	        System.out.println("请输入注册用户名：");
	        String userName = s.next();

	        System.out.println("请输入注册密码：");
	        String userPassword = s.next();

	        System.out.println("请输入注册用户角色：");
	        String userPart = s.next();

	        while(true){
	            boolean flag = true;
	            for (int i = 0; i < users.length; i++) {
	                if(userName.equals(users[i][1])){
	                    flag = false;
	                    break;
	                }
	            }
	            if (flag == false) {
	                System.out.println("您注册的用户名已存在,请重新输入");
	                userName = s.next();
	            }else{
	                System.out.println("注册成功！");
	                break;
	            }
	        }

	        //判断数组有没有满，没满直接赋值，满了新建一个更大数组赋值
	        if(index != -1) {
	            users[index][0] = userDepart;
	            users[index][1] = userName;
	            users[index][2] = userPassword;
	            users[index][3] = userPart;
	        }else {
	            usersDefaultNum += 50;
	            String[][] newUsers = new String[usersDefaultNum][4];

	            newUsers = copyArrays(users,newUsers);

	            newUsers[users.length][0] = userDepart;
	            newUsers[users.length][1] = userName;
	            newUsers[users.length][2] = userPassword;
	            newUsers[users.length][3] = userPart;

	            //把新数组赋值给旧数组
	            users = newUsers;
	        }

	        //遍历新数组
//	        for (int i = 0; i < users.length; i++) {
//	            for (int j = 0; j < users[i].length; j++) {
//	                System.out.print(users[i][j] + "|");
//	            }
//	            System.out.println();
//	        }
	    }

	    //二维数组拷贝
	    public static String[][] copyArrays(String[][] old, String[][] newArrays) {
	        for (int i = 0; i < old.length; i++) {
	            for (int j = 0; j < old[i].length; j++) {
	                newArrays[i][j] = users[i][j];
	            }
	        }

	        return newArrays;

	    }

	    //登陆
	    public static void login(String[][] users) {
	        boolean flag = false;
	        for(int j = 0;j < 4;j++) {

	            System.out.println("请输入用户名：");
	            String userName = s.next();

	            System.out.println("请输入密码：");
	            String userPassword = s.next();

	            for(int i = 0;i < users.length;i++) {
	                if(userName.equals(users[i][1]) && userPassword.equals(users[i][2])) {
	                    flag = true;
	                    break;
	                }
	            }

	            if(j == 2 && flag == false) {
	                System.out.println("输入错误超过3次，退出系统");
	                System.exit(0);
	            }else if(flag == true) {
	                nowUserName = userName;
	                System.out.println("登陆成功！");
	                break;
	            }else if (flag == false && j < 3) {
	                System.out.println("该用户不存在或者密码错误！请重新登录！");
	            }
	        }

	    }


	    //初始化出版社
	    public static void initPublishers(String[][] publishers) {
	        publishers[0][0] = "人民文学出版社";
	        publishers[0][1] = "北京市xxxx";
	        publishers[0][2] = "amy";

	        publishers[0][0] = "意林文学出版社";
	        publishers[0][1] = "上海市xxxx";
	        publishers[0][2] = "adam";
	    }

	    //初始化书
	    public static void initBooks(String[][] books) {
	        books[0][0] = "165631";
	        books[0][1] = "直捣蜂窝的女孩";
	        books[0][2] = "30";
	        books[0][3] = "人民文学出版社";
	        books[0][4] = "史迪格拉森";

	        books[1][0] = "46161641";
	        books[1][1] = "神经漫游者";
	        books[1][2] = "50";
	        books[1][3] = "意林出版社";
	        books[1][4] = "威廉吉布森";
	    }

	    //初始化用户
	    public static void initUsers(String[][] users) {
	        users[0][0] = "管理部门";
	        users[0][1] = "admin";
	        users[0][2] = "abc";
	        users[0][3] = "管理者";

	        users[1][0] = "普通用户";
	        users[1][1] = "sally";
	        users[1][2] = "def";
	        users[1][3] = "普通用户";
	    }



	}