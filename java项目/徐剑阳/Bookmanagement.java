import java.util.Scanner;

public class Book {
	static String users[][] = new String[20][4];
	static String book[][] = new String[20][5];
	static String press[][] = new String[20][3];
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		init();
		while (true) {
			System.out.println("欢迎使用闽大图书管理系统!");
			System.out.println("1.登录     2.注册");
			int key = sc.nextInt();
			switch (key) {
			case 1:
				loginview();
				break;
			case 2:
				register();
				break;
			default:
				break;
			}
		}
	}
	public static void init() {
		
		users[0][0]="软件部";
		users[0][1]="admin";
		users[0][2]="123";
		users[0][3]="管理员";

		book[0][0]="123";
		book[0][1]="活着";
		book[0][2]="200";
		book[0][3]="中华书局";
		book[0][4]="余华";
		
		book[1][0]="456";
		book[1][1]="哑舍";
		book[1][2]="100";
		book[1][3]="闽大出版社";
		book[1][4]="玄色";
		
		book[2][0]="789";
		book[2][1]="史记";
		book[2][2]="300";
		book[2][3]="重庆出版社";
		book[2][4]="司马迁";
		
		press[0][0]="中华书局";
		press[0][1]="北京市王府井大街36号";
		press[0][2]="张三";
		
		press[1][0]="重庆出版社";
		press[1][1]="北京市王府井大街37号";
		press[1][2]="李四";
	}
	public static void register() {
		int index = getFirstNullUserIndex();
		System.out.println("请输入所属部门");
		users[index][0] = sc.next();
		System.out.println("请输入用户名");
		users[index][1] = sc.next();
		System.out.println("请输入密码");
		users[index][2] = sc.next();
		System.out.println("请输入用户角色");
		users[index][3] = sc.next();
	}	
	public static int getFirstNullUserIndex() {
		int index = -1;
		for (int i = 0; i < users.length; i++) {
			if (users[i][0]==null) {
				index = i;
				break;
			}
		}
		return index;
	}
	public static boolean login(String Username,String Password) {
		boolean flag = false;
		for (int i = 0; i < 3; i++) {
			if (Username.equals(users[i][1])&&Password.equals(users[i][2])) {
				flag = true;
			}
		}
		return flag;
	}
	public static void loginview() {
		System.out.println("请输入用户名");
		String Username = sc.next();
		System.out.println("请输入密码");
		String Password = sc.next();
		boolean flag = login(Username,Password);
		if (flag) {
			System.out.println(Username+"登录成功! 欢迎使用闽大书籍管理系统！");
			menu();
		}else {
			System.out.println("输入错误，请重新输入");
			loginview();
		}
	}
	public static void menu() {
			System.out.println("1.图书管理 2.出版社管理 3.退出登陆 4.退出系统");
			int key = sc.nextInt();
			switch (key) {
			case 1:
				bookManagement();
				break;
			case 2:
				PressManagement();
				break;
			case 3:
				System.out.println("退出登录成功！");
				loginview();
				break;
			case 4:
				System.out.println("系统退出成功！");
				System.exit(0);
				break;
			default:
				break;
			}
	}
	public static void PressManagement() {
		while (true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
			int key = sc.nextInt();
			switch (key) {
			case 1:
				addpress();
				break;
			case 2:
				droppress();
				break;
			case 3:
				changepress();
				break;
			case 4:
				pressnamerefer();
				break;
			case 5:
				pressallrefer();
				break;
			case 6:
				menu();
				break;
			default:
				break;
			}
		}
		
	}
	private static void pressallrefer() {
		int index = getpressisnull();
		System.out.println("出版社名称"+'\t'+"出版社地址"+'\t'+"出版社联系人");
		for (int i = 0; i < index; i++) {
			System.out.println(press[i][0]+'\t'+press[i][1]+'\t'+press[i][2]);
		}
		
	}
	private static int getpressisnull() {
		int index = -1;
		for (int i = 0; i < press.length; i++) {
			if (press[i][0] == null) {
				index = i;
				break;
			}
		}		
		return index;
	}
	private static void pressnamerefer() {
		System.out.println("请输入出版社名称：");
		String pressname = sc.next();
		for (int i = 0; i < press.length; i++) {
			if (pressname.equals(press[i][0])) {
				System.out.println("出版社名称"+press[i][0]);
				System.out.println("出版社地址"+press[i][1]);
				System.out.println("出版社联系人"+press[i][2]);
				break;
			}
			else {
				System.out.println("该出版社不存在！！！");
				PressManagement();
			}
		}
		
	}
	public static void changepress() {
		System.out.println("请输入要更新的出版社名称：");
		String newpress = sc.next();
		System.out.println("出版社名称"+'\t'+"出版社地址"+'\t'+"出版社联系人");
		for (int i = 0; i < press.length; i++) {
			if (newpress.equals(press[i][0])) {
				System.out.println(press[i][0]+'\t'+press[i][1]+'\t'+press[i][2]);
			}
		}
		int index = getpress(newpress);
		System.out.println("请输入要更新的地址：");
		press[index][1] = sc.next();
		System.out.println("请输入要更新的联系人姓名：");
		press[index][2] = sc.next();
		System.out.println("更新成功");
		System.out.println("出版社名称"+'\t'+"出版社地址"+'\t'+"出版社联系人");
		System.out.println(press[index][0]+'\t'+press[index][1]+'\t'+press[index][2]);
	}
	public static int getpress(String newpress) {
		int index = -1;
		for (int i = 0; i < press.length; i++) {
			if (newpress.equals(press[i][0])) {
				index = i;
			}
		}
		return index;
	}
	public static void droppress() {
		System.out.println("请输入要删除的出版社名称：");
		String pressname = sc.next();
		for (int i = 0; i < press.length; i++) {
			if (pressname!=null && pressname.equals(book[i][3])) {
				System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！");
				System.out.println("删除失败");
				break;
			}
		if (pressname.equals(press[i][0])) {
			press[i][0] = null;
			press[i][1] = null;
			press[i][2] = null;
			System.out.println("删除成功");
			break;
		}
		}
	}
	
	public static void addpress() {
		int index = getfirstpressisnull();
		System.out.println("输入出版社名称：");
		press[index][0] = sc.next();
		System.out.println("输入出版社地址：");
		press[index][1] = sc.next();
		System.out.println("输入出版社联系人：");
		press[index][2] = sc.next();
		System.out.println("出版社添加成功！");
	}
	public static int getfirstpressisnull() {
		int index = -1;
		for (int i = 0; i < press.length; i++) {
			if (users[i][0]==null) {
				index = i;
				break;
			}
		}
		return index;
	}
	public static void bookManagement() {
		while (true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
			int key = sc.nextInt();
			switch (key) {
			case 1:
				addbook();
				break;
			case 2:
				dropbook();
				break;
			case 3:
				change();
				break;
			case 4:
				bookrefer();
				break;
			case 5:
				menu();
				break;
			default:
				break;
			}
		}
	}
	private static void bookrefer() {
		System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		int key = sc.nextInt();
		switch (key) {
		case 1:
			Isbnrefer();
			break;
		case 2:
			booknamerefer();
			break;
		case 3:
			bookpressrefer();
			break;
		case 4:
			writerrefer();
			break;
		case 5:
			pricerefer();
			break;
		case 6:
			referall();
			break;
		case 7:
			bookManagement();
			break;
		default:
			break;
		}
		
	}
	private static void referall() {
		int index = getbookisnotnull();
		System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < index; i++) {
			System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]);
		}
		
	}
	private static int getbookisnotnull() {
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0] == null) {
				index = i;
				break;
			}
		}
		return index;
	}
	private static void pricerefer() {
		System.out.println("请输入最低价格");
		int min = sc.nextInt();
		System.out.println("请输入最高价格");
		int max = sc.nextInt();
		for (int i = 0; i < book.length; i++) {
			if (Integer.parseInt(book[i][2])>min || Integer.parseInt(book[i][2])<max) {
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
				System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]);
				break;
			}
		}
	}
	private static void writerrefer() {
		System.out.println("请输入作者姓名：");
		String bookwriter = sc.next();
		for (int i = 0; i < book.length; i++) {
			if (bookwriter.equals(book[i][4])) {
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
				System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]);
			}
		}
	}
	private static void bookpressrefer() {
		System.out.println("请输入出版社：");
		String press = sc.next();
		System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < book.length; i++) {
			if (press.equals(book[i][3])) {
				System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]);
			}
		}
	}
	private static void booknamerefer() {
		System.out.println("请输入书名：");
		String bookname = sc.next();
		System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < book.length; i++) {
			if (book[i][1] != null && book[i][1].indexOf(bookname)!=-1) {
				System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]);
			}
		}
	}
	private static void Isbnrefer() {
		System.out.println("请输入ISBN号：");
		String is = sc.next();
		for (int i = 0; i < book.length; i++) {
			if (is.equals(book[i][0])) {
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
				System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]);
			}
		}
	}
	public static void change() {
		System.out.println("请输入ISBN号：");
		String newISBN = sc.next();
		int index = getnewIsbn(newISBN);
		System.out.println("请输入新的书名");
		book[index][1] = sc.next();
		System.out.println("请输入新的价格");
		book[index][2] = sc.next();
		System.out.println("请输入新的出版社");
		book[index][3] = sc.next();
		System.out.println("请输入新的作者");
		book[index][4] = sc.next();
		System.out.println("更新成功！");
	}
	private static int getnewIsbn(String newISBN) {
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if (newISBN.equals(book[i][0])) {
				index = i;
			}
		}
		return index;
	}
	public static void dropbook() {
		System.out.println("请输入书名：");
		String bookname = sc.next();
		int index = getbookname(bookname);
			book[index][0] = null;
			book[index][1] = null;
			book[index][2] = null;
			book[index][3] = null;
			book[index][4] = null;
		System.out.println("删除成功！");
	}
	public static int getbookname(String bookname) {
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if (bookname.equals(book[i][1])) {
				index = i;
				break;
			}
		}
		return index;
	}
	public static void addbook() {
		int index = getfirstbookisnull();
		System.out.println("请输入图书ISBN");
		book[index][0] = sc.next();
		System.out.println("请输入书名：");
		book[index][1] = sc.next();
		System.out.println("请输入图书价格：");
		book[index][2] = sc.next();
		System.out.println("请输入图书出版社：");
		book[index][3] = sc.next();
		System.out.println("请输入图书作者：");
		book[index][4] = sc.next();
		System.out.println("添加成功！");
	}
	public static int getfirstbookisnull() {
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if (users[i][0]==null) {
				index = i;
				break;
			}
		}
		return index;
	}
}