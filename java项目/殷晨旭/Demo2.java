package Lession;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

import javax.print.attribute.IntegerSyntax;

public class Demo1 {
	static Scanner scanner = new Scanner(System.in);
	static String users [][] = new String [100][4];
	static String books [][] = new String [500][5];
	static String press [][] = new String [70][3];
	public static void main(String[] args) {
		usersinfo();
		booksinfo();
		pressinfo();
		System.out.println("欢迎登陆闽西图书馆！");
		System.out.println("1.登录2.注册");
		int login = scanner.nextInt();
		switch (login) {
		case 1:
			logininfo();
			break;
		case 2:
			register();
			break;
		default:
			System.out.println("输入错误！系统退出");
			break;
		}
	}
	public static void register() {
			System.out.println("请输入所属部门：");
			String department = scanner.next();
			System.out.println("请输入用户名：");
			String username = scanner.next();
			System.out.println("请输入密码：");
			String password = scanner.next();
			for (int i = 0; i < users.length; i++) {
				if(users[i][1] == null) {
					users[i][0]=department;
					users[i][1]=username;
					users[i][2]=password;
					users[i][3]="普通用户";
			}
		}
		System.out.println("注册成功");
		logininfo();
	}
	public static void logininfo() {
		boolean a = false;

			System.out.println("请输入用户名：");
			String loginname = scanner.next();
			System.out.println("请输入密码：");
			String loginpassword = scanner.next();
			for (int i = 0; i < users.length; i++) {
				usersinfo();
				if(loginname.equals(users[i][1])&&loginpassword.equals(users[i][2])) {
					a = true;
					break;
			}
		}
		if(a) {
			System.out.println("登陆成功");
			system();
		}else {
			System.out.println("登陆失败!请重新登录！");
			main(null);
		}
	}
	public static void system() {
		System.out.println("欢迎您使用闽大图书管理系统！！！");
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		int a = scanner.nextInt();
		switch (a) {
		case 1:
			book();
			break;
		case 2:
			press();
			break;
		case 3:
			main(null);
			break;
		case 4:
			System.out.println("已退出系统！！！");
			System.exit(0);
			break;
		default:
			break;
		}
	}
	/*
	 * 出版社系统
	 */
	public static void press() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int keye = scanner.nextInt();
		switch (keye) {
		case 1:
			pressadd();
			break;
		case 2:
			pressdelete();
			break;
		case 3:
			pressupdate();
			break;
		case 4:
			pressnameinquire();
			break;
		case 5:
			pressallunquire();
			break;
		case 6:
			system();
			break;

		default:
			break;
		}
	}
	/*
	 * 查询所有出版社
	 */
	public static void pressallunquire() {
		System.out.println("出版社名称"+'\t'+"地址"+'\t'+"联系人");
		for (int i = 0; i < press.length; i++) {
			if(press[i][0]!=null) {
				System.out.println(press[i][0]+'\t'+press[i][1]+'\t'+press[i][2]);
			}
		}
		press();
	}
	/*
	 * 以名字查询出版社
	 */
	public static void pressnameinquire() {
		System.out.println("请输入出版社的名字：");
		String pressname = scanner.next();
		for (int i = 0; i < press.length; i++) {
			if(pressname.equals(press[i][0])) {
				System.out.println("出版社名称："+'\t'+press[i][0]);
				System.out.println("出版社地址："+'\t'+press[i][1]);
				System.out.println("出版社联系人："+'\t'+press[i][2]);
				press();
			}
		}
		System.out.println("你的输入有误！请重新输入！");
		press();
	}
	/*
	 * 更新出版社
	 */
	public static void pressupdate() {
		System.out.println("请输入出版社名称：");
		String a = scanner.next();
		for (int i = 0; i < press.length; i++) {
			if(a.equals(press[i][0])) {
				System.out.println("出版社名称"+'\t'+"地址"+'\t'+"联系人");
				System.out.println(press[i][0]+'\t'+press[i][1]+'\t'+press[i][2]);
				System.out.println("请输入要更新的出版社名称：");
				String pressname = scanner.next();
				System.out.println("请输入要更新的地址：");
				String pressaddress = scanner.next();
				System.out.println("请输入要更新的联系人姓名：");
				String presslinkman = scanner.next();
				System.out.println("更新完成！");
				press[i][0] = pressname;
				press[i][1] = pressaddress;
				press[i][2] = presslinkman;
				System.out.println("出版社名称"+'\t'+"地址"+'\t'+"联系人");
				System.out.println(press[i][0]+'\t'+press[i][1]+'\t'+press[i][2]);
				press();
			}
		}
		System.out.println("你是输入有误！请重新输入！");
		press();
	}
	/*
	 * 删除出版社
	 */
	public static void pressdelete() {
		System.out.println("请输入要删除的出版社名称：");
		String pressname = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if(books[i][3]!=null && books[i][3].equals(pressname)) {
				System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！");
				press();
			}
		}
		for (int i = 0; i < press.length; i++) {
            if (press[i][0] != null && pressname.equals(press[i][0])) {
                for (int j = 0; j < press[i].length; j++) {
                    press[i][j]=null;
                    System.out.println(i);
                    System.out.println("删除成功");
                    press();
                }
            }
        }
		/*
		 * for (int i = 0; i < press.length; i++) {
			if(press[i][0]!=null&&pressname.equals(press[i][0])) {
				press[i][0] = null;
				press[i][1] = null;
				press[i][2] = null;
				System.out.println("删除成功");
				System.out.println(i);
				press();
			}
		}
		 */
		
		/*
		 * for (int i = 0; i < books.length; i++) {
			for (int j = 0; j < press.length; j++) {
				if(pressname.equals(press[j][0])) {
					if(books[i][3]!=null&&press[j][0].equals(books[i][3])) {
						System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！");
						return;
					}else {
						press[j][0] = null;
						press[j][1] = null;
						press[j][2] = null;
						System.out.println("删除成功！");
						return;
					}
				}else {
					System.out.println("出现错误！");
					break;
				}
			}
		}
		 */
		
	}
	/*
	 * 增加出版社
	 */
	public static void pressadd() {
		for (int i = 0; i < books.length; i++) {
			if(press[i][0]==null) {
				System.out.println("请输入出版社名称：");
				press[i][0] = scanner.next();
				System.out.println("请输入出版社地址：");
				press[i][1] = scanner.next();
				System.out.println("请输入联系人：");
				press[i][2] = scanner.next();
				System.out.println("出版社添加成功");
				system();
			}
		}
		system();
	}
	/*
	 * 图书管理系统
	 */
	public static void book() {
		System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			bookadd();
			break;
		case 2:
			bookdelete();
			break;
		case 3:
			bookupdate();
			break;
		case 4:
			inquire();
			break;
		case 5:
			system();
			break;
		default:
			break;
		}
	}
	/*
	 * 图书查询
	 */
	public static void inquire() {
		System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		int a = scanner.nextInt();
		switch (a) {
		case 1:
			System.out.println("请输入该书的ISBN号：");
			String isbn = scanner.next();
			for (int i = 0; i < books.length; i++) {
				if(isbn.equals(books[i][0])) {
					System.out.println("ISNB号"+'\t'+"书名"+'\t'+"价格"+'\t'+"新华书社"+'\t'+"作者");
					System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
				}
			}
			inquire();
			break;
		case 2:
			booksinquire();
			break;
		case 3:
			bookspress();
			break;
		case 4:
			booksauthor();
			break;
		case 5:
			booksprice();
			break;
		case 6:
			allbook();
			break;
		case 7:
			book();
			break;
		default:
			break;
		}
	}
	/*
	 * 价格范围查询
	 */
	public static void booksprice() {
		System.out.println("请输入最低价格");
		int min = scanner.nextInt();
		System.out.println("请输入最高价格");
		int max = scanner.nextInt();
		System.out.println("ISNB号"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < books.length; i++) {
			if(books[i][0]!=null) {
				int in = Integer.parseInt(books[i][2]);
				if(in>min && in<max) {
					System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
				}
			}
		}
		book();
	}
	/*
	 * 查询所有的书
	 */
	public static void allbook() {
		System.out.println("ISNB号"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < books.length; i++) {
			if(books[i][0]!=null) {
				System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
			}
		}
		inquire();
	}
	/*
	 * 以作者查询书
	 */
	public static void booksauthor() {
		System.out.println("请输入作者的名字：");
		String booksname = scanner.next();
		System.out.println("ISNB号"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < books.length; i++) {	
			if(booksname.equals(books[i][4])) {
				System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);	
			}
		}
		inquire();
	}
	/*
	 * 以出版社查询书
	 */
	public static void bookspress() {
		System.out.println("请输入出版社的名字：");
		for (int j = 0; j < press.length; j++) {
			if(press[j][0]!=null) {
				System.out.print(j+1+"."+press[j][0]+'\t');
			}
		}
		String pressname = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if(books[i][0] != null && pressname.equals(books[i][3])) {
				System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
			}
		}
		inquire();
		/*
		 * System.out.println("请输入出版社前的数字进行查询："+'\t');
		int arr [] = new int [100];
		for (int j = 0; j < press.length; j++) {
			if(press[j][0]!=null) {
				arr[j] = j;
				System.out.print(j+1+"."+press[j][0]+'\t');
			}
		}
		int a = scanner.nextInt();
		for (int i = 0; i < arr.length; i++) {
			if(books[i][0] != null) {
				System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);	
			}
		}
		book();
		 */
		
	}
	/*
	 * 以书名查询
	 */
	public static void booksinquire() {
		System.out.println("请输入书名：");
		String booksname = scanner.next();
		System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < books.length; i++) {
			if(books[i][1] != null) {
				int a = books[i][1].indexOf(booksname);
				int b = booksname.indexOf(books[i][1]);
				if(a != b) {
					System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
				}
			}
		}
		book();
		/*
		 * String booksname = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if(booksname.equals(books[i][1])) {
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
				System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
				book();
			}
		}
		System.out.println("你的输入有误！请重新输入！");
		book();
		 */
	}
	/*
	 * 图书更新
	 */
	public static void bookupdate() {
		System.out.println("请输入ISBN号：");
		String a = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if(a.equals(books[i][0])) {
				System.out.println("请输入要新的书名：");
				books[i][1] = scanner.next();
				System.out.println("请输入要新的价格：");
				books[i][2] = scanner.next();
				System.out.println("请输入要新的出版社：");
				books[i][3] = scanner.next();
				System.out.println("请输入要新的作者：");
				books[i][4] = scanner.next();
				System.out.println("更新成功！");
				book();
			}
		}
		System.out.println("更新错误！请重新执行");
		book();
	}
	/*
	 * 删除书籍
	 */
	public static void bookdelete() {
		System.out.println("请输入要删除的书名：");
		String bookname = scanner.next();
		for (int i = 0; i < books.length; i++) {booksinfo();
			if(bookname.equals(books[i][1])) {
				books[i][0] = null;
				books[i][1] = null;
				books[i][2] = null;
				books[i][3] = null;
				books[i][4] = null;
				System.out.println(books[i][0]+books[i][1]);
				System.out.println("删除成功");
				book();
			}
		}
		System.out.println("删除错误或查无此书！请重新执行！");
		book();
	}
	/*
	 * 增加书籍
	 */
	public static void bookadd() {
		int index = -1;
		for (int i = 0; i < books.length; i++) {
			if(books[i][0] == null) {
				index = i;
			}
		}
		System.out.println("请输入图书ISBN:");
		books[index][0] = scanner.next();
		System.out.println("请输入书名:");
		books[index][1] = scanner.next();
		System.out.println("请输入价格:");
		books[index][2] = scanner.next();
		System.out.println("请输入出版社:");
		books[index][3] = scanner.next();
		System.out.println("请输入作者:");
		books[index][4] = scanner.next();
		System.out.println("添加成功！");
		book();
	}
	public static void usersinfo() {
		users[0][0] = "开发部";
		users[0][1] = "abc";
		users[0][2] = "123";
		users[0][3] = "管理员";
		users[1][0] = "后勤部";
		users[1][1] = "AABB";
		users[1][2] = "10086";
		users[1][3] = "普通人员";
	}
	public static void booksinfo() {
		books[0][0] = "8848001";
		books[0][1] = "青铜葵花";
		books[0][2] = "38";
		books[0][3] = "新华书社";
		books[0][4] = "曹文轩";
		books[1][0] = "8848002";
		books[1][1] = "狗牙雨";
		books[1][2] = "36";
		books[1][3] = "老舍书社";
		books[1][4] = "曹文轩";
		books[2][0] = "8848003";
		books[2][1] = "三体";
		books[2][2] = "42";
		books[2][3] = "周树人出版社";
		books[2][4] = "刘慈欣";
		books[3][0] = "8848004";
		books[3][1] = "盗墓笔记";
		books[3][2] = "38";
		books[3][3] = "新华书社";
		books[3][4] = "南派三叔";
		
	}
	public static void pressinfo() {
		press[0][0] = "新华书社";
		press[0][1] = "龙岩市新罗区曹溪街道123号";
		press[0][2] = "张三";
		press[1][0] = "老舍书社";
		press[1][1] = "龙岩市新罗区曹溪街道124号";
		press[1][2] = "李四";
		press[2][0] = "周树人出版社";
		press[2][1] = "龙岩市新罗区曹溪街道125号";
		press[2][2] = "王五";
		press[3][0] = "哈麻批出版社";
		press[3][1] = "龙岩市新罗区曹溪街道126号";
		press[3][2] = "老六";
	}
}
