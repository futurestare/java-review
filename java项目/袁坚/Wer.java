package vbfg;
import java.util.Arrays;
import java.util.Scanner;
public class Wer {
	static String[][] user=new String[20][4];//用户信息（部门、用户名、密码、用户角色）
	static String[][] book=new String[20][5];//书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）
	static String[][] press=new String[20][3];//出版社信息（出版社名称、地址、联系人）
	static boolean f=false;//登录状态
	static int a;//当前登录用户名
	static Scanner scanner=new Scanner(System.in);
public static void main(String[] args) {
		init();	//数据初始化
		index();//首页
		if (f=true) {
			df();//登录-首页
		}
	}
public static void init() {//数据初始化
	//用户信息（部门、用户名、密码、用户角色）
	user[0][0]="飞升部";user[0][1]="wdnm";user[0][2]="123456";user[0][3]="管理员";
	user[1][0]="飞升部";user[1][1]="哈喇子";user[1][2]="123456";user[1][3]="管理员";
	user[2][0]="飞升部";user[2][1]="二楞子";user[2][2]="123456";user[2][3]="管理员";
	//书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）
	book[0][0]="12352";book[0][1]="富婆爱上我";book[0][2]="120";book[0][3]="上天出版社";book[0][4]="小曹";
	book[1][0]="12585";book[1][1]="全球富婆通讯录";book[1][2]="100";book[1][3]="上天出版社";book[1][4]="曹仔";	
	book[2][0]="23582";book[2][1]="邻居的大姐姐";book[2][2]="10";book[2][3]="入地出版社";book[2][4]="小李";
	book[3][0]="21553";book[3][1]="Java入土级";book[3][2]="60";book[3][3]="大家出版社";book[3][4]="武安";
	//出版社信息（出版社名称、地址、联系人）
	press[0][0]="上天出版社";press[0][1]="飞天省天町路南86号";press[0][2]="曹先生";
	press[1][0]="入地出版社";press[1][1]="入土省地福路北44号";press[1][2]="李先生";
	press[2][0]="大家出版社";press[2][1]="通道省人民街东66号";press[2][2]="大先生";
	press[3][0]="敬奉出版社";press[3][1]="老魏省人民街东66号";press[3][2]="小先生";
}	
public static void index(){//首页
	System.out.println("欢迎光临图书管理系统");
	System.out.println("1.登录  2.注册  3.忘记密码？");
		int key=scanner.nextInt();
	switch (key) {
	case 1:
		login();
		break;
	case 2:
		longinfo();
		break;
	case 3:
		System.out.println("不会吧！不会吧！不会还有人忘记密码吧？");
		break;
	default:
		break;
		}
}
public static void login() {//登录
	System.out.println("请输入用户名");
	String userName=scanner.next();
	System.out.println("请输入密码");
	String coded=scanner.next();
		boolean c=false;
		for (int i = 0; i < user.length; i++) {
			if (userName.equals(user[i][1])) {
				if (coded.equals(user[i][2])) {
					System.out.println("登录成功！");
					c=true;
					df();	
				}
			}
		}
		if (c!=true) {
				System.out.println("用户名或密码错误，请  1.重试  2.回到首页");
				int d=scanner.nextInt();
				switch (d) {
				case 1:
					login();//回到登录
					break;
				case 2:
					index();//回到首页
					break;
				default:
					break;
				}	
		}
}
public static void longinfo() {	//注册
	System.out.println("请输入部门");
	String bumen = scanner.next();
	System.out.println("请输入用户名");
	String Name=scanner.next();
	System.out.println("请输入密码");
	String cod=scanner.next();
	int b=0;
	for (int i = 0; i < user.length; i++) {
		if (user[i][0]=="null") {
			b=i;
		}
	}
	user[b][0]=bumen;
	user[b][1]=Name;
	user[b][2]=cod;
	user[b][3]="普通用户";
	System.out.println("注册成功");
	index();
}
private static void df() {//登录后首页
	System.out.println(user[a][1]+".欢迎您使用闽大书籍管理系统！！！");
	System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 返回上一级菜单");
	int zx=scanner.nextInt();
	switch (zx) {
	case 1:
		bokinfo();//图书管理
		break;
	case 2:
		press();//出版社管理
		break;
	case 3:
		f=false;
		
		break;
	case 4:
		index();
	break;
	default:
		break;
	}
}
private static void bokinfo() {//图书管理
	System.out.println("图书管理");
System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
int key=scanner.nextInt();
switch (key) {
case 1:
	increase();
	break;
case 2:
	bdel();
	break;
case 3:
	gaid();
	break;
case 4:
	tmnd();
	break;
case 5:
	df();
	break;
default:
	break;
}
}

private static void increase() {//图书增加（编码（ISBN）、书籍名称、价格、出版社、作者）
	int c=-1;
for (int i = 0; i < book.length; i++) {
	if (book[i][0]==null) {
		c=i;
		break;
	}
}
if (c>=0) {
	System.out.println("请输入编码（ISBN）");
	String df=scanner.next();
	for (int i = 0; i < book.length; i++) {
			if (df==book[i][0]) {
		System.out.println("编码错误或重复");
		bokinfo();
	}
	}
	book[c][0]=df;
	System.out.println("请输入书籍名称");
	book[c][1]=scanner.next();
	System.out.println("请输入价格");
	book[c][2]=scanner.next();
	System.out.println("请输入出版社");
	book[c][3]=scanner.next();
	System.out.println("请输入作者");
	book[c][4]=scanner.next();
}
bokinfo();
}
private static void bdel() {//图书删除
System.out.println("请输入要删除的书本名称：");
String bok= scanner.next();
boolean a=false;
for (int i = 0; i < book.length; i++) {
	if (bok.equals(book[i][1])) {
		a=true;
		for (int j = 0; j < book[i].length; j++) {
			book[i][j]=null;
			}
	}
}
if (a==true) {
		System.out.println("图书删除成功");
	}else {
		System.out.println("书本名称错误");
	}
bokinfo();
}
private static void gaid() {//图书更新
	System.out.println("请输入ISBN号：");
	String df = scanner.next();
	int s=0;
	for (int i = 0; i < book.length; i++) {
		if (df.equals(book[i][0])) {
			s=1;
	System.out.println("请输入新的书名：");
	book[i][1]=scanner.next();
	System.out.println("请输入新的价格：");
	book[i][2]=scanner.next();
	System.out.println("请输入新的出版社：");
	book[i][3]=scanner.next();
	System.out.println("请输入新的作者：");
	book[i][4]=scanner.next();
	System.out.println("更新成功！！！");
	
		}
	}	
	if (s==0) {
		System.out.println("该ISBN号不存在！！！");
	}
	bokinfo();
}
private static void tmnd() {//图书.查询
	System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
int key=scanner.nextInt();
switch (key) {
case 1://1.ISBN号查询
	System.out.println("请输入ISBN号：");
	String bok= scanner.next();
	int s=0;
	for (int i = 0; i < book.length; i++) {
		if (bok.equals(book[i][0])) {
			s=1;
	System.out.println("编码（ISBN）    书籍名称       价格     出版社     作者");
	System.out.println(Arrays.toString(book[i]));
		}
	}	
	if (s==0) {
		System.out.println("该ISBN号不存在！！！");
	}
	bokinfo();
	break;
case 2://.书名（模糊）查询
	tmbv();
	break;
case 3://出版社查询
	System.out.println("请输入出版社：");
	String bo= scanner.next();
	int sq=0;
	for (int i = 0; i < book.length; i++) {
		if (bo.equals(book[i][3])) {
			sq=1;
	System.out.println("编码（ISBN）    书籍名称       价格     出版社     作者");
	System.out.println(Arrays.toString(book[i]));
		}
	}	
	if (sq==0) {
		System.out.println("该出版社不存在！！！");
	}
	bokinfo();
	break;
case 4://作者 查询
	System.out.println("请输入作者：");
	String b= scanner.next();
	int sqr=0;
	for (int i = 0; i < book.length; i++) {
		if (b.equals(book[i][4])) {
			sqr=1;
	System.out.println("编码（ISBN）    书籍名称       价格     出版社     作者");
	System.out.println(Arrays.toString(book[i]));
		}
	}	
	if (sqr==0) {
		System.out.println("该作者不存在！！！");
	}
	bokinfo();
	break;
case 5://图书价格范围.查询
	pricerefer();
	break;
case 6://查询所有图书.查询
	for (int i = 0; i < book.length; i++) {
		if (book[i][0]!=null) {
			for (int k = 0; k < book[i].length; k++) {
				System.out.print(book[i][k]+"  ");	
		}
	System.out.println();		
	}	
	}
	bokinfo();
	break;
case 7://返回上一级
	bokinfo();
	break;
default:
	break;
}
}
private static void tmbv() {
	System.out.println("请输入书名：");
	String bookname = scanner.next();
	System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
	for (int i = 0; i < book.length; i++) {
		if (book[i][1] != null && book[i][1].indexOf(bookname)!=-1) {
			System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]);
		}
	}
}
private static void pricerefer() {//图书价格范围.查询
	System.out.println("请输入最低价格");
	int min = scanner.nextInt();
	System.out.println("请输入最高价格");
	int max = scanner.nextInt();
	for (int i = 0; i < book.length; i++) {
		if (Integer.parseInt(book[i][2])>min || Integer.parseInt(book[i][2])<max) {
			System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
			System.out.println(book[i][0]+'\t'+book[i][1]+'\t'+book[i][2]+'\t'+book[i][3]+'\t'+book[i][4]);
			break;
		}
	}
}
private static void press() {//出版社管理
	System.out.println("出版社管理");
System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
int key=scanner.nextInt();
switch (key) {
case 1:
	ati();
	break;
case 2:
	System.out.println("请输入要删除的出版社名称：");
	String bok= scanner.next();
	boolean a=false;
	for (int i = 0; i <press.length; i++) {
		if (bok==(press[i][0])) {
			a=true;
			for (int j = 0; j < press[i].length; j++) {
				press[i][j]=null;
				}
		}
	}
	if (a==true) {
			System.out.println("出版社删除成功");
		}else {
			System.out.println("出版社名称错误");
		}
	press();
	break;
case 3:
	System.out.println("请输入出版社名：");
	String df = scanner.next();
	int s=0;
	for (int i = 0; i <press.length; i++) {
		if (df.equals(press[i][0])) {
			s=1;
	System.out.println("请输入新的出版社名：");
	press[i][0]=scanner.next();
	System.out.println("请输入新的出版社地址：");
	press[i][1]=scanner.next();
	System.out.println("请输入新的出版社联系人：");
	press[i][2]=scanner.next();
		}
	}	
	if (s==0) {
		System.out.println("该出版社不存在！！！");
	}
	press();
	break;
case 4://根据出版社名称查询 
	System.out.println("请输入出版社：");
	String b= scanner.next();
	int sqr=0;
	for (int i = 0; i < press.length; i++) {
		if (b.equals(press[i][0])) {
			sqr=1;
	System.out.println("出版社名称       地址           联系人");
	System.out.println(Arrays.toString(press[i]));
		}
	}	
	if (sqr==0) {
		System.out.println("该出版社不存在！！！");
	}
	press();
	break;
case 5:
	System.out.println("出版社名称       地址           联系人");
	for (int i = 0; i <press.length; i++) {
		if (book[i][0]!=null) {
			for (int k = 0; k < press[i].length; k++) {
				System.out.print(press[i][k]+"  ");	
		}
	System.out.println();		
	}	
	}
	press();
	break;
case 6:
	df();
	break;
default:
	break;
}
}
public static void ati() {
	int c=-1;
	for (int i = 0; i < press.length; i++) {
		if (press[i][0]==null) {
			c=i;
		}
	}
	if (c>=0) {
		System.out.println("请输入出版社名");
		String df=scanner.next();
		for (int i = 0; i < press.length; i++) {
				if (df.equals(press[i][0])) {
			System.out.println("出版社名重复");
			press();
		}
		}
		press[c][0]=df;
		System.out.println("请输入出版社地址");
		press[c][1]=scanner.next();
		System.out.println("请输入出版社联系人");
		press[c][2]=scanner.next();
		System.out.println("添加成功");
	}
	press();
}
	
}
