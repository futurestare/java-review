package das;

import java.util.Iterator;
import java.util.Scanner;

public class Demo1 {

	static String arr[][]=new String [90][4];//登录用户的
	static String book[][]=new String [90][5];//图书管理
	static String press[][]=new String [90][3];//出版社管理
	static Scanner scan=new Scanner(System.in);
	public static void main(String[] args) {
		kaishi();
	}
	private static void kaishi() {
		while(true) {
			System.out.println("欢迎来到闽西管理系统");
			System.out.println("1.登录"+"2.注册");
			int key =scan.nextInt();
			switch (key) {
			case 1:
				user();
				break;
			case 2:
				password();
				break;

			default:
				break;
			}		
		}
		
	}
	private static void password() {//密码方法
		int index=-1;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i][0]==null) {
				index=i;
				break;
			}
		}
		System.out.println("请输入部门");
		arr[index][0]=scan.next();
		System.out.println("请输入用户名");
		arr[index][1]=scan.next();
		System.out.println("请输入密码");
		arr[index][2]=scan.next();
		
	}
	private static void user() {//登录方法
		System.out.println("请输入用户名");
		String user=scan.next();
		System.out.println("请输入密码");
		String password=scan.next();
		boolean k=false;
		for (int i = 0; i < arr.length; i++) {
			if (user.equals(arr[i][1])&&password.equals(arr[i][2])) {
				k=true;
			}
		}
		if (k) {
			System.out.println("登录成功");
			//登录进去后
			afterlogin();
		}
		else {
			System.out.println("登录失败");
		}		
	}
	private static void afterlogin() {//登录成功后的
		System.out.println("欢迎使用闽大图书系统");
		System.out.println("1.图书管理"+"2.出版社管理"+"3.退出登录"+"4.退出系统");
		int kk =scan.nextInt();
		switch (kk) {
		case 1:
			//进入图书管理
			bookscoll();
			break;
		case 2:
			//进入出版社管理
			pressscoll();
			
			break;
		case 3:
			System.out.println("退出成功");
			kaishi();
		
			break;
		case 4:
				System.out.println("系统退出");
				
			break;

		default:
			break;
		}
		
	}
	private static void pressscoll() {//进入出版社管理
		System.out.println("1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int key=scan.nextInt();
		switch (key) {
		case 1:
			presszengjia();
			break;
		case 2:
			
			break;
		case 3:
			
			break;
		case 4:
			
			break;
		case 5:
			
			break;
		case 6:
			
			break;
		default:
			break;
		}
		
	}
	private static void presszengjia() {//出版社增加
		int index=-1;
		for (int i = 0; i < press.length; i++) {
			if (press[i][0]==null) {
				index=i;
				break;
			}
		}
		System.out.println("请输入出版社名称:");
		press[index][0]=scan.next();
		System.out.println("请输入出版社地址:");
		press[index][1]=scan.next();
		System.out.println("请输入出版社联系人:");
		press[index][2]=scan.next();
		System.out.println("出版社添加成功");
		boolean rea =addpress(press[index][0]);
		if (rea) {
			System.out.println("重复，添加失败");
		}else {
			System.out.println("添加成功");
		}
		afterlogin();
		
	}
	private static boolean addpress(String string) {//出版社增加??
		boolean str =false;
		for (int i = 0; i < press.length; i++) {
			if (string.equals(press[i][0])) {
				str=true;
				break;
			}
		}
		return str;
	}
	private static void bookscoll() {//进入图书管理
		System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
		int wq=scan.nextInt();
		switch(wq) {
		case 1:
			increase();
			break;
		case 2:
			delete();
			break;
		case 3:
			update();
			break;
		case 4://后面需要再写
			System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
			
			break;
		case 5:
			afterlogin();
			break;
		default:
			break;
		}
		
	}
	private static void update() {//更新
		System.out.println("请输入ISBN号：");
		String ISBN = scan.next();
		int index = -1 ;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0].equals(ISBN)) {
				index = i;
				break;
			}
		}
		System.out.println("请输入新的书名:");
		book[index][1] = scan.next();
		System.out.println("请定义新的价格:");
		book[index][2] = scan.next();
		System.out.println("请输入新的出版社:");
		book[index][3] = scan.next();
		System.out.println("请输入新的作者:");
		book[index][4] = scan.next();
		System.out.println("更新成功！！！");
		
	}
	private static void delete() {//图书管理删除
		System.out.println("请输入要删除的书本名称：");
		String bookname=scan.next();
		int index=-1;
		for (int i = 0; i < book.length; i++) {
			if (bookname.equals(book[i][0])) {
				index=i;
				book[index][0] = null;
				book[index][1] = null;
				book[index][2] = null;
				book[index][3] = null;
				book[index][4] = null;
				
				System.out.println("删除成功！！！");
				break;
			}else {
				System.out.println("没有找到该书!,删除失败");
				break;
			}
		}
		bookscoll();
		
	}
//	private static boolean deletecheck(String bookname) {//删除?
//		boolean Flag = false;
//		for (int i = 0; i < book.length; i++) {
//			if (bookname.equals(book[i][1])) {//null
//				Flag=true;
//				break;
//			}
//		}
//		return false;
//	}
	private static void increase() {//图书管理增加
		int index=-1;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]==null) {
				index=i;
				break;
			}
		}
		while(true) {
			
			System.out.println("请输入图书ISBN:");
			book[index][0]=scan.next();
			System.out.println("请输入书名");
			book[index][1]=scan.next();	
			System.out.println("请输入价格:");
			book[index][2] = scan.next();
			System.out.println("请输入出版社:");
			book[index][3] = scan.next();
			System.out.println("请输入作者:");
			book[index][4] = scan.next();
			boolean result = addcheck(book[index][1]);
			if (result) {
				System.out.println("添加失败！！！该书已经存在！！！");
			}else {
				System.out.println("添加成功！！！");
			}
			bookscoll();
		}
			
		
	}
	private static boolean addcheck(String string) {//图书管理找重复的
		boolean flag =false;
		for (int i = 0; i < book.length; i++) {
			if (string.equals(book[i][1])) {
				flag=true;
				break;
			}
		}
		return false;
	}

}