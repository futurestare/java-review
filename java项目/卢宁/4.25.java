package com.md.lession;

import java.util.Scanner;

public class Demo17 {
	
	static Scanner scanner = new Scanner(System.in);
	static String[][] users = new String[20][4];
	static String[][] books = new String[20][5];
	static String[][] pubs = new String[20][3];
	static String loginUser = "";

	public static void main(String[] args) {
		init();
		index();
	}
	private static void index() {
		System.out.println("亲爱的主人，欢迎来到闽大图书管理系统");
		System.out.println("1.登录  2.注册");
		
		while (true) {
			int key = scanner.nextInt();
			
			switch (key) {
			case 1:
				loginModule();
				break;
			case 2:
				registeredModule();
				break;
				default:
				break;
			}				
		}
	}
	
	private static void loginModule() {
		System.out.println("请输入用户名：");
		String userName = scanner.next();
		System.out.println("请输入密码：");
		String password = scanner.next();		
		
		boolean flag = false;
		for (int i = 0; i < users.length; i++) {
			if (userName.equals(users[i][1]) && password.equals(users[i][2])) {
				flag = true ; 
			}
			loginUser = userName;
		}
		if (flag) {
			System.out.println("登录成功！");
			homePage();

		}else {
			System.out.println("登录失败，请重试！！！");
		}
	}
	
	private static void registeredModule() {
		
		int index =-1;
		for (int i = 0; i < users.length; i++) {
			if (users[i][0]==null) {
				index = i ;
				break;
			}
		}
		
		System.out.println("请输入部门：");
		String deptName = scanner.next();
		users[index][0]=deptName;
		System.out.println("请输入用户名：");
		String userName = scanner.next();
		users[index][1]=userName;
		System.out.println("请输入密码：");
		String password = scanner.next();		
		users[index][2]=password;			
		users[index][3]="普通用户";
		System.out.println("注册成功！！！");

		index();
	}
	
	private	static void homePage() {
		System.out.println(loginUser +"\t"+"亲爱的主人，欢迎来到闽大图书管理系统");
		System.out.println("请输入数字进行选择：");
		System.out.println("1.图书管理  2.出版社管理  3.退出登录  4.退出系统");
		
		int key2 = scanner.nextInt();
		
		switch (key2) {
		case 1:
			bookmanage();
			break;
		case 2:
			press();
			break;
		case 3:
			logout();
			break;					
		case 4:
			exit();
			break;	
		default:
			break;
		}
	}
	private static void exit() {
		//退出系统
		System.out.println("系统退出成功！");
		System.exit(0);
	}
	private static void logout() {
		//退出登录
		System.out.println("退出登录成功！");
		main(null);
	}
	private static void press() {
		//出版社管理
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int key =scanner.nextInt();
		switch (key) {
		case 1:
			//1.增加
		pressadd(); 
		break;
		case 2:
			//2.删除
		pressDelete();
			break;
		case 3:
			//3.更新
			pressDate();
			break;
		case 4:
			//4.根据出版社名称查询 
			 NameQuery();
			break;
		case 5:
			//5.查询所有出版社
			pressinQuire();
			break;
		case 6:
			//6.返回上一级菜单
			homePage();
			break;
		default:
			break;
		}
	}
	private static void pressDate() {
		//更新
		System.out.println("请输入出版社名称：");
		String a = scanner.next();
		for (int i = 0; i < pubs.length; i++) {
			if(a.equals(pubs[i][0])) {
				System.out.println("出版社名称"+'\t'+"地址"+'\t'+"联系人");
				System.out.println(pubs[i][0]+'\t'+pubs[i][1]+'\t'+pubs[i][2]);
				System.out.println("请输入要更新的出版社名称：");
				String pressname = scanner.next();
				System.out.println("请输入要更新的地址：");
				String pressaddress = scanner.next();
				System.out.println("请输入要更新的联系人姓名：");
				String presslinkman = scanner.next();
				System.out.println("更新完成！");
				pubs[i][0] = pressname;
				pubs[i][1] = pressaddress;
				pubs[i][2] = presslinkman;
				System.out.println("出版社名称"+'\t'+"地址"+'\t'+"联系人");
				System.out.println(pubs[i][0]+'\t'+pubs[i][1]+'\t'+pubs[i][2]);
				press();
			}
		}
	}
	private static void NameQuery() {
		//根据出版社名称查询 
		System.out.println("请输入出版社的名字：");
		String pubsname = scanner.next();
		for (int i = 0; i < pubs.length; i++) {
			if(pubsname.equals(pubs[i][0])) {
				System.out.println("出版社名称："+'\t'+pubs[i][0]);
				System.out.println("出版社地址："+'\t'+pubs[i][1]);
				System.out.println("出版社联系人："+'\t'+pubs[i][2]);
			}
		}
	}
	private static void pressinQuire() {
		//查询所有出版社
		System.out.println("出版社名称"+'\t'+"地址"+'\t'+"联系人");
		for (int i = 0; i < pubs.length; i++) {
			if(pubs[i][0]!=null) {
				System.out.println(pubs[i][0]+'\t'+pubs[i][1]+'\t'+pubs[i][2]);
			}
		}
	}
	
	private static void pressDelete() {
		//删除
		System.out.println("请输入要删除的出版社名称：");
		String pubsname = scanner.next();
		for (int i = 0; i < pubs.length; i++) {
			if (books[i][3]!=null && pubsname.equals(books[i][3])) {
				System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！");
				System.out.println("删除失败!!!");
				break;
			}
		if (pubsname.equals(pubs[i][0])) {
			pubs[i][0] = null;
			pubs[i][1] = null;
			pubs[i][2] = null;
			System.out.println("删除成功");
			break;
		}
		}
	}
	private static void pressadd() {
		//增加
		pressOL();
		System.out.println("请输入出版社名称:");
		String pressName=scanner.next();
		System.out.println("请输入出版社地址：");
        String pressDress = scanner.next();
        System.out.println("请输入出版社联系人：");
        String presscall= scanner.next();
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] == null) {
            	books[i][0] = pressName;
            	books[i][1] = pressDress;
            	books[i][2] = presscall;
            }
        }
        System.out.println("添加出版社成功！");
	}
        private static void pressOL() {
        	books[0][0]="闽大出版社";
        	books[0][1]="闽大路";
        	books[0][2]="张三";
			
		}
	private static void bookmanage() {
		//图书管理系统
		System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
		int key =scanner.nextInt();
		switch (key) {
		case 1:
			//1.增加 
			bookadd(); 
			break;
		case 2:
			 //2.删除
			bookDelete();
			break;
		case 3:
			// 3.更新
			bookupDate();
			break;
		case 4:
			// 4.查询
			 bookinQuire();
			break;
		case 5:
			//5.返回上一级菜单
			homePage();
		}
		
	}
	private static void bookDelete() {
		//删除
		System.out.println("请输入要删除的书名：");
		String bookname = scanner.next();
		for (int i = 0; i < books.length; i++) {init();
			if(bookname.equals(books[i][1])) {
				books[i][0] = null;
				books[i][1] = null;
				books[i][2] = null;
				books[i][3] = null;
				books[i][4] = null;
				System.out.println(books[i][0]+books[i][1]);
				System.out.println("删除成功");
				bookDelete();
			}
		}
		System.out.println("删除错误或查无此书！请重新执行！");
		bookDelete();
	}
	private static void bookadd() {
		//增加
		int index = -1;
		for (int i = 0; i < books.length; i++) {
			if(books[i][0] == null) {
				index = i;
			}
		}
		System.out.println("请输入图书ISBN:");
		books[index][0] = scanner.next();
		System.out.println("请输入书名:");
		books[index][1] = scanner.next();
		System.out.println("请输入价格:");
		books[index][2] = scanner.next();
		System.out.println("请输入出版社:");
		books[index][3] = scanner.next();
		System.out.println("请输入作者:");
		books[index][4] = scanner.next();
		System.out.println("添加成功！");
	}
	
	private static void bookupDate() {
		//更新
		System.out.println("请输入ISBN号：");
		String a = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if(a.equals(books[i][0])) {
				System.out.println("请输入要新的书名：");
				books[i][1] = scanner.next();
				System.out.println("请输入要新的价格：");
				books[i][2] = scanner.next();
				System.out.println("请输入要新的出版社：");
				books[i][3] = scanner.next();
				System.out.println("请输入要新的作者：");
				books[i][4] = scanner.next();
				System.out.println("更新成功！");
				bookadd();
			}
		}
		System.out.println("更新错误！请重新执行");
		bookadd();
	}
	private static void bookinQuire() {
		//查询
		while (true) {
			System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
			int b = scanner.nextInt();
			switch (b) {
			case 1:
				//1.isbn
				Isbnrefer();
				break;
			case 2:
				//2.书名（模糊）
				booknamerefer();
				break;
			case 3:
				//3.出版社
				bookpressrefer();
				break;
			case 4:
				//4. 作者
				writerrefer();
				break;
			case 5:
				//5. 价格范围
				pricerefer();
				break;
			case 6:
				//6.查询所有
				referall();
				break;
			case 7:
				//7.返回上一级
				bookmanage();
				break;

			default:
				break;
			}
		}
		
	}
	private static void Isbnrefer() {
		//1.isbn
		System.out.println("请输入ISBN号：");
		String is = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if (is.equals(books[i][0])) {
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
				System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
			}
		}
	}
	private static void booknamerefer() {
		//2.书名（模糊）
		System.out.println("请输入书名：");
		String booknamerefer = scanner.next();
		System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < books.length; i++) {
			if (books[i][1] != null && books[i][1].indexOf(booknamerefer)!=-1) {
				System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
			}
		}
	}
	private static void bookpressrefer() {
		//3.出版社
		System.out.println("请输入出版社：");
		String bookpressrefer = scanner.next();
		System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < books.length; i++) {
			if (bookpressrefer.equals(books[i][3])) {
				System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
			}
		}
	}
	private static void writerrefer() {
		//4. 作者
		System.out.println("请输入作者姓名：");
		String writerrefer = scanner.next();
		for (int i = 0; i < books.length; i++) {
			if (writerrefer.equals(books[i][4])) {
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
				System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
			}
		}
	}
	private static void pricerefer() {
		//5. 价格范围
		System.out.println("请输入最低价格");
		int min = scanner.nextInt();
		System.out.println("请输入最高价格");
		int max = scanner.nextInt();
		for (int i = 0; i < books.length; i++) {
			if (Integer.parseInt(books[i][2])>min || Integer.parseInt(books[i][2])<max) {
				System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
				System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
				break;
			}
		}
	}
	private static void referall() {
		//6.查询所有
		int index = getbooks();
		System.out.println("ISBN"+'\t'+"书名"+'\t'+"价格"+'\t'+"出版社"+'\t'+"作者");
		for (int i = 0; i < index; i++) {
			System.out.println(books[i][0]+'\t'+books[i][1]+'\t'+books[i][2]+'\t'+books[i][3]+'\t'+books[i][4]);
		}
		
	}
	private static int getbooks() {
		int index = -1;
		for (int i = 0; i < books.length; i++) {
			if (books[i][0] == null) {
				index = i;
				break;
			}
		}
		return index;
	}
	private static void init() {
		users[0][0]="技术部";
		users[0][1]="ln";
		users[0][2]="123456";
		users[0][3]="管理员";
		
		users[1][0]="前端部";
		users[1][1]="lun";
		users[1][2]="123";
		users[1][3]="普通人员";
		
		users[2][0] = "前端部";
		users[2][1] = "admin";
		users[2][2] = "123";
		users[2][3] = "高层官员";
		
		books[0][0] = "001";
		books[0][1] = "青铜葵花";
		books[0][2] = "38.00";
		books[0][3] = "新华书社";
		books[0][4] = "曹文轩";
		
		books[1][0] = "002";
		books[1][1] = "狗牙雨";
		books[1][2] = "36.00";
		books[1][3] = "老舍书社";
		books[1][4] = "曹文轩";
		
		books[2][0] = "003";
		books[2][1] = "毛球";
		books[2][2] = "54.00";
		books[2][3] = "老舍书社";
		books[2][4] = "风味儿";
		
		books[3][0] = "004";
		books[3][1] = "钢铁是怎样练成的";
		books[3][2] = "1000.00";
		books[3][3] = "新华书社";
		books[3][4] = "八个人";
		
		pubs[0][0] = "新华书社";
		pubs[0][1] = "龙岩新罗区";
		pubs[0][2] = "李四";
		
		pubs[1][0] = "书斋";
		pubs[1][1] = "闽大出版社";
		pubs[1][2] = "浩克";
		
}

}
