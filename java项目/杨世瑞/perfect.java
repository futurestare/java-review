package 书籍管理系统1;

import java.util.Arrays;
import java.util.Scanner;

public class perfect {

	static String[][] userinfo = new String[5][4];// 用户信息 && 部门、用户名、密码、用户角色
	static String[][] booksinfo = new String[20][5];// 书籍信息 && 编码（ISBN）、书籍名称、价格、出版社、作者
	static String[][] pressinfo = new String[10][3];// 出版社信息 && 出版社名称、地址、联系人
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		CS(); // 录入初始用户信息
		boolean DLCGPD = false;// ****登录成功判定
		boolean ZCCGPD = false;// ****注册成功判定
		// 部门、用户名、密码、用户角色
		System.out.println("欢迎来到图书管理系统：");
		System.out.println("1.登录  2.注册");
		int a2 = sc.nextInt();
		if (a2 == 1) {
			DLCGPD = !DL(); // 登录
		} else if (a2 == 2) {
			ZCCGPD = ZC(); // 注册
		} else {
			System.out.println("请输入正确的选项编号");
			main(args);
		}
		if (DLCGPD | ZCCGPD) {
			CD();
		}
	}
//功能

//*******************以下为方法*************************

//登陆
	public static boolean DL() {
		boolean DLPD = true;// *******DLPD==是否成功登陆判断
		System.out.println("请输入用户名：");
		String DLusername = sc.next();
		System.out.println("请输入密码：");
		String DLpassworld = sc.next();
		for (int i = 0; i < 5; i++) {
			if (DLusername.equals(userinfo[i][1]) && DLpassworld.equals(userinfo[i][2])) {
				System.out.println("登陆成功！");
				DLPD = false;
				CD();
				break;
			}
		}
		if (DLPD) {
			System.out.println("账号或密码错误");
			System.out.println("1.重新输入   2.前往注册");
			int DLSB = sc.nextInt();// ***********DLSB=登陆失败
			if (DLSB == 1) {
				DL();
			} else if (DLSB == 2) {
				ZC();
			} else {
				System.out.println("请输入正确的数字选项");
			}
		}
		return DLPD;
	}

//注册
	public static boolean ZC() {
		boolean ZCPD = false;// **********是否注册成功判断
		System.out.println("请输入部门：");
		String BM = sc.next();
		System.out.println("请输入账号：");
		String zcusername = sc.next();
		System.out.println("请输入密码：");
		String zcpassworld = sc.next();
		System.out.println("请输入用户身份：");
		String SF = sc.next();
		for (int i = 0; i < 5; i++) {
			for (int j = 0; j < 4; j++) {
				if (userinfo[i][j] == null) {
					userinfo[i][0] = BM;
					userinfo[i][1] = zcusername;
					userinfo[i][2] = zcpassworld;
					userinfo[i][3] = SF;
					ZCPD = true;
					break;
				}
			}
			if (ZCPD) {
				System.out.println("注册成功,已为你登录！");
				CD();
				break;
			}
		}
		return ZCPD;
	}

//*******************功能
	// 菜单
	public static void CD() {
		System.out.println("1.图书管理    2.出版社管理    3.退出登录   4.退出系统");
		int CD = sc.nextInt();
		if (CD == 1) {
			TSGL();
		} else if (CD == 2) {
			CBSGL();
		} else if (CD == 3) {
			main(null);
		} else if (CD == 4) {
			System.out.println("系统已退出！");
		}
	}

	// 图书管理
	public static void TSGL() {// ********TSGL==图书管理
		System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");// ***(增刪改)
		int add = sc.nextInt();// ************************增加
		if (add == 1) {
			System.out.println("请输入图书ISBN:");
			String ISBN = sc.next();
			System.out.println("请输入书名:");
			String bookname = sc.next();
			System.out.println("请输入价格:");
			String money = sc.next();
			System.out.println("请输入出版社:");
			String press = sc.next();
			System.out.println("请输入作者:");
			String name = sc.next();
			for (int k = 0; k < booksinfo.length; k++) {
				if (booksinfo[k][0] == null) {
					booksinfo[k][0] = ISBN;
					booksinfo[k][1] = bookname;
					booksinfo[k][2] = money;
					booksinfo[k][3] = press;
					booksinfo[k][4] = name;
					System.out.println("添加成功！");
					TSGL();
					break;
				}
			}
		} else if (add == 2) {
			System.out.println("请输入要删除的书名：");// *******删除
			String name = sc.next();
			for (int i = 0; i < 20; i++) {
				if (name.equals(booksinfo[i][1])) {
					booksinfo[i][0] = null;
					System.out.println("刪除成功！");
					TSGL();
				}
			}
		} else if (add == 3) {
			boolean XGPD = true;
			System.out.println("请输入ISBN号：");// **********修改
			String ISBN = sc.next();
			System.out.println("请输入新的书名：");
			String bookname = sc.next();
			System.out.println("请输入新的价格：");
			String money = sc.next();
			System.out.println("请输入新的出版社：");
			String press = sc.next();
			System.out.println("请输入新的作者：");
			String name = sc.next();
			for (int i = 0; i < 20; i++) {
				if (ISBN.equals(booksinfo[i][0])) {
					booksinfo[i][1] = bookname;
					booksinfo[i][2] = money;
					booksinfo[i][3] = press;
					booksinfo[i][4] = name;
					System.out.println("更新成功！");
					XGPD = false;
					TSGL();
					break;
				}
			}
			if (XGPD) {
				System.out.println("更新失败，请输入正确的ISBN号！");
			}
		} else if (add == 4) {
			CX();// *********转到查询
		} else if (add == 5) {
			CD();
		}
	}

	public static void CX() { // ****************查
		System.out.println("1.根据ISBN查询  2.根据书名查询（模糊）  3.根据出版社查询  4.根据作者查询   5.根据价格范围查询   6.查询所有书籍信息   7.返回上一级菜单");
		int cx = sc.nextInt();
		if (cx == 1) {
			System.out.println("请输入需要查询的书的ISBN：");// ****根据ISBN查询
			String ISBN = sc.next();
			System.out.println("编码（ISBN）    书籍名称      价格     出版社     作者");
			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 5; j++) {
					if (booksinfo[i][0] != null && booksinfo[i][0].equals(ISBN)) {
						if (j == 4) {
							System.out.println(booksinfo[i][j] + "    ");
						} else {
							System.out.print(booksinfo[i][j] + "    ");
						}
					}
				}
			}
			System.out.println("");
			CX();
		} else if (cx == 2) {

			System.out.println("请输入需要查询的书的名字(模糊查询)：");
			String bookname = sc.next();
			System.out.println("编码（ISBN）    书籍名称      价格     出版社     作者");
			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 5; j++) {
					if (booksinfo[i][0] != null && booksinfo[i][1].indexOf(bookname) != -1) {
						if (j == 4) {
							System.out.println(booksinfo[i][j] + "    ");
						} else {
							System.out.print(booksinfo[i][j] + "    ");
						}
					}
				}
			}
			System.out.println("");
			CX();
		} else if (cx == 3) {
			System.out.println("请输入需要查询的出版社：");// ****根据出版社查询
			String pressname = sc.next();
			System.out.println("编码（ISBN）    书籍名称      价格     出版社     作者");
			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 5; j++) {
					if (booksinfo[i][0] != null && pressname.equals(booksinfo[i][3])) {
						if (j == 4) {
							System.out.println(booksinfo[i][j] + "    ");
						} else {
							System.out.print(booksinfo[i][j] + "    ");
						}
					}
				}
			}
			System.out.println("");
			CX();

		} else if (cx == 4) {

			System.out.println("请输入需要查询的作者：");// ******根据作者查询
			String name = sc.next();
			System.out.println("编码（ISBN）    书籍名称      价格     出版社     作者");
			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 5; j++) {
					if (booksinfo[i][0] != null && name.equals(booksinfo[i][4])) {
						if (j == 4) {
							System.out.println(booksinfo[i][j] + "    ");
						} else {
							System.out.print(booksinfo[i][j] + "    ");
						}
					}
				}
			}
			System.out.println("");
			CX();

		} else if (cx == 5) {
			System.out.println("请输入最低价格:");// *****根据价格区间查询
			int min = sc.nextInt();
			System.out.println("请输入最高价格：");
			int max = sc.nextInt();
			System.out.println("编码（ISBN）    书籍名称      价格     出版社     作者");
			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 5; j++) {
					if (booksinfo[i][0] != null) {
						int money = Integer.parseInt(booksinfo[i][2]);
						if (money >= min && money <= max) {

							if (j == 4) {
								System.out.println(booksinfo[i][j] + "    ");
							} else {
								System.out.print(booksinfo[i][j] + "    ");
							}
						}
					}
				}
			}
			System.out.println("");
			CX();

		} else if (cx == 6) {
			System.out.println("编码（ISBN）    书籍名称      价格     出版社     作者");// ****查询所有书籍信息
			for (int i = 0; i < 20; i++) {
				for (int j = 0; j < 5; j++) {
					if (booksinfo[i][0] != null) {
						if (j == 4) {
							System.out.println(booksinfo[i][j] + "    ");
						} else {
							System.out.print(booksinfo[i][j] + "    ");
						}
					}
				}
			}
			System.out.println("");
			CX();
		} else if (cx == 7) {
			TSGL();
		}
	}

	// 出版社管理
	public static void CBSGL() {
		System.out.println("请输入：1.增加   2.删除   3.更新   4.根据出版社名称查询   5.查询所有出版社   6.返回上一级菜单");
		int CBS = sc.nextInt();
		if (CBS == 1) {
			System.out.println("请输入出版社名称：");// ********增加
			String cbsname = sc.next();
			System.out.println("请输入出版社地址：");
			String cbsdz = sc.next();
			System.out.println("请输入出版社联系人：");
			String lxrname = sc.next();
			for (int k = 0; k < 10; k++) {
				if (pressinfo[k][0] == null) {
					pressinfo[k][0] = cbsname;
					pressinfo[k][1] = cbsdz;
					pressinfo[k][2] = lxrname;
					System.out.println("出版社添加成功！");
					CBSGL();
					break;
				}
			}
		} else if (CBS == 2) {
			System.out.println("请输入要删除的出版社名称：");// ***********删除
			String cbsname = sc.next();
			for (int i = 0; i < 20; i++) {
				if (cbsname.equals(pressinfo[i][0])) {
					pressinfo[i][0] = null;
					System.out.println("出版社刪除成功！");
					CBSGL();
				}
			}
		} else if (CBS == 3) {
			boolean CBSTJPD = true; // **********CBSTJPD=出版社添加成功判定
			System.out.println("请输入出版社名称：");// **********修改
			String cbsname = sc.next();
			System.out.println("请输入新的出版社名：");
			String newcbsname = sc.next();
			System.out.println("请输入新的地址：");
			String cbsdz = sc.next();
			System.out.println("请输入新的联系人：");
			String cbslxr = sc.next();
			for (int i = 0; i < 10; i++) {
				if (cbsname.equals(pressinfo[i][0])) {
					pressinfo[i][0] = newcbsname;
					pressinfo[i][1] = cbsdz;
					pressinfo[i][2] = cbslxr;
					System.out.println("出版社信息更新成功！");
					CBSTJPD = false;
					CBSGL();
					break;
				}
			}
			if (CBSTJPD) {
				System.out.println("更新失败，请输入正确的出版社名！");
				CBSGL();
			}

		} else if (CBS == 4) {
			boolean CBSCX;
			CBSCX = false;
			System.out.println("请输入出版社名称：");// **************根据出版社名称查询
			String cxcbsname = sc.next();// cxcbsname=查询出版社名称
			System.out.println("出版社名称    出版社地址     联系人");
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 3; j++) {
					if (booksinfo[i][0] != null && cxcbsname.equals(pressinfo[i][0])) {
						if (j == 2) {
							System.out.println(pressinfo[i][j] + "    ");
						} else {
							System.out.print(pressinfo[i][j] + "    ");
						}
						CBSCX = true;
					}

				}
			}
			if (CBSCX) {
				System.out.println("查询成功！");
			} else {
				System.out.println("查询失败，请输入正确的出版社名!");
			}
			CBSGL();
		} else if (CBS == 5) {
			System.out.println("出版社名称       出版社地址        联系人");
			for (int i = 0; i < 10; i++) {
				for (int j = 0; j < 3; j++) {
					if (pressinfo[i][0] != null) {
						if (j == 2) {
							System.out.println(pressinfo[i][j] + "    ");
						} else {
							System.out.print(pressinfo[i][j] + "    ");
						}
					}
				}
			}
			CBSGL();
		} else if (CBS == 6) {
			CD();
		}

	}

//***********************************数据初始化
	public static void CS() {
		userinfo[0][0] = "摸鱼部";
		userinfo[0][1] = "YSR";
		userinfo[0][2] = "1";
		userinfo[0][3] = "超级管理员"; // 录入初始用户

		booksinfo[0][0] = "1";
		booksinfo[0][1] = "老人与海";
		booksinfo[0][2] = "75";
		booksinfo[0][3] = "上海译文出版社";
		booksinfo[0][4] = "海明威";

		booksinfo[1][0] = "2";
		booksinfo[1][1] = "海底两万里";
		booksinfo[1][2] = "80";
		booksinfo[1][3] = "新华出版社";
		booksinfo[1][4] = "儒勒·凡尔纳";

		booksinfo[2][0] = "3";
		booksinfo[2][1] = "鲁滨逊漂流记";
		booksinfo[2][2] = "90";
		booksinfo[2][3] = "清华出版社";
		booksinfo[2][4] = "丹尼尔·笛福";

		booksinfo[3][0] = "4";
		booksinfo[3][1] = "百万英镑";
		booksinfo[3][2] = "70";
		booksinfo[3][3] = "新华出版社";
		booksinfo[3][4] = "马克·吐温"; // 录入初始书籍

		pressinfo[0][0] = "上海译文出版社";
		pressinfo[0][1] = "上海";
		pressinfo[0][2] = "张三";

		pressinfo[1][0] = "新华出版社";
		pressinfo[1][1] = "北京";
		pressinfo[1][2] = "李四";

		pressinfo[2][0] = "清华出版社";
		pressinfo[2][1] = "北京";
		pressinfo[2][2] = "王五"; // 录入初始出版社信息

	}

}