package com.day.demo;

import java.util.Scanner;

public class ManagerEntry {
    static String loginUser = "";
    static Scanner sc = new Scanner(System.in);
    //	用户信息（部门、用户名、密码、用户角色）
    static String[][] users = new String[100][4];
    //	书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）
    static String[][] books = new String[5][5];
    //	出版社信息（出版社名称、地址、联系人）
    static String[][] press = new String[5][3];

    //一级菜单（登录前）
    public static void main(String[] args) {
        setup();
//		System.out.println(Arrays.toString(users[0]));
        lo:
        while (true) {

            System.out.println("欢迎使用<图书信息>管理系统！");
            System.out.println("1.登录\t2.注册\t3.退出");

            String choice = sc.next();
            switch (choice) {
                case "1":
//				System.out.println("登录");
                    login();
                    break;
                case "2":
//				System.out.println("注册");
                    register();
                    break;
                case "3":
                    System.out.println("感谢您的使用，再见！");
                    break lo;
                default:
                    System.out.println("输入错误，请重新输入！");
                    break;
            }
        }
    }

    //注册
    private static void register() {
        System.out.println("请输入用户部门：");
        String userBranch = sc.next();
        String userName;
        while (true) {
            System.out.println("请输入用户名");
            userName = sc.next();
            boolean flag = isUserExists(userName);
            if (flag) {
                System.out.println("用户名已存在，请重新输入！");
            } else {
                break;
            }
        }
        System.out.println("请输入用户密码：");
        String userPassword = sc.next();
        for (int i = 0; i < users.length; i++) {
            if (users[i][0] == null) {
                users[i][0] = userBranch;
                users[i][1] = userName;
                users[i][2] = userPassword;
                users[i][3] = "普通用户";
                break;
            }
        }
        System.out.println("注册成功:)");
    }

    //登录
    private static void login() {
        String userName;
        while (true) {
            System.out.println("请输入用户名");
            userName = sc.next();
            boolean flag = isUserExists(userName);
            if (flag) {
                break;
            } else {
                System.out.println("用户名不存在，请重新输入！");
            }
        }
        int Index = getUserIndex(userName);
        String userPassword;
        for (int i = 1; i <= 3; i++) {
            System.out.println("请输入密码，您还有" + (4 - i) + "次机会");
            userPassword = sc.next();
            if (userPassword.equals(users[Index][2])) {
                System.out.println("登录成功！");
                loginUser = userName;
                menu();
                return;
            } else {
                System.out.println("密码错误，请重新输入！");
            }
        }
        System.out.println("错误次数过多，系统自动退出，再见！-_-");
        System.exit(0);
    }

    //二级菜单（登录后）
    private static void menu() {
        lo:
        while (true) {
            System.out.println(loginUser + ",欢迎回来^_^");
            System.out.println("1.图书管理\t2. 出版社管理\t3. 退出登录\t4.退出系统");
            String choice = sc.next();
            switch (choice) {
                case "1":
//				System.out.println("图书管理");
                    bookMenu();
                    break;
                case "2":
//				System.out.println("出版社管理");
                    pressMenu();
                    break;
                case "3":
                    System.out.println("已退出登录！");
                    break lo;
                case "4":
                    System.out.println("感谢您使用图书管理系统，再见！");
                    System.exit(0);
                default:
                    System.out.println("输入错误请重新输入");
                    break;
            }
        }
    }

    //三级菜单（出版社）
    private static void pressMenu() {
        while (true) {
            System.out.println("欢迎使用<出版社>管理系统");
            System.out.println("请输入：\t1.增加\t2.删除\t3.更新\t4.根据出版社名称查询\t5.查询所有出版社\t6.返回上级菜单");
            String choice = sc.next();
            switch (choice) {
                case "1":
//                    System.out.println("增加");
                    addPressInfo();
                    break;
                case "2":
//                    System.out.println("删除");
                    deletePressInfoByName();
                    break;
                case "3":
//                    System.out.println("更新");
                    updatePressInfoByName();
                    break;
                case "4":
//                    System.out.println("根据出版社名称查询");
                    getPressInfoByName();
                    break;
                case "5":
//                    System.out.println("查询所有出版社");
                    getAllPressInfo();
                    break;
                case "6":
                    return;
                default:
                    System.out.println("输入错误，请重新输入");
                    break;
            }
        }
    }

    private static void updatePressInfoByName() {
        if (isPressNull()){
            return;
        }
        String pressName;
        while (true) {
            System.out.println("请输入出版社名称：");
            pressName = sc.next();
            boolean exists = isPressExists(pressName);
            if (exists) {
                break;
            } else {
                System.out.println("您输入的编码不存在，请重新输入！");
            }
        }
        System.out.println("请输入出版社地址：");
        String pressAddress = sc.next();
        System.out.println("请输入出版社联系人：");
        String pressBoss = sc.next();

        for (int i = 0; i < press.length; i++) {
            if (press[i][0]!=null&& pressName.equals(press[i][0])){
                press[i][1]=pressAddress;
                press[i][2]=pressBoss;
                System.out.println("修改成功！");
                return;
            }
        }

    }

    private static void deletePressInfoByName() {
        if (isPressNull()){
            return;
        }
        String pressName;
        while (true) {
            System.out.println("请输入出版社名称：");
            pressName = sc.next();
            boolean exists = isPressExists(pressName);
            if (exists) {
                break;
            } else {
                System.out.println("您输入的编码不存在，请重新输入！");
            }
        }
        for (int i = 0; i < books.length; i++) {
            if (books[i][3] != null && pressName.equals(books[i][3])) {
                System.out.println("出版社有关联的图书禁止删除！");
                return;
            }
        }
        for (int i = 0; i < press.length; i++) {
            if (press[i][0] != null && pressName.equals(press[i][0])) {
                for (int j = 0; j < press[i].length; j++) {
                    press[i][j]=null;
                }
            }
        }
        System.out.println("删除成功！");
    }

    private static void getAllPressInfo() {
        if (isPressNull()){
            return;
        }
        System.out.println("出版社名称\t地址\t联系人");
        for (int i = 0; i < press.length; i++) {
            for (int j = 0; j < press[i].length; j++) {
                if (press[i][j] != null) {
                    System.out.print(press[i][j] + "\t");
                    if(j==press[i].length-1){
                        System.out.println();
                    }
                }
            }
        }
    }

    private static boolean isPressNull(){
        boolean flag=false;
        lo:for (int i = 0; i < press.length; i++) {
            for (int j = 0; j < press[i].length; j++) {
                if (press[i][j]!=null){
                    flag=true;
                    break lo;
                }
            }
        }
        if (!flag){
            System.out.println("暂无数据，请添加后重试");
            return true;
        }
        return false;
    }

    private static void getPressInfoByName() {
        if (isPressNull()){
            return;
        }
        String pressName;
        while (true) {
            System.out.println("请输入出版社名称：");
            pressName = sc.next();
            boolean exists = isPressExists(pressName);
            if (exists) {
                break;
            } else {
                System.out.println("您输入的编码不存在，请重新输入！");
            }
        }
        int index=-1;
        for (int i = 0; i < press.length; i++) {
            for (int j = 0; j < press.length; j++) {
                if(press[i][0]!=null && pressName.equals(press[i][0])){
                    index=i;
                }
            }
        }

        //	出版社信息（出版社名称、地址、联系人）
        System.out.println("出版社名称\t地址\t联系人");
        for (int i = 0; i < press[index].length; i++) {
            System.out.print(press[index][i]+"\t");
        }
        System.out.println();
    }

    //	出版社信息（出版社名称、地址、联系人）
    private static void addPressInfo() {
        String pressName;
        while (true) {
            System.out.println("请输入出版社名称：");
            pressName = sc.next();
            boolean exists = isPressExists(pressName);
            if (exists) {
                System.out.println("您输入的编码已存在，请重新输入！");
            } else {
                break;
            }
        }
        System.out.println("请输入出版社地址：");
        String pressAddress = sc.next();
        System.out.println("请输入出版社联系人：");
        String pressBoss = sc.next();
        for (int i = 0; i < press.length; i++) {
            if (press[i][0]==null){
                press[i][0]=pressName;
                press[i][1]=pressAddress;
                press[i][2]=pressBoss;
                System.out.println("添加成功！");
                return;
            }
        }
        System.out.println("添加失败+_+");
    }

    private static boolean isPressExists(String pressName) {
        boolean flag = false;
        for (int i = 0; i < press.length; i++) {
            if (press[i][0] != null && press[i][0].equals(pressName)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    //三级菜单（图书）
    private static void bookMenu() {
        while (true) {
            System.out.println("欢迎使用<图书>管理系统");
            System.out.println("请输入：\t1.增加\t2.删除\t3.更新\t4.查询\t5.返回上级菜单");
            String choice = sc.next();
            switch (choice) {
                case "1":
//                    System.out.println("增加");
                    addBookInfo();
                    break;
                case "2":
//                    System.out.println("删除");
                    deleteBookInfoByISBN();
                    break;
                case "3":
//                    System.out.println("更新");
                    updateBookInfoByISBN();
                    break;
                case "4":
//                    System.out.println("查询");
                    getBookInfo();
                    break;
                case "5":
                    return;
                default:
                    System.out.println("输入错误，请重新输入");
                    break;
            }
        }
    }

    private static void deleteBookInfoByISBN() {
        if (isBookNull()){
            return;
        }
        String bookNum;
        while (true) {
            System.out.println("请输入您想删除的图书编码（ISBN）");
            bookNum = sc.next();
            boolean isExists = isBookExists(bookNum);
            if (isExists) {
                break;
            } else {
                System.out.println("查无信息，请重新输入！");
            }
        }
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] != null && bookNum.equals(books[i][0])) {
                for (int j = 0; j < books[i].length; j++) {
                    books[i][j] = null;
                }
            }
        }
        System.out.println("删除成功！");
    }

    private static void updateBookInfoByISBN() {
        if (isBookNull()){
            return;
        }
        String bookNum;
        while (true) {
            System.out.println("请输入您更新的图书编码（ISBN）");
            bookNum = sc.next();
            boolean isExists = isBookExists(bookNum);
            if (isExists) {
                break;
            } else {
                System.out.println("查无信息，请重新输入！");
            }
        }
        System.out.println("请输入书籍名称：");
        String bookName = sc.next();
        System.out.println("请输入书籍价格：");
        String bookPrice = sc.next();
        System.out.println("请输入书籍出版社：");
        String bookPress = sc.next();
        System.out.println("请输入书籍作者：");
        String bookWriter = sc.next();
        int index = -1;
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] != null && bookNum.equals(books[i][0])) {
                index = i;
                break;
            }
        }
        books[index][1] = bookName;
        books[index][2] = bookPrice;
        books[index][3] = bookPress;
        books[index][4] = bookWriter;
        System.out.println("修改成功！");
    }

    //四级菜单（图书信息查询）
    private static void getBookInfo() {
        if (isBookNull()){
            return;
        }
        while (true) {
            System.out.println("1.根据ISBN查询\t2.根据书名查询（模糊）\t3.根据出版社查询\t4.根据作者查询\t5.根据价格范围查询\t6.查询所有书籍信息\t7.返回上一级菜单");
            int choice = sc.nextInt();
            switch (choice) {
                case 1:
//                    System.out.println("根据ISBN查询");
                    getBookByISBN();
                    break;
                case 2:
//                    System.out.println("根据书名查询（模糊）");
                    getBookByName();
                    break;
                case 3:
//                    System.out.println("根据出版社查询");
                    getBookByPress();
                    break;
                case 4:
//                    System.out.println("根据作者查询");
                    getBookByWriterName();
                    break;
                case 5:
//                    System.out.println("根据价格范围查询");
                    getBookByPrice();
                    break;
                case 6:
//                    System.out.println("查询所有书籍信息");
                    getAllBook();
                    break;
                case 7:
                    return;
                default:
                    System.out.println("输入错误，请重新输入！");
                    break;
            }
        }

    }

    private static boolean isBookNull() {
        boolean flag=false;
        lo:for (int i = 0; i < books.length; i++) {
            for (int j = 0; j < books.length; j++) {
                if (books[i][j]!=null){
                    flag=true;
                    break lo;
                }
            }
        }
        if (!flag){
            System.out.println("暂无数据，请添加后重试");
            return true;
        }
        return false;
    }

    //查询所有书籍
    private static void getAllBook() {
        System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");

        for (int i = 0; i < books.length; i++) {
            for (int j = 0; j < books.length; j++) {
                if (books[i][j] != null) {
                    System.out.print(books[i][j] + "\t");
                    if(j==books.length-1){
                        System.out.println();
                    }
                }
            }
        }
    }

    //根据价格查找书籍
    private static void getBookByPrice() {
        double min;
        double max;
        lo:
        while (true) {
            System.out.println("请输入最低价");
            min = sc.nextDouble();
            System.out.println("请输入最高价");
            max = sc.nextDouble();
            for (int i = 0; i < books.length; i++) {
                if (books[i][3] != null && Double.parseDouble(books[i][2]) >= min && Double.parseDouble(books[i][2]) <= max) {
                    break lo;
                }
            }
            System.out.println("未查询到相关信息，请重新输入");
        }
        System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");
        for (int i = 0; i < books.length; i++) {
            if (books[i][3] != null && Double.parseDouble(books[i][2]) >= min && Double.parseDouble(books[i][2]) <= max) {
                for (int j = 0; j < books[i].length; j++) {
                    System.out.print(books[i][j] + "\t");
                }
                System.out.println();
            }
        }
    }

    //根据作者查找书籍
    private static void getBookByWriterName() {
        String writerName;
        lo:
        while (true) {
            System.out.println("请输入图书作者名");
            writerName = sc.next();
            for (int i = 0; i < books.length; i++) {
                if (books[i][4] != null && writerName.equals(books[i][4])) {
                    break lo;
                }
            }
            System.out.println("未查询到相关信息，请重新输入");
        }
        System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");
        for (int i = 0; i < books.length; i++) {
            if (books[i][4] != null && writerName.equals(books[i][4])) {
                for (int j = 0; j < books[i].length; j++) {
                    System.out.print(books[i][j] + "\t");
                }
                System.out.println();
            }
        }
    }

    //根据出版社查找书籍
    private static void getBookByPress() {
        String name;
        lo:
        while (true) {
            System.out.println("请输入图书出版社名");
            name = sc.next();
            for (int i = 0; i < books.length; i++) {
                if (books[i][3] != null && name.equals(books[i][3])) {
                    break lo;
                }
            }
            System.out.println("未查询到相关信息，请重新输入");
        }

        System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");
        for (int i = 0; i < books.length; i++) {
            if (books[i][3] != null && name.equals(books[i][3])) {
                for (int j = 0; j < books[i].length; j++) {
                    System.out.print(books[i][j] + "\t");
                }
                System.out.println();
            }
        }
    }

    //根据书名查找书籍
    private static void getBookByName() {
        String bookName;
        lo:
        while (true) {
            System.out.println("请输入图书名字包含的内容");
            bookName = sc.next();
            for (int i = 0; i < books.length; i++) {
                if (books[i][1] != null && books[i][1].indexOf(bookName) != -1) {
                    break lo;
                }
            }
            System.out.println("未查询到相关信息，请重新输入");
        }
        System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");
        for (int i = 0; i < books.length; i++) {
            if (books[i][1] != null && books[i][1].indexOf(bookName) != -1) {
                for (int j = 0; j < books[i].length; j++) {
                    System.out.print(books[i][j] + "\t");
                }
                System.out.println();
            }
        }
    }

    //根据编号查找书籍
    private static void getBookByISBN() {
        String num;
        System.out.println("请输入图书编号（ISBN）");
        num = sc.next();
        int index = -1;
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] != null && num.equals(books[i][0])) {
                index = i;
                break;
            }
        }
        //	书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）
        System.out.println("编码（ISBN）\t书籍名称\t价格\t出版社\t作者");
        for (int i = 0; i < books[index].length; i++) {
            System.out.print(books[index][i] + "\t");
        }
        System.out.println();
    }

    //	书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）
    private static void addBookInfo() {
        String bookNum;
        while (true) {
            System.out.println("请输入编码（ISBN）：");
            bookNum = sc.next();
            boolean exists = isBookExists(bookNum);
            if (exists) {
                System.out.println("您输入的编码已存在，请重新输入！");
            } else {
                break;
            }
        }
        System.out.println("请输入书籍名称：");
        String bookName = sc.next();
        System.out.println("请输入书籍价格：");
        String bookPrice = sc.next();
        System.out.println("请输入书籍出版社：");
        String bookPress = sc.next();
        System.out.println("请输入书籍作者：");
        String bookWriter = sc.next();
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] == null) {
                books[i][0] = bookNum;
                books[i][1] = bookName;
                books[i][2] = bookPrice;
                books[i][3] = bookPress;
                books[i][4] = bookWriter;
                System.out.println("添加书籍成功！");
                return;
            }
        }
        System.out.println("添加失败+_+");
    }

    //查询书籍是否存在（通过编号）
    private static boolean isBookExists(String bookNum) {
        boolean flag = false;
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] != null && books[i][0].equals(bookNum)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    //查询用户索引
    private static int getUserIndex(String userName) {
        int index = -1;
        for (int i = 0; i < users.length; i++) {
            if (users[i][1] != null && userName.equals(users[i][1])) {
                index = i;
                break;
            }
        }
        return index;
    }

    //查询用户名是否存在
    private static boolean isUserExists(String userName) {
        boolean flag = false;
        for (int i = 0; i < users.length; i++) {
            if (users[i][0] != null && users[i][1].equals(userName)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    //	初始化用户信息、出版社信息、书籍信息；（设置基础信息，赋值；）
    private static void setup() {
        users[0][0] = "Test";
        users[0][1] = "1";
        users[0][2] = "2";
        users[0][3] = "admin";

        books[0][0] = "123";
        books[0][1] = "Java基础";
        books[0][2] = "9.9";
        books[0][3] = "闽大出版社";
        books[0][4] = "大老";

        books[1][0] = "124";
        books[1][1] = "HTML基础";
        books[1][2] = "9.8";
        books[1][3] = "闽大出版社";
        books[1][4] = "大老";

        press[0][0] = "闽大出版社";
        press[0][1] = "闽大门口";
        press[0][2] = "大老";
    }
}