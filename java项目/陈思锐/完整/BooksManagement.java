package com.dongli.jinjie1;

//做到模糊出版社查询
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BooksManagement {
    static int usersDefaultNum = 50;
    static int booksDefaultNum = 100;
    static int publishersDefaultNum = 30;
    static String[][] users = new String[usersDefaultNum][4];
    static String[][] books = new String[booksDefaultNum][5];
    static String[][] publishers = new String[publishersDefaultNum][3];
    static Scanner s = new Scanner(System.in);
    static String nowUserName;

    public static void main(String[] args) {
        //初始化信息
        initUsers(users);
        initBooks(books);
        initPublishers(publishers);
        welcome();

    }

    //选择功能
    public static void managementSysChoose() {
        System.out.println( nowUserName + ",欢迎您使用闽大书籍管理系统！！！");

        while (true) {
            System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
            int choose = s.nextInt();
            if (choose == 1){
                booksManage();
                break;
            }else if(choose == 2){
                publisherManage();
                break;
            }else if(choose == 3){
                welcome();
                break;
            }else if(choose == 4){
                System.exit(0);
            }else{
                System.out.println("请重新输入");
                continue;
            }
        }

    }

    //出版社管理
    public static void publisherManage() {
        System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
        int choose = s.nextInt();

        boolean flag;
        while(true){
            flag = false;
            switch (choose) {
                //1.增加
                case 1:
                    publisherManageAdd();
                    break;
                //2.删除
                case 2:
                    publisherManageDelete();
                    break;
                // 3.更新
                case 3:
                    publisherUpdate();
                    break;

                //根据出版社名称查询
                case 4:
                    System.out.println("请输入出版社名称");
                    String pName = s.next();
                    int index = findIndex(pName,publishers,0);
                    if (index >= 0) {
                        System.out.print("出版社名称" + "\t" + "地址" + "\t" + "联系人");
                        System.out.println();
                        for(int i = 0;i < publishers[index].length;i++){
                            System.out.print(publishers[index][i] + "\t");
                        }
                        System.out.println();
                    }else{
                        System.out.println("未找到该出版社信息");
                    }
                    publisherManage();

                //查询所有出版社
                case 5:
                    System.out.print("出版社名称" + "\t" + "地址" + "\t" + "联系人");
                    System.out.println();
                    for(int i = 0;i < publishers.length;i++){
                        if(publishers[i][0] == null) {
                            break;
                        }
                        for(int j = 0;j < publishers[i].length;j++){
                            System.out.print(publishers[i][j] + "\t");
                        }
                        System.out.println();
                    }
                    publisherManage();
                    break;
                case 6:
                    managementSysChoose();
                    break;
                default:
                    flag = true;
                    System.out.println("请重新输入");
                    choose = s.nextInt();
                    break;
            }
            if(flag == false){
                break;
            }
        }

    }

    //出版社更新
    public static void publisherUpdate() {
        System.out.println("请输入出版社名称：");
        String pName = s.next();
        //查找下标,-1提示没有找到该出版社
        int index = findIndex(pName,publishers,0);
        if(index != -1){
            System.out.println("请输入地址：");
            String publisherAddr = s.next();

            System.out.println("请输入联系人：");
            String publisherLink = s.next();

            publishers[index][1] = publisherAddr;
            publishers[index][2] = publisherLink;

            System.out.println("更新成功");

        }else{
            System.out.println("没有找到该编号");
        }

        publisherManage();
    }

    //出版社删除
    public static void publisherManageDelete() {
        System.out.println("请输入要删除的出版社名称：");
        String deleteName = s.next();
        //查找下标,-1提示没有找到该编号
        int index = findIndex(deleteName,publishers,0);
        if(index != -1){
            //从要删除的目录开始，后一个数组赋给前一个数组
            for (int i = index; i < publishers.length - 1; i++) {
                publishers[i] = publishers[i + 1];
            }
            //最后的数组全部元素为空
            for(int i = 0;i < publishers[publishers.length - 1].length;i++){
                publishers[publishers.length - 1][i] = null;
            }

            System.out.println("删除成功");
        }else{
            System.out.println("没有找到该出版社");
        }
        publisherManage();

    }

    //出版社增加
    public static void publisherManageAdd() {
        //出版社名称      地址    联系人
        System.out.println("请输入出版社名称：");
        String publisherName = s.next();
        boolean flag;
        //判断出版社名称是否相同
        while (true) {
            flag = false;
            for (int i = 0; i < publishers.length; i++) {
                if (publisherName.equals(publishers[i][0])) {
                    flag = true;
                    System.out.println("本出版社已存在，请重新输入");
                    publisherName = s.next();
                    break;
                }
            }
            if(flag == false){
                break;
            }
        }
        System.out.println("请输入地址：");
        String publisherAddr = s.next();

        System.out.println("请输入联系人：");
        String publisherLink = s.next();

        //判断书本数组的空位
        int index = isNull(publishers);

        if(index != -1) {
            publishers[index][0] = publisherName;
            publishers[index][1] = publisherAddr ;
            publishers[index][2] = publisherLink;
        }else {
            publishersDefaultNum += 50;
            String[][] newpublishers = new String[publishersDefaultNum][3];

            newpublishers = copyArrays(publishers,newpublishers);

            publishers[index][0] = publisherName;
            publishers[index][1] = publisherAddr ;
            publishers[index][2] = publisherLink;

            //更新旧数组
            publishers = newpublishers;
        }
        System.out.println("新增成功");
        publisherManage();

    }

    //图书管理功能
    public static void booksManage() {
        System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
        int choose = s.nextInt();

        boolean flag;
        while(true){
            flag = false;
            switch (choose) {
                case 1:
                    booksManageAdd();
                    break;
                case 2:
                    booksManageDelete();
                    break;
                case 3:
                    booksUpdate();
                    break;
                case 4:
                    booksQuery();
                    break;
                case 5:
                    managementSysChoose();
                    break;
                default:
                    flag = true;
                    System.out.println("请重新输入");
                    choose = s.nextInt();
                    break;
            }
            if(flag == false){
                break;
            }
        }
    }

    //图书管理查询界面
    public static void booksQuery() {
        System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社  4.作者  5.价格范围  6.查询所有  7.返回上一级");
        int choose = s.nextInt();
        boolean flag;
        int index = -1;

        while(true){
            flag = false;
            switch (choose) {
                //根据isbn查找
                case 1:
                    System.out.println("请输入ISBN号");
                    String iNum = s.next();
                    index = findIndex(iNum,books,0);
                    if (index != -1) {
                        System.out.print("ISBN" + " \t" + "书名" + " \t" + "价格" + " \t" + "出版社" + " \t" + "作者");
                        System.out.println();
                        for (int i = 0; i < books[index].length; i++) {
                            System.out.print(books[index][i] + " \t");
                        }
                        System.out.println();
                    }else{
                        System.out.print("ISBN" + "\t" + "书名" + "\t" + "价格" + "\t" + "出版社" + "\t" + "作者");
                        System.out.println();
                    }
                    break;
                //根据书名查找
                case 2:
                    System.out.println("请输入书名关键字");
                    String bookName = s.next();
                    ArrayList<Integer> arrayIndex2 = findIndexArray(bookName,1);
                    arrayPrint(arrayIndex2);
                    break;
                //根据出版社查找
                case 3:
                    int index1 = isNull(books);
                    System.out.print("请输入出版社前的数字进行选择：");

                    //这是用来装打印出来的书籍出版社下标的集合
                    ArrayList<Integer> booksIndex = new ArrayList<Integer>();
                    int count = 0;
                    for (int i = 0; i < index1; i++) {
                        boolean flag1 = true;
                        for (int j = 0; j < i; j++) {
                            if (books[i][3] != null && books[i][3].equals(books[j][3])) {
                                flag1 = false;
                                break;
                            }
                        }
                        if (flag1) {
                            System.out.print(count++ + 1 + " " + books[i][3] + " ");
                            booksIndex.add(i);
                        }
                    }
                    System.out.println();
                    int choice = s.nextInt();
                    if (booksIndex.size() == 0) {
                        System.out.println("这里没有任何书籍");
                    }else if (choice < 1 || choice > count) {
                        System.out.println("您输入的数字不存在");
                    }else{
                        String bookPublisher = books[booksIndex.get(choice - 1)][3];
                        ArrayList<Integer> arrayIndex3 = findIndexArrayComplete(bookPublisher,3);
                        arrayPrint(arrayIndex3);
                    }
                    booksManage();
                    break;
                //根据作者信息查询
                case 4:
                    System.out.println("请输入作者");
                    String bookAuthor = s.next();
                    ArrayList<Integer> arrayIndex4 = findIndexArrayComplete(bookAuthor,4);
                    arrayPrint(arrayIndex4);
                    break;
                //根据价格范围查询
                case 5:
                    System.out.println("请输入价格范围，注意第一次输入应比第二次小");
                    System.out.println("请输入价格范围1");
                    double priceRange1 = s.nextDouble();
                    System.out.println("请输入价格范围2");
                    double priceRange2 = s.nextDouble();
                    boolean flag1 = true;
                    while(flag1) {
                        if(priceRange1 > priceRange2) {
                            System.out.println("输入错误，第一次输入应比第二次小");
                            System.out.println("请重新输入价格范围1");
                            priceRange1 = s.nextDouble();
                            System.out.println("请重新输入价格范围2");
                            priceRange2 = s.nextDouble();
                        }else {
                            flag1 = false;
                        }
                    }
                    ArrayList<Integer> arrayIndex5 = findIndexArrayDouble(priceRange1,priceRange2,2);
                    arrayPrint(arrayIndex5);
                    break;
                //查询所有
                case 6:
                    System.out.print("ISBN" + " \t" + "书名" + " \t" + "价格" + " \t" + "出版社" + " \t" + "作者");
                    System.out.println();
                    for(int i = 0;i < books.length;i++){
                        if(books[i][0] == null) {
                            break;
                        }
                        for(int j = 0;j < books[i].length;j++){
                            System.out.print(books[i][j] + "\t");
                        }
                        System.out.println();
                    }
                    break;
                case 7:
                    booksManage();
                    break;
                default:
                    flag = true;
                    System.out.println("请重新输入");
                    choose = s.nextInt();
                    break;
            }
            if(flag == false){
                break;
            }
        }

        booksQuery();

    }


    //传集合打印数组方法
    public static void arrayPrint(ArrayList<Integer> arrayIndex) {
        if(arrayIndex.size() != 0){
            System.out.print("ISBN" + " \t" + "书名" + " \t" + "价格" + " \t" + "出版社" + " \t" + "作者");
            System.out.println();
            for (int i = 0; i < arrayIndex.size(); i++) {
                for (int j = 0; j < books[arrayIndex.get(i)].length; j++) {
                    System.out.print(books[arrayIndex.get(i)][j] + " \t");
                }
                System.out.println();
            }
        }else{
            System.out.print("ISBN" + " \t" + "书名" + " \t" + "价格" + " \t" + "出版社" + " \t" + "作者");
            System.out.println();
        }

    }

    //根据String查找数组下标识,返回int
    public static int findIndex(String s,String[][] strs,int num) {
        for (int i = 0; i < strs.length; i++) {
            if (s.equals(strs[i][num])) {
                return i;
            }
        }
        return -1;
    }

    //传字符串模糊查询,返回集合
    public static ArrayList<Integer> findIndexArray(String strIndex,int num) {
        ArrayList<Integer> arrayIndex = new ArrayList<>();
        for (int i = 0; i < books.length; i++) {
            if(books[i][num] != null && books[i][num].contains(strIndex)){
                arrayIndex.add(i);
            }
        }
        return arrayIndex;
    }

    //传字符串全等查询，返回集合
    public static ArrayList<Integer> findIndexArrayComplete(String strIndex,int num) {
        ArrayList<Integer> arrayIndex = new ArrayList<>();
        for (int i = 0; i < books.length; i++) {
            if(strIndex.equals(books[i][num])){
                arrayIndex.add(i);
            }
        }
        return arrayIndex;
    }

    //通过价格区间查找,返回集合
    public static ArrayList<Integer> findIndexArrayDouble(double priceRange1, double priceRange2, int num) {
        ArrayList<Integer> arrayIndex = new ArrayList<>();
        for (int i = 0; i < books.length; i++) {
            if( books[i][2] != null && Double.parseDouble(books[i][2]) >= priceRange1 && Double.parseDouble(books[i][2]) <= priceRange2) {
                arrayIndex.add(i);
            }
        }
        return arrayIndex;
    }

    //图书更新
    public static void booksUpdate() {
        System.out.println("请输入ISBN号：");
        String iNum = s.next();
        //查找下标,-1提示没有找到该编号
        int index = findIndex(iNum,books,0);
        if(index != -1){
            System.out.println("请输入新书籍名称：");
            String bookName = s.next();

            System.out.println("请输入新书籍价格：");
            String bookPrice = s.next();

            System.out.println("请输入新书籍出版社：");
            String bookPublisher = s.next();

            System.out.println("请输入新书籍作者");
            String author = s.next();

            books[index][1] = bookName;
            books[index][2] = bookPrice;
            books[index][3] = bookPublisher;
            books[index][4] = author;
            System.out.println("更新成功");

        }else{
            System.out.println("没有找到该编号");
        }

//        for(int i = 0;i < books.length;i++){
//            for(int j = 0;j < books[i].length;j++){
//                System.out.print(books[i][j] + "|");
//            }
//            System.out.println();
//        }

        booksManage();
    }

    //图书管理（删除）
    public static void booksManageDelete() {
        System.out.println("请输入要删除的书本编号");
        String deleteNum = s.next();
        //查找下标,-1提示没有找到该编号
        int index = findIndex(deleteNum,books,0);
        if(index != -1){
            //从要删除的目录开始，后一个数组赋给前一个数组
            for (int i = index; i < books.length - 1; i++) {
                books[i] = books[i + 1];
            }
            //最后的数组全部元素为空
            for(int i = 0;i < books[books.length - 1].length;i++){
                books[books.length - 1][i] = null;
            }
        }else{
            System.out.println("没有找到该编号");
        }

//        for(int i = 0;i < books.length;i++){
//            for(int j = 0;j < books[i].length;j++){
//                System.out.print(books[i][j] + "|");
//            }
//            System.out.println();
//        }
        booksManage();

    }


    //图书馆里(增加)
    public static void booksManageAdd() {
        //编码（ISBN）、书籍名称、价格、出版社、作者
        System.out.println("请输入书籍编码（ISBN）：");
        String ISBN = s.next();
        boolean flag;
        //判断编码是否相同
        while (true) {
            flag = false;
            for (int i = 0; i < books.length; i++) {
                if (ISBN.equals(books[i][0])) {
                    flag = true;
                    System.out.println("本编号已存在，请重新输入");
                    ISBN = s.next();
                    break;
                }
            }
            if(flag == false){
                break;
            }
        }

        System.out.println("请输入书籍名称：");
        String bookName = s.next();

        System.out.println("请输入价格：");
        String bookPrice = s.next();

        System.out.println("请输入出版社：");
        String bookPublisher = s.next();

        System.out.println("请输入作者");
        String author = s.next();

        //判断书本数组的空位
        int index = isNull(books);

        if(index != -1) {
            books[index][0] = ISBN;
            books[index][1] = bookName;
            books[index][2] = bookPrice;
            books[index][3] = bookPublisher;
            books[index][4] = author;
        }else {
            booksDefaultNum += 50;
            String[][] newBooks = new String[booksDefaultNum][5];

            newBooks = copyArrays(books,newBooks);

            newBooks[books.length][0] = ISBN;
            newBooks[books.length][1] = bookName;
            newBooks[books.length][2] = bookPrice;
            newBooks[books.length][3] = bookPublisher;
            newBooks[books.length][4] = author;

            //更新旧数组
            books = newBooks;
        }
        System.out.println("新增成功");

//        for(int i = 0;i < books.length;i++){
//            for(int j = 0;j < books[i].length;j++){
//                System.out.print(books[i][j] + "|");
//            }
//            System.out.println();
//        }

        booksManage();
    }

    //欢迎方法
    public static void welcome() {
        System.out.println("欢迎使用图书管理系统：");
        System.out.println("1.登陆   2.注册");

        //判断用户的选择
        int choose = s.nextInt();
        switch (choose) {
            case 1:
                login(users);
                managementSysChoose();
                break;
            case 2:
                int index = isNull(users);
                Register(index);
                welcome();
                managementSysChoose();
                break;

            default:
                System.out.println("退出系统！！");
                System.exit(0);
                break;
        }
    }

    //判断数组哪里有空
    public static int isNull(String[][] strs) {
        int index = -1;

        for(int i = 0;i < strs.length;i++) {
            if(strs[i][1] == null) {
                index = i;
                return index;
            }
        }

        return index;
    }

    //注册方法
    public static void Register(int index) {

        System.out.println("请输入注册用户部门：");
        String userDepart = s.next();

        System.out.println("请输入注册用户名：");
        String userName = s.next();

        System.out.println("请输入注册密码：");
        String userPassword = s.next();

        System.out.println("请输入注册用户角色：");
        String userPart = s.next();

        while(true){
            boolean flag = true;
            for (int i = 0; i < users.length; i++) {
                if(userName.equals(users[i][1])){
                    flag = false;
                    break;
                }
            }
            if (flag == false) {
                System.out.println("您注册的用户名已存在,请重新输入");
                userName = s.next();
            }else{
                System.out.println("注册成功！");
                break;
            }
        }

        //判断数组有没有满，没满直接赋值，满了新建一个更大数组赋值
        if(index != -1) {
            users[index][0] = userDepart;
            users[index][1] = userName;
            users[index][2] = userPassword;
            users[index][3] = userPart;
        }else {
            usersDefaultNum += 50;
            String[][] newUsers = new String[usersDefaultNum][4];

            newUsers = copyArrays(users,newUsers);

            newUsers[users.length][0] = userDepart;
            newUsers[users.length][1] = userName;
            newUsers[users.length][2] = userPassword;
            newUsers[users.length][3] = userPart;

            //把新数组赋值给旧数组
            users = newUsers;
        }

        //遍历新数组
//        for (int i = 0; i < users.length; i++) {
//            for (int j = 0; j < users[i].length; j++) {
//                System.out.print(users[i][j] + "|");
//            }
//            System.out.println();
//        }
    }

    //二维数组拷贝
    public static String[][] copyArrays(String[][] old, String[][] newArrays) {
        for (int i = 0; i < old.length; i++) {
            for (int j = 0; j < old[i].length; j++) {
                newArrays[i][j] = users[i][j];
            }
        }

        return newArrays;

    }

    //登陆
    public static void login(String[][] users) {
        boolean flag = false;
        for(int j = 0;j < 4;j++) {

            System.out.println("请输入用户名：");
            String userName = s.next();

            System.out.println("请输入密码：");
            String userPassword = s.next();

            for(int i = 0;i < users.length;i++) {
                if(userName.equals(users[i][1]) && userPassword.equals(users[i][2])) {
                    flag = true;
                    break;
                }
            }

            if(j == 2 && flag == false) {
                System.out.println("输入错误超过3次，退出系统");
                System.exit(0);
            }else if(flag == true) {
                nowUserName = userName;
                System.out.println("登陆成功！");
                break;
            }else if (flag == false && j < 3) {
                System.out.println("该用户不存在或者密码错误！请重新登录！");
            }
        }

    }


    //初始化出版社
    public static void initPublishers(String[][] publishers) {
        publishers[0][0] = "人民文学出版社";
        publishers[0][1] = "北京市xxxx";
        publishers[0][2] = "amy";

        publishers[1][0] = "意林文学出版社";
        publishers[1][1] = "上海市xxxx";
        publishers[1][2] = "adam";
    }

    //初始化书
    public static void initBooks(String[][] books) {
        books[0][0] = "165631";
        books[0][1] = "直捣蜂窝的女孩";
        books[0][2] = "30";
        books[0][3] = "人民文学出版社";
        books[0][4] = "史迪格拉森";

        books[1][0] = "461641";
        books[1][1] = "神经漫游者";
        books[1][2] = "50";
        books[1][3] = "意林出版社";
        books[1][4] = "威廉吉布森";
    }

    //初始化用户
    public static void initUsers(String[][] users) {
        users[0][0] = "管理部门";
        users[0][1] = "admin";
        users[0][2] = "abc";
        users[0][3] = "管理者";

        users[1][0] = "普通用户";
        users[1][1] = "sally";
        users[1][2] = "def";
        users[1][3] = "普通用户";
    }



}
