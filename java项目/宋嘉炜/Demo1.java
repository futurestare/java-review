package 晚自习.md.Demo;



import java.util.Arrays;
import java.util.Scanner;

public class Demo1{

	static Scanner scanner = new Scanner(System.in);
	static String UserM[][] = new String [20][4];
	static String bookM[][] = new String [20][5];
	static String ExpressM [][] = new String [20][4];
	static String LoginUserName = "";
	public static void main(String[] args) {
		UserM[0][0]="软件部";
		UserM[0][1]="张三";
		UserM[0][2]="123";
		UserM[0][3]="普通员工";

		
		UserM[1][0]="设计部";
		UserM[1][1]="NNK";
		UserM[1][2]="12358";
		UserM[1][3]="老板";
		
		bookM[0][0]="白夜行";
		bookM[0][1]="123456789";
		bookM[0][2]="东京出版社";
		bookM[0][3]="东野圭吾";
		bookM[0][4]="30";
		
		Index();
	}
	
	
	private static void Index() {
		System.out.println("欢迎来到闽大图书馆系统");
		System.out.println("1.登录\t"+"2.注册  \t"+"3.查询");
		int eraben = scanner.nextInt();
		
		switch (eraben) {
		case 1:
			login();
			break;
		case 2:
			register();
			break;
		case 3:
			MessageContext();//用户信息
			break;
		default:
			System.out.println("欢迎下次光临！拜拜");
			break;
		}
	}


	private static void login() {
		System.out.println("请输入用户名");
		String username = scanner.next();
		System.out.println("请输入密码");
		String password = scanner.next();
		LoginUserName=username;
		int count = 0;
		boolean a = false;
		for (int i = 0; i < UserM.length; i++) {
			if (username.equals(UserM[i][1])&&password.equals(UserM[i][2])) {
				a = true;
				break;
			}
		}
		
		if (a) {
			System.out.println("恭喜登陆成功！");
			System.out.println("Loading...");
			System.out.println("欢迎"+"\t"+LoginUserName+"\t"+"进入系统！");
			System();
		}else if(count==3){
			System.out.println("由于三次出错，直接返回首页");
			Index();
		}else{
			System.out.println("输入错误！");
			count++;
			login();
		}
	}
	private static void System() {
		System.out.println("三个板块你想选择哪一个？");
		System.out.println("1.图书管理\t"+"2.出版社信息\t"+"3.退出系统\t"+"4.退出登录");
		
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			System.out.println("正在进入图书管理菜单");
			System.out.println("Loading...");
			Library();
			break;
		case 2:
			System.out.println("正在进入出版社管理菜单");
			System.out.println("Loading...");
			Express();
			break;
		case 3:
			System.out.println("欢迎下次访问哦！ 亲~~");
			break;
		case 4:
			System.out.println("退出登录中。。。");
			login();
		}
	}


	private static void Express() {
		//5.	出版社管理菜单：
		//1)	增加
		//2)	删除：出版社有关联的图书不能删除；
		//3)	更新 
		//4)	根据出版社名称查询
		//5)	查询所有出版社
		//6)	返回上一级菜单

		
				//名称，地点，开创时间，创办人

			System.out.println("欢迎来到出版社管理菜单");
			System.out.println("以下四个区域:");
			System.out.println("1.查询出版社\t"+"2.增加\t"+"3.更新\t"+"4.删除\t"+"按其他键返回上一级");
			int key = scanner.nextInt();
			switch (key) {
			case 1:
				EReference();
				break;
			case 2:
				EAdd(); 
				break;	
			case 3:
				EUpdate();
				break;
			case 4:
				EDelete();
				break;
			default:
				System.out.println("正在返回上一级");
				System();
				break;
		}
		
	}

	private static void EDelete() {
		for (int i = 0; i < ExpressM.length; i++) {
			if (ExpressM[i][0]!=null) {
				System.out.println(Arrays.toString(ExpressM[i]));
			}
		}
		
		System.out.println("请删除你想删除的ISBN编号信息");
		String Del = scanner.next();
		
		boolean B = false;
		int R = -1;
		for (int i = 0; i < ExpressM.length; i++) {
			if (Del.equals(ExpressM[i][1])) {
				B=true;
				R=i;
				break;
			}
		}

		if (B) {
			ExpressM[R][0]=null;
			ExpressM[R][1]=null;
			ExpressM[R][2]=null;
			ExpressM[R][3]=null;
			System.out.println("删除成功");
			System.out.println("正在退回图书管理菜单页面");
			Express();
		}
		else {
			System.out.println("?????");
			EDelete();
			}

		
		
	}
	private static void EUpdate() {
		System.out.println("输入你要修改的ISBN编号");
		String check = scanner.next();
		int i = 0;
		System.out.println("现在开始核对ISBN编号的书籍");
		for ( i = 0; i < ExpressM.length; i++) {
			if (check.equals(ExpressM[i][1])) {
				System.out.println("存在此书");
				System.out.println("书名"+"IBSN"+"出版社"+"作者");
				System.out.println(ExpressM[i][0]+"\t"+ExpressM[i][1]+"\t"+ExpressM[i][2]+"\t"+ExpressM[i][3]);
				break;
			}
			i++;
		}
		
		boolean Judge = true;
				while(Judge) {
				System.out.println("1.对出版名进行更改\t"+"2.对出版社地址进行更改\t"+"3.对开创时间进行修改\t"+"4.对所有人进行调整\t"+"按5退出");
				int C = scanner.nextInt();
				switch (C) {
				case 0:
					String Update0 = scanner.next();
					ExpressM[i][0]=Update0;
					break;
				case 1:
					String Update1 = scanner.next();
					ExpressM[i][1]=Update1;
					break;
				case 2:
					String Update2 = scanner.next();
					ExpressM[i][2]=Update2;
					break;
				case 3:
					String Update3 = scanner.next();
					ExpressM[i][3]=Update3;
					break;
				default:
					Judge = false;
					System.out.println(Arrays.toString(ExpressM[i]));
					Express();
					break;
				}
			}
		
	}
	private static void EAdd() {
		System.out.println("你所想添加的出版社名");
		String Name = scanner.next();
		
		System.out.println("出版社地址");
		String Id = scanner.next();
		
		System.out.println("出版社开创时间");
		String printName = scanner.next();
		
		System.out.println("所有人");
		String Belong = scanner.next();
		
		
		for (int i = 0; i < ExpressM.length; i++) {
			if (Id.equals(ExpressM[i][1])) {
				System.out.println("在书库中已经存在这本书，输入1重新添加，输入别的数字退回出版社管理页面");
				
				int A = scanner.nextInt();
				switch (A) {
				case 1:
					System.out.println("loading。。。");
					EAdd();
					break;
				default:
					System.out.println("退回出版社管理");
					Express();
					break;
					}
				}
				if (ExpressM[i][0]==null) {
					ExpressM[i][0] = Name;
					ExpressM[i][1] = Id;
					ExpressM[i][2] = printName;
					ExpressM[i][3] = Belong;
					
					System.out.println("添加成功！");
					System.out.println("正在退回图书管理菜单页面");
					Library();
					
					break;
				}
			}
		
	}
	private static void EReference() {//查询
		//a)	根据ISBN查询
		//b)	根据书名查询（模糊）
		//c)	根据出版社查询
		//d)	根据作者查询
		//e)	根据价格范围查询
		//f)	查询所有书籍信息
		System.out.println("开始查询。。。");
		System.out.println("通过以下几种方式来查询");
		System.out.println("请输入:"+"1.开始出版社名称查询\t"+"2.书名查询（模糊）\t");
		int key = scanner.nextInt();
		
		switch (key) {
		case 1:
			System.out.println("开始出版社名称查询");
			ExprssRefer();
			break;

		case 2:
			System.out.println("查询所有人");
			boolean F = false;
			int i = 0;
			for ( i = 0; i < ExpressM.length; i++) {
				if (ExpressM[i][3]!=null) {
					F = true;
					break;
				}
				i++;
			}
			if (F) {
				System.out.println(Arrays.toString(ExpressM[i]));
			}else {
				System.out.println("查询错误正在返回");
			}
			EReference();
			break;

		default:
			System.out.println("退出系统");
			System.exit(0);
			break;
		}
	}
	private static void ExprssRefer() {
		String Ename = scanner.next();
		boolean F = false;
		int i = 0;
		for (i = 0; i < ExpressM.length; i++) {
			if (Ename.equals(ExpressM[i][0])) {
				F = true;
				break;
			}
			i++;
		}
		
		if (F) {
			System.out.println(Arrays.toString(ExpressM[i]));
		}else {
			System.out.println("查询不对，正在返回");
		}
		EReference();
	}
	
	private static void Library() {
		bookM[0][0]="白夜行";
		bookM[0][1]="123456789";
		bookM[0][2]="东京出版社";
		bookM[0][3]="东野圭吾";
		bookM[0][4]="30";
		
				
		
		
		System.out.println("欢迎来到图书管理菜单");
		System.out.println("以下四个区域:");
		System.out.println("1.查询图书\t"+"2.增加\t"+"3.更新\t"+"4.删除\t"+"按其他键返回上一级");
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			BReference();
			break;
		case 2:
			BAdd(); 
			break;	
		case 3:
			BUpdate();
			break;
		case 4:
			BDelete();
			break;
		default:
			System.out.println("正在返回上一级");
			System();
			break;
		}
		
	}
	private static void BDelete() {
		for (int i = 0; i < bookM.length; i++) {
			if (bookM[i][0]!=null) {
				System.out.println(Arrays.toString(bookM[i]));
			}
		}
		
		System.out.println("请删除你想删除的ISBN编号信息");
		String Del = scanner.next();
		
		boolean B = false;
		int R = -1;
		for (int i = 0; i < bookM.length; i++) {
			if (Del.equals(bookM[i][1])) {
				B=true;
				R=i;
				break;
			}
		}

		if (B) {
				bookM[R][0]=null;
				bookM[R][1]=null;
				bookM[R][2]=null;
				bookM[R][3]=null;
				bookM[R][4]=null;
				System.out.println("删除成功");
				System.out.println("正在退回图书管理菜单页面");
				Express();
		}
		else {
			System.out.println("?????");
			BDelete();
			}

		
	}
	private static void BUpdate() {
		System.out.println("输入你要修改的ISBN编号");
		String check = scanner.next();
		int i = 0;
		System.out.println("现在开始核对ISBN编号的书籍");
		for ( i = 0; i < bookM.length; i++) {
			if (check.equals(bookM[i][1])) {
				System.out.println("存在此书");
				System.out.println("书名"+"IBSN"+"出版社"+"作者");
				System.out.println(bookM[i][0]+"\t"+bookM[i][1]+"\t"+bookM[i][2]+"\t"+bookM[i][3]);
				break;
			}
			i++;
		}
		if (i==bookM.length) {
			System.out.println("不存在此书");
			Library();
		}
		boolean Judge = true;
				while(Judge) {
				System.out.println("1.对书名进行更改\t"+"2.对出版社进行更改\t"+"3.对作者进行修改\t"+"4.对价格进行调整\t"+"按5退出");
				int C = scanner.nextInt();
				switch (C) {
				case 0:
					String Update0 = scanner.next();
					bookM[i][0]=Update0;
					break;
				case 1:
					String Update1 = scanner.next();
					bookM[i][2]=Update1;
					break;
				case 2:
					String Update2 = scanner.next();
					bookM[i][3]=Update2;
					break;
				case 3:
					String Update4 = scanner.next();
					bookM[i][4]=Update4;
					break;
				default:
					Judge = false;
					System.out.println(Arrays.toString(bookM[i]));
					Library();
					break;
				}
			}
		}
		
	private static void BAdd() {
		System.out.println("你所想添加的书名");
		String Name = scanner.next();
		
		System.out.println("ISBN编码");
		String Id = scanner.next();
		
		System.out.println("出版社");
		String printName = scanner.next();
		
		System.out.println("作者");
		String writer = scanner.next();
		
		System.out.println("价格");
		String price = scanner.next();
		
		
		for (int i = 0; i < bookM.length; i++) {
			if (Id.equals(bookM[i][1])) {
				System.out.println("在书库中已经存在这本书，输入1重新添加，输入别的数字退回图书管理菜单页面");
				int A = scanner.nextInt();
				switch (A) {
				case 1:
					System.out.println("loading。。。");
					BAdd();
					break;
				default:
					Library();
					break;
				}
				}
				if (bookM[i][1]==null) {
					bookM[i][0] = Name;
					bookM[i][1] = Id;
					bookM[i][2] = printName;
					bookM[i][3] = writer;
					bookM[i][4] = price;
					
					System.out.println("添加成功！");
					System.out.println("正在退回图书管理菜单页面");
					Library();
					
					break;
				}
			}
		}
		
	
	private static void BReference() {//查询
		//a)	根据ISBN查询
		//b)	根据书名查询（模糊）
		//c)	根据出版社查询
		//d)	根据作者查询
		//e)	根据价格范围查询
		//f)	查询所有书籍信息
		System.out.println("开始查询。。。");
		System.out.println("通过以下几种方式来查询");
		System.out.println("请输入:"+"1.ISBN查询\t"+"2.书名查询（模糊）\t"+"3.出版社查询\t"+"4.作者查询\t"+"5.价格范围查询\t"+"6.查询所有书籍信息\t"+"7.返回图书馆系统");
		int key = scanner.nextInt();
		
		switch (key) {
		case 1:
			System.out.println("开始ISBN查询");
			BISBNRefer();
			break;
		case 2:
			System.out.println("开始书名查询（模糊）");
			BookNameRefer();
			break;
		case 3:
			System.out.println("开始出版社查询");
			BExpress();
			break;
		case 4:
			System.out.println("开始作者查询");
			BWriteRefer();
			break;
		case 5:
			System.out.println("开始价格范围查询");
			Bprice();
			
			break;
		case 6:
			System.out.println("开始查询所有书籍信息");
			for (int i = 0; i < bookM.length; i++) {
				if (bookM[i][1]!=null) {
					System.out.println(Arrays.toString(bookM[i]));
				}
			}
			Library();
			break;
		case 7:
			System.out.println("返回图书馆管理页面");
			Library();
			break;
		default:
			System.out.println("退出系统");
			System.exit(0);
			break;
		}
	}
	private static void Bprice() {
		System.out.println("请输入最大承受金额");
		int A = scanner.nextInt();
		for (int i = 0; i < bookM.length; i++) {
			if (Integer.parseInt(bookM[i][4])<= A) {
				System.out.println(Arrays.toString(bookM[i]));
			}
		}
		BReference();
	}


	private static void BISBNRefer() {
		String iBSNr = scanner.next();
		boolean a = false;
		int i = 0;
		for ( i = 0; i < bookM.length; i++) {
			if (iBSNr.equals(bookM[i][1])) {
				a = true;
				break;
			}i++;
		}
		if (a) {
			System.out.println(Arrays.toString(bookM[i]));
		}else {
			System.out.println("错误，返回");
		}
		BReference();
		
	}
	private static void BookNameRefer() {
		String BookN = scanner.next();
		boolean a = false;
		int i = 0;
		for ( i = 0; i < bookM.length; i++) {
			if (BookN.equals(bookM[i][0])) {
				a = true;
				break;
			}
			i++;
		}
		if (a) {
			System.out.println(Arrays.toString(bookM[i]));
		}else {
			System.out.println("错误，返回");
		}
		BReference();

	}



	private static void BExpress() {
		String ExpressR = scanner.next();
		int i = 0;
		boolean a = false;
		for ( i = 0; i < bookM.length; i++) {
			if (bookM[i][1]!=null && bookM[i][0].indexOf(ExpressR)!=-1) {
				a=true;
				break;
			}i++;
		}
		if (a) {
			System.out.println(Arrays.toString(bookM[i]));
		}else {
			System.out.println("错误，返回");
		}
		BReference();
	}
	private static void BWriteRefer() {
		String WriterR = scanner.next();
		for (int i = 0; i < bookM.length; i++) {
			if (bookM[i][1]!=null && bookM[i][0].indexOf(WriterR)!=-1) {
				System.out.println(Arrays.toString(bookM[i]));
				break;
			}
		}
		BReference();
	}
	

	private static void register() {
		
		System.out.println("请输入隶属部门");
		String department = scanner.next();
		System.out.println("请输入用户名");
		String username = scanner.next();
		System.out.println("请输入密码");
		String password = scanner.next();
		
		int index = -1;
		for (int i = 0; i < UserM.length; i++) {
			if(UserM[i][1]==null) {
				index = i;
				UserM[i][0] = department;
				UserM[i][1] = username;
				UserM[i][2] = password;
				UserM[i][3] = "普通员工";
				break;
			}
		}

			if(index!=-1) {
				System.out.println("恭喜");
			}else {
				System.out.println("...");
			}
			Index();
		}	
		
		
	private static void MessageContext() {
		//用户信息（部门、用户名、密码、用户角色）
		System.out.println("请输入你想查询的对象");
		int inquire = scanner.nextInt();
		for (int i = 0; i < UserM[inquire].length; i++) {
			System.out.println(Arrays.toString(UserM[inquire]));
		}
	}	
}
