package md.java.cyx;

import java.awt.print.Book;
import java.util.Scanner;

public class BookManagementSystem {
	
	static Scanner scanner = new Scanner(System.in);
	static String[][] users = new String[20][4];
	static String[][] books = new String[20][5];
	static String[][] pubs = new String[20][3];
	static String loginUser = "";
	
	public static void main(String[] args) {
		init();
		index();
	}
		
	private static void index() {
		System.out.println("*Welcome to the library management system*");
		System.out.println("\t 1.log in \t 2.Sign Up \t");
		
		while (true) {
			int key = scanner.nextInt();
			
			switch (key) {
			case 1:
				loginModule();
				break;
			case 2:
				registeredModule();
				break;
				default:
				break;
			}				
		}
	}
	
	private static void loginModule() {
		System.out.println("Please enter your UserName：");
		String userName = scanner.next();
		System.out.println("Please enter your PassWord：");
		String password = scanner.next();		
		
		boolean flag = false;
		for (int i = 0; i < users.length; i++) {
			if (userName.equals(users[i][1]) && password.equals(users[i][2])) {
				flag = true ; 
			}
			loginUser = userName;
		}
		if (flag) {
			System.out.println("Login successfully!!!");
			homePage();

		}else {
			System.out.println("Login failed, please reenter!!!");
		}
	}
	
	private static void registeredModule() {
		
		int index =-1;
		for (int i = 0; i < users.length; i++) {
			if (users[i][0]==null) {
				index = i ;
				break;
			}
		}
		
		System.out.println("Please enter your department：");
		String deptName = scanner.next();
		users[index][0]=deptName;
		System.out.println("Please enter your UserName：");
		String userName = scanner.next();
		users[index][1]=userName;
		System.out.println("Please enter your Password：");
		String password = scanner.next();		
		users[index][2]=password;			
		users[index][3]="Normal User";
		System.out.println("Registered Successfully!!!");

		index();
	}
	
	private	static void homePage() {
		System.out.println(loginUser+"\tWelcome to the library management system");
		System.out.println("Please enter the following numbers to select:");
		System.out.println("1. Library management  2.Press Management  3.Exit  4.Log out");
		
		int key2 = scanner.nextInt();
		
		switch (key2) {
		case 1:
			bookManagement();
			break;
		case 2:
			PressManagement();
			break;
		case 3:
			System.out.println(loginUser+"\t已退出登录,期待您下次使用");
		case 4:
			System.out.println("感谢您使用图书管理系统，再见！");
			System.exit(0);
			break;	
		default:
			System.out.println("输入错误请重新输入");
			break;
		}
	}
	
	private static void bookManagement() {
		System.out.println("Please enter the following numbers to select:");
		System.out.println("1. AddBooks 2.DeleteBooks 3.UpdateBooks 4.QueryBooks 5.Return to the previous menu");
		
		int key3 = scanner.nextInt();
		
		switch(key3) {
		case 1:
			AddBooks();
			break;
		case 2:
			DeleteBooks();
			break;
		case 3:
			UpdateBooks();
			break;					
		case 4:
			QueryBooks();
			break;	
		case 5:
			return;
		default:
			System.out.println("输入错误，请重新输入");
			break;
		}
	}

	private static void AddBooks() {
        String bookNum;
        while (true) {
            System.out.println("请输入编码（ISBN）：");
            bookNum = scanner.next();
            boolean exists = isBookExists(bookNum);
            if (exists) {
                System.out.println("您输入的编码已存在，请重新输入！");
            } else {
                break;
            }
        }
        System.out.println("请输入书籍名称：");
        String bookName = scanner.next();
        System.out.println("请输入书籍价格：");
        String bookPrice = scanner.next();
        System.out.println("请输入书籍出版社：");
        String bookPress = scanner.next();
        System.out.println("请输入书籍作者：");
        String bookWriter = scanner.next();
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] == null) {
                books[i][0] = bookNum;
                books[i][1] = bookName;
                books[i][2] = bookPrice;
                books[i][3] = bookPress;
                books[i][4] = bookWriter;
                System.out.println("添加书籍成功！");
                return;
            }
        }
        System.out.println("添加失败+_+");
    }

	private static void DeleteBooks() {
        if (isBookNull()){
            return;
        }
        String bookNum;
        while (true) {
            System.out.println("请输入您想删除的图书编码（ISBN）");
            bookNum = scanner.next();
            boolean isExists = isBookExists(bookNum);
            if (isExists) {
                break;
            } else {
                System.out.println("查无信息，请重新输入！");
            }
        }
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] != null && bookNum.equals(books[i][0])) {
                for (int j = 0; j < books[i].length; j++) {
                    books[i][j] = null;
                }
            }
        }
        System.out.println("删除成功！");
		
	}
	
	private static void UpdateBooks() {
		if (isBookNull()){
            return;
        }
        String bookNum;
        while (true) {
            System.out.println("请输入您更新的图书编码（ISBN）");
            bookNum = scanner.next();
            boolean isExists = isBookExists(bookNum);
            if (isExists) {
                break;
            } else {
                System.out.println("查无信息，请重新输入！");
            }
        }
        System.out.println("请输入书籍名称：");
        String bookName = scanner.next();
        System.out.println("请输入书籍价格：");
        String bookPrice = scanner.next();
        System.out.println("请输入书籍出版社：");
        String bookPress = scanner.next();
        System.out.println("请输入书籍作者：");
        String bookWriter = scanner.next();
        int index = -1;
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] != null && bookNum.equals(books[i][0])) {
                index = i;
                break;
            }
        }
        books[index][1] = bookName;
        books[index][2] = bookPrice;
        books[index][3] = bookPress;
        books[index][4] = bookWriter;
        System.out.println("修改成功！");
	}
	
	private static void QueryBooks() {
		System.out.println("1. QueryTheIsBN 2.InquiryOfTheTitleOfTheFuzzyQuery 3.EnquiryPress 4.QuerytheAuthor 5.InquiryPriceRange 6.QueryAll 7.Return to the previous menu");

        int key4 = scanner.nextInt();
        switch (key4) {
            case 1:
            	QueryTheIsBN();
                break;
            case 2:
            	InquiryOfTheTitleOfTheFuzzyQuery();
                break;
            case 3:
            	EnquiryPress();
                break;
            case 4:
            	QuerytheAuthor();
                break;
            case 5:
            	InquiryPriceRange();
                break;
            case 6:
            	QueryAll();
                break;
            case 7:
                return;
            default:
                System.out.println("输入错误，请重新输入！");
                break;
        }
    }

	private static void QueryTheIsBN() {
        String IsBN;
        System.out.println("请输入图书编号（ISBN）");
        IsBN = scanner.next();
        int index = -1;
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] != null && IsBN.equals(books[i][0])) {
                index = i;
                break;
            }
        }
			
	}
	
	private static void InquiryOfTheTitleOfTheFuzzyQuery() {
        String bookName;
        lo:
        while (true) {
            System.out.println("请输入图书名字包含的内容");
            bookName = scanner.next();
            for (int i = 0; i < books.length; i++) {
                if (books[i][1] != null && books[i][1].indexOf(bookName) != -1) {
                    break lo;
                }
            }
            System.out.println("未查询到相关信息，请重新输入");
        }
        System.out.println("编码（ISBN） \t 书籍名称 \t 价格 \t 出版社 \t 作者");
        for (int i = 0; i < books.length; i++) {
            if (books[i][1] != null && books[i][1].indexOf(bookName) != -1) {
                for (int j = 0; j < books[i].length; j++) {
                    System.out.print(books[i][j] + "\t");
                }
                System.out.println();
            }
        }
	}
		
	private static void EnquiryPress() {
	    String name;
	    lo:
	    while (true) {
	    System.out.println("请输入图书出版社名");
	    name = scanner.next();
	    for (int i = 0; i < books.length; i++) {
	    	if (books[i][3] != null && name.equals(books[i][3])) {
	    		break lo;
	           }
	    	}
	    	System.out.println("未查询到相关信息，请重新输入");
	    }

	    System.out.println("编码（ISBN） \t 书籍名称 \t 价格 \t 出版社 \t 作者");
	    for (int i = 0; i < books.length; i++) {
	    	if (books[i][3] != null && name.equals(books[i][3])) {
	    		for (int j = 0; j < books[i].length; j++) {
	    			System.out.print(books[i][j] + "\t");
	         }
	    		System.out.println();
	       }
	    }
		
	}
	
	private static void QuerytheAuthor() {
        String writerName;
        lo:
        while (true) {
            System.out.println("请输入图书作者名");
            writerName = scanner.next();
            for (int i = 0; i < books.length; i++) {
                if (books[i][4] != null && writerName.equals(books[i][4])) {
                    break lo;
                }
            }
            System.out.println("未查询到相关信息，请重新输入");
        }
        System.out.println("编码（ISBN） \t 书籍名称 \t 价格 \t 出版社 \t 作者");
        for (int i = 0; i < books.length; i++) {
            if (books[i][4] != null && writerName.equals(books[i][4])) {
                for (int j = 0; j < books[i].length; j++) {
                    System.out.print(books[i][j] + "\t");
                }
                System.out.println();
            }
        }
    }
	
	private static void InquiryPriceRange() {
        double min;
        double max;
        lo:
        while (true) {
            System.out.println("请输入最低价");
            min = scanner.nextDouble();
            System.out.println("请输入最高价");
            max = scanner.nextDouble();
            for (int i = 0; i < books.length; i++) {
                if (books[i][3] != null && Double.parseDouble(books[i][2]) >= min && Double.parseDouble(books[i][2]) <= max) {
                    break lo;
                }
            }
            System.out.println("未查询到相关信息，请重新输入");
        }
        System.out.println("编码（ISBN） \t 书籍名称 \t 价格 \t 出版社 \t 作者");
        for (int i = 0; i < books.length; i++) {
            if (books[i][3] != null && Double.parseDouble(books[i][2]) >= min && Double.parseDouble(books[i][2]) <= max) {
                for (int j = 0; j < books[i].length; j++) {
                    System.out.print(books[i][j] + "\t");
                }
                System.out.println();
            }
        }
		
	}
	private static void QueryAll() {
        System.out.println("编码（ISBN） \t 书籍名称 \t 价格 \t 出版社 \t 作者");

        for (int i = 0; i < books.length; i++) {
            for (int j = 0; j < books.length; j++) {
                if (books[i][j] != null) {
                    System.out.print(books[i][j] + "\t");
                    if(j==books.length-1){
                        System.out.println();
                    }
                }
            }
        }
				
	}
	
	private static boolean isBookNull() {
        boolean flag=false;
        lo:for (int i = 0; i < books.length; i++) {
            for (int j = 0; j < books.length; j++) {
                if (books[i][j]!=null){
                    flag=true;
                    break lo;
                }
            }
        }
        if (!flag){
            System.out.println("暂无数据，请添加后重试");
            return true;
        }
        return false;
	}

	private static boolean isBookExists(String bookNum) {
        boolean flag = false;
        for (int i = 0; i < books.length; i++) {
            if (books[i][0] != null && books[i][0].equals(bookNum)) {
                flag = true;
                break;
            }
        }
        return flag;
	}
	
	private static void PressManagement() {
        while (true) {
            System.out.println("欢迎使用<出版社>管理系统");
            System.out.println("请输入：\t 1.增加 \t 2.删除 \t 3.更新 \t 4.根据出版社名称查询 \t 5.查询所有出版社 \t 6.返回上级菜单");
            String choice = scanner.next();
            switch (choice) {
                case "1":
                    AddPressInfo();
                    break;
                case "2":
                    DeletePressInfo();
                    break;
                case "3":
                    UpdatePressInfo();
                    break;
                case "4":
                    GetPressInfoByName();
                    break;
                case "5":
                    QueryAllPressInfo();
                    break;
                case "6":
                    return;
                default:
                    System.out.println("输入错误，请重新输入");
                    break;
            }
        }
	}
	
	private static void AddPressInfo() {
		String pressName;
        while (true) {
            System.out.println("请输入出版社名称：");
            pressName = scanner.next();
            boolean exists = isPressExists(pressName);
            if (exists) {
                System.out.println("您输入的编码已存在，请重新输入！");
            } else {
                break;
            }
        }
        System.out.println("请输入出版社地址：");
        String pressAddress = scanner.next();
        System.out.println("请输入出版社联系人：");
        String pressBoss = scanner.next();
        for (int i = 0; i < pubs.length; i++) {
            if (pubs[i][0]==null){
                pubs[i][0]=pressName;
                pubs[i][1]=pressAddress;
                pubs[i][2]=pressBoss;
                System.out.println("添加成功！");
                return;
            }
        }
        System.out.println("添加失败+_+");
		
	}

	private static void DeletePressInfo() {
		 if (isPressNull()){
	            return;
	        }
	        String pressName;
	        while (true) {
	            System.out.println("请输入出版社名称：");
	            pressName = scanner.next();
	            boolean exists = isPressExists(pressName);
	            if (exists) {
	                break;
	            } else {
	                System.out.println("您输入的编码不存在，请重新输入！");
	            }
	        }
	        for (int i = 0; i < books.length; i++) {
	            if (books[i][3] != null && pressName.equals(books[i][3])) {
	                System.out.println("出版社有关联的图书禁止删除！");
	                return;
	            }
	        }
	        for (int i = 0; i < pubs.length; i++) {
	            if (pubs[i][0] != null && pressName.equals(pubs[i][0])) {
	                for (int j = 0; j < pubs[i].length; j++) {
	                	pubs[i][j]=null;
	                }
	            }
	        }
	        System.out.println("删除成功！");
		
	}

	private static void UpdatePressInfo() {
        if (isPressNull()){
            return;
        }
        String pressName;
        while (true) {
            System.out.println("请输入出版社名称：");
            pressName = scanner.next();
            boolean exists = isPressExists(pressName);
            if (exists) {
                break;
            } else {
                System.out.println("您输入的编码不存在，请重新输入！");
            }
        }
        System.out.println("请输入出版社地址：");
        String pressAddress = scanner.next();
        System.out.println("请输入出版社联系人：");
        String pressBoss = scanner.next();

        for (int i = 0; i < pubs.length; i++) {
            if (pubs[i][0]!=null&& pressName.equals(pubs[i][0])){
            	pubs[i][1]=pressAddress;
            	pubs[i][2]=pressBoss;
                System.out.println("修改成功！");
                return;
            }
        }
		
	}

	private static void GetPressInfoByName() {
        if (isPressNull()){
            return;
        }
        String pressName;
        while (true) {
            System.out.println("请输入出版社名称：");
            pressName = scanner.next();
            boolean exists = isPressExists(pressName);
            if (exists) {
                break;
            } else {
                System.out.println("您输入的编码不存在，请重新输入！");
            }
        }
		
	}

	private static void QueryAllPressInfo() {
		if (isPressNull()){
            return;
        }
        System.out.println("出版社名称\t地址\t联系人");
        for (int i = 0; i < pubs.length; i++) {
            for (int j = 0; j < pubs[i].length; j++) {
                if (pubs[i][j] != null) {
                    System.out.print(pubs[i][j] + "\t");
                    if(j==pubs[i].length-1){
                        System.out.println();
                    }
                }
            }
        }
		
	}

	private static boolean isPressExists(String pressName) {
	    boolean flag = false;
	      for (int i = 0; i < pubs.length; i++) {
	          if (pubs[i][0] != null && pubs[i][0].equals(pressName)) {
	              flag = true;
	              break;
	          }
	      }
	      return false;
	}
	
	private static boolean isPressNull() {
	    boolean flag=false;
	      lo:for (int i = 0; i < pubs.length; i++) {
	          for (int j = 0; j < pubs[i].length; j++) {
	              if (pubs[i][j]!=null){
	                  flag=true;
	                  break lo;
	           }
	          }
	       }
			return flag;
	}
	
	private static void init() {
		users[0][0] = "The ministry of software";
		users[0][1] = "admin";
		users[0][2] = "123";
		users[0][3] = "Administrator";
		
		users[1][0] = "Logistics Department";
		users[1][1] = "admin";
		users[1][2] = "123";
		users[1][3] = "Normal User";
		
		users[2][0] = "Administration department";
		users[2][1] = "admin";
		users[2][2] = "123";
		users[2][3] = "Administrator";
		
		users[2][0] = "Personnel department";
		users[2][1] = "admin";
		users[2][2] = "123";
		users[2][3] = "Administrator";
		
		books[0][0] = "011009";
		books[0][1] = "One Hundred Years of Solitude";
		books[0][2] = "$100.00";
		books[0][3] = "Little Genius Press";
		books[0][4] = "García Márquez";
		
		books[1][0] = "011010";
		books[1][1] = "The Old Man and the Sea";
		books[1][2] = "$1000.00";
		books[1][3] = "Little Genius Press";
		books[1][4] = "Ernest Miller Hemingway";
				
		books[2][0] = "011011";
		books[2][1] = "Robinson Crusoe";
		books[2][2] = "$10000.00";
		books[2][3] = "Little Genius Press";
		books[2][4] = "Daniel Defoe";
		
		books[3][0] = "011012";
		books[3][1] = "How the Steel was Tempered";
		books[3][2] = "$100000.00";
		books[3][3] = "Little Genius Press";
		books[3][4] = "Hиколай Алексеевич Островский";
		
		books[4][0] = "011013";
		books[4][1] = "The Wizard of OZ";
		books[4][2] = "$1000000.00";
		books[4][3] = "Little Genius Press";
		books[4][4] = "L. Frank Baum";
		
		books[5][0] = "011014";
		books[5][1] = "Harry Potter and the Sorcerer's Stone";
		books[5][2] = "$100000000.00";
		books[5][3] = "Little Genius Press";
		books[5][4] = "J.K. Rowling";
		
		pubs[0][0] = "Little Genius Press";
		pubs[0][1] = "Mars of the Universe";
		pubs[0][2] = "Little Genius";
		
		pubs[1][0] = "Little Genius Press";
		pubs[1][1] = "Mars of the Universe";
		pubs[1][2] = "Little Genius";

		pubs[2][0] = "Little Genius Press";
		pubs[2][1] = "Mars of the Universe";
		pubs[2][2] = "Little Genius";

		pubs[2][0] = "Little Genius Press";
		pubs[2][1] = "Mars of the Universe";
		pubs[2][2] = "Little Genius";
		
		pubs[3][0] = "Little Genius Press";
		pubs[3][1] = "Mars of the Universe";
		pubs[3][2] = "Little Genius";
		
		pubs[4][0] = "Little Genius Press";
		pubs[4][1] = "Mars of the Universe";
		pubs[4][2] = "Little Genius";
		
		pubs[5][0] = "Little Genius Press";
		pubs[5][1] = "Mars of the Universe";
		pubs[5][2] = "Little Genius";
		

		
	}

	
}
