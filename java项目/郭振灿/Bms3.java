package  BMS;
import java.util.ArrayList;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;

public class Bms {
	static String [][]user = new String [20][4];
	static Scanner scanner = new Scanner(System.in);
	static String [][]book = new String [100][5];
	static String [][]press = new String [100][3];
	static String loginuser = "";
	public static void main(String[] args) {
		
		init();
		indexpage();
		
	}
	private static void init() {
		//用户信息（部门、用户名、密码、用户角色）
		user [0][0] = "A部" ;
		user [0][1] = "yyds";
		user [0][2] = "wo20";
		user [0][3] = "管理员";
		
		user [1][0] = "B部" ;
		user [1][1] = "cpdd";
		user [1][2] = "wo880";
		user [1][3] = "普通用户";
		
		user [2][0] = "c部" ;
		user [2][1] = "lsp";
		user [2][2] = "wo770";
		user [2][3] = "VIP用户";
		
		//书籍信息（编码（ISBN）、书籍名称、价格、出版社、作者）
		book[0][0] = "9787540487645";
		book[0][1] = "云边有个小卖部";
		book[0][2] = "46.6";
		book[0][3] = "湖南文艺出版社";
		book[0][4] = "张嘉佳";
		
		book[1][0] = "9787506355957";
		book[1][1] = "活着";
		book[1][2] = "36.6";
		book[1][3] = "作家出版社";
		book[1][4] = "余华";
		
		book[2][0] = "9787507424065";
		book[2][1] = "唤醒心中的巨人";
		book[2][2] = "58.8";
		book[2][3] = "中国城市出版社";
		book[2][4] = "安东尼·罗宾华";
		
		//出版社信息（出版社名称、地址、联系人）
		press[0][0] = "湖南文艺出版社";
		press[0][1] = "湖南";
		press[0][2] = "阿刁";
		
		press[1][0] = "作家出版社";
		press[1][1] = "北京";
		press[1][2] = "阿肆";
		
		press[1][0] = "中国城市出版社";
		press[1][1] = "旅顺";
		press[1][2] = "阿逼";
	}


	private static void indexpage() {
		System.out.println("欢迎来到闽大图书管理系统！！！");
		System.out.println("1.登录  ; 2.注册  ");
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			login();
			break;

		case 2:
			register();
			break;
		}
	}

	private static void register() {
		System.out.println("请输入部门:");
		String departName = scanner.next();
		System.out.println("请输入用户名:");
		String userName = scanner.next();
		System.out.println("请输入密码:");
		String password = scanner.next();
		for (int i = 0; i < user.length; i++) {
			if (user[i][0] ==null) {
				user[i][0]=departName;
				user[i][1]=userName;
				user[i][2]=password;
				user[i][3]="普通用户";
			}
		}
		System.out.println("注册成功！");
		System.out.println("欢迎来到闽大图书管理系统！");
		System.out.println("1.登录  ; 2.注册  ");
		int key = scanner.nextInt();
		switch (key) {
		case 1:
			login();
			break;

		case 2:
			register();
		default:
			break;
		}
	}

	private static void login() {
		for (int i = 0; i < 3; i++) {
			System.out.println("请输入所属部门：");
			String userName = scanner.next();
			System.out.println("请输入密码:");
			String password = scanner.next();
			boolean flag = login(userName,password);
			if (flag) {
				System.out.println("登陆成功！");
				loginuser = userName;
				homepage();
				menu();
				break;
			}else {
				System.out.println("该用户不存在或者密码错误！请重新登录！");
				}
		 if (i==2) {
			 System.out.println("输入错误超过3次,系统自动退出");
		}
		}
		}
		

	private static void menu() {
		System.out.println("请输入数字进行选择：1 图书管理  2 出版社管理  3 退出登录  4 退出系统");
		
		int choose = scanner.nextInt();
		switch (choose) {
		case 1:
			// 图书管理
			bookmanagement();
			break;
		case 2:
			//出版社管理
			pressmanagement();
			break;
		case 3:
			//退出登录
			System.out.println("已退出登录");
			login();
			indexpage();
			break;
		case 4:
			//退出系统
			System.out.println("系统退出成功 !");
			System.exit(0);
			break;
			
		default:
			break;
		}
	}

	private static void pressmanagement() {
		while(true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
			int bb = scanner.nextInt();
			switch (bb) {
			case 1:
				//增加
				pressadd();
				break;
	        case 2:
				//删除
	        	pressdelete();
				break;
	        case 3:
		        //更新
	        	pressupdate();
		        break;
	        case 4:
	        	//根据出版社名称查询
	        	pressname();
	        	menu();
		        break;
	        case 5:
	        	//查询所有出版社
	        	pressquery();
	        	menu();
	        	break;
	        case 6:
	        	//返回上一级菜单
	        	homepage();
	        	menu();
		        break;
			default:
				break;
			}
		}
	}

	private static void pressquery() {
		System.out.println("出版社名称"+"\t"+"地址"+"\t"+"联系人");
		for (int i = 0; i < press.length; i++) {
			for (int j = 0; j < press[i].length; j++) {
				System.out.print(press[i][j]+"|");
			}
			System.out.println();
		}
		
	}

	private static void pressname() {
		System.out.println("请输入出版社名称：");
		String pressname = scanner.next();
		for (int i = 0; i < book.length; i++) {
			System.out.print("出版社名称："+press[i][0]);
			System.out.println();
			System.out.println("出版社地址 :"+ press[i][1]);
			System.out.println("出版社联系人："+press[i][2]);
		}
		
	}

	private static void pressupdate() {
		System.out.println("请输入要更新的出版社名称：");
		String pressname = scanner.next();
		int index = 0;
		for (int i = 0; i < press.length; i++) {
			if (pressname.equals(press[i][0])) {
				index = 1;
				System.out.println("请输入要更新的地址：");
				press[index][1] = scanner.next();
				System.out.println("请输入要更新的联系人：");
				press[index][2] = scanner.next();
				
				System.out.println("更新成功");		
			}
		}
		if (index == 0) {
			System.out.println("该出版社不存在！！！");
		}
	}

	private static void pressdelete() {
	System.out.println("请输入要删除的出版社名称：");
	String pressname = scanner.next();
		boolean a = false;
		for (int i = 0; i < press.length; i++) {
			if (pressname.equals(press[i][1])) {
				a=true;
				for (int j = 0; j < press[i].length; j++) {
					press[i][j]=null;
				}
			}
		}
		if(a == true) {
			System.out.println("删除成功!!!");
		}else {
			System.out.println("删除失败！！！");
		}
		pressmanagement();
	}

	private static void pressadd() {
		int index = -1;
		for (int i = 0; i < press.length; i++) {
			if (press[i][0]==null) {
				index = i;
				break;
			}
		}
		System.out.println("请输入出版社名称：");
		press[index][0] = scanner.next();
		System.out.println("请输入出版社地址：");
		press[index][1] = scanner.next();
		System.out.println("请输入出版社联系人：");
		press[index][2] = scanner.next();
		
		System.out.println("出版社添加成功");
	}

	private static void bookmanagement() {
		while(true) {
			System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
			int dd = scanner.nextInt();
			switch (dd) {
			case 1:
				//增加
				bookadd();
				break;
	        case 2:
				//删除
	        	bookdelete();
				break;
	        case 3:
		        //更新
	        	bookupdate();
		        break;
	        case 4:
	        	//查询
	        	System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
	        	int cc = scanner.nextInt();
	        	switch (cc) {
				case 1:
					//isbn
					bookisbn();
					
					break;
				case 2:
					//书名（模糊）
					bookname();
					break;
				case 3:
					//出版社
					bookpress();
					break;
				case 4:
					//作者
					bookauthor();
					break;
				case 5:
					//价格范围
					bookprice();
					break;
				case 6:
					//查询所有
					bookquery();
					break;
				case 7:
					//返回上一级
					bookmanagement();
					break;
				

				default:
					break;
				}
		        break;
	        case 5:
	        	//返回上一级菜单
	        	homepage();
	        	menu();
		        break;
			default:
				break;
			}
		}
	}

	private static void bookquery() {
		System.out.println("ISBN"+"\t"+"书名"+"\t"+"价格"+"\t"+"出版社"+"\t"+"作者");
		for (int i = 0; i < book.length; i++) {
			if (book[i][0]!=null) {
				for (int j = 0; j < book[i].length; j++) {
					System.out.print(book[i][j]+" ");
				}
				System.out.println();
			}
		}
		
	}

	private static void bookprice() {
		System.out.println("请输入最低价格:");
		String bookprice1 = scanner.next();
		System.out.println("请输入最高价格:");
		String bookprice2 = scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (bookprice1.equals(book[i][0])&& bookprice2.equals(book[i][0])) {
				System.out.print("ISBN"+"\t"+"书名"+"\t"+"价格"+"\t"+"出版社"+"\t"+"作者");
				System.out.println();
				System.out.print(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
				System.out.println();
			}
		}
	}

	private static void bookauthor() {
		System.out.println("请输入作者姓名：");
		String bookauthor = scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (bookauthor.equals(book[i][0])) {
				System.out.print("ISBN"+"\t"+"书名"+"\t"+"价格"+"\t"+"出版社"+"\t"+"作者");
				System.out.println();
				System.out.print(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
				System.out.println();
			}
		}
	}

	private static void bookpress() {
		System.out.println("请输入出版社名称：");
		String bookpress = scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (bookpress.equals(book[i][0])) {
				System.out.print("ISBN"+"\t"+"书名"+"\t"+"价格"+"\t"+"出版社"+"\t"+"作者");
				System.out.println();
				System.out.print(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
				System.out.println();
			}
		}
	}

	private static void bookname() {
		System.out.println("请输入书名关键字：");
		String bookname = scanner.next();
		ArrayList<Integer> arrayIndex = IndexName(bookname);
        if(arrayIndex.size() != 0){
            System.out.print("ISBN" + " \t" + "书名" + " \t" + "价格" + " \t" + "出版社" + " \t" + "作者");
            System.out.println();
            for (int i = 0; i < arrayIndex.size(); i++) {
                for (int j = 0; j < book[arrayIndex.get(i)].length; j++) {
                    System.out.print(book[arrayIndex.get(i)][j] + " \t");
                }
                System.out.println();
            }
        }else{
            System.out.print("ISBN" + " \t" + "书名" + " \t" + "价格" + " \t" + "出版社" + " \t" + "作者");
            System.out.println();
        }
	}

	private static ArrayList<Integer> IndexName(String bookname) {
		      ArrayList<Integer> arrayIndex = new ArrayList<>();
		      for (int i = 0; i < book.length; i++) {
		          if(book[i][1] != null && book[i][1].contains(bookname)){
		              arrayIndex.add(i);
		          }
		      }
		      return arrayIndex;
	}

	private static void bookisbn() {
		System.out.println("请输入ISBN号：");
		String bookisbn = scanner.next();
		for (int i = 0; i < book.length; i++) {
			if (bookisbn.equals(book[i][0])) {
				System.out.print("ISBN"+"\t"+"书名"+"\t"+"价格"+"\t"+"出版社"+"\t"+"作者");
				System.out.println();
				System.out.print(book[i][0]+"\t"+book[i][1]+"\t"+book[i][2]+"\t"+book[i][3]+"\t"+book[i][4]);
				System.out.println();
			}
		}
		
	}

	private static void bookupdate() {//图书更新
		System.out.println("请输入ISBN号:");
		String bookISBN = scanner.next();
		int index = 0;
		for (int i = 0; i < book.length; i++) {
			if (bookISBN.equals(book[i][0])) {
				index = 1;
				System.out.println("请输入新的书名：:");
				book[index][1] = scanner.next();
				System.out.println("请输入新的价格：:");
				book[index][2] = scanner.next();
				System.out.println("请输入新的出版社：");
				book[index][3] = scanner.next();
				System.out.println("请输入新的作者：");
				book[index][4] = scanner.next();
				
				System.out.println("更新成功！！！:");
			}
		}
		if (index == 0) {
			System.out.println("ISBN号不存在！！！");
		}
	}

	private static void bookdelete() {//图书删除
		System.out.println("请输入要删除的书本名称：");
		String bookname = scanner.next();
		boolean a = false;
		for (int i = 0; i < book.length; i++) {
			if (bookname.equals(book[i][1])) {
				a = true;
				for (int j = 0; j < book[i].length; j++) {
					book[i][j]=null;
			}
		 }
		}
			if (a == true) {
				System.out.println("删除成功！！！:");
			}else {
				System.out.println("删除失败！！！");
				}
			bookmanagement();
	}

	private static void bookadd() {//图书增加
		int index = -1;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0] == null) {
				index = i;
				break;
			}
		}
		System.out.println("请输入图书ISBN:");
		book[index][0] = scanner.next();
		System.out.println("请输入书名:");
		book[index][1] = scanner.next();
		System.out.println("请输入价格:");
		book[index][2]= scanner.next();
		System.out.println("请输入出版社:");
		book[index][3] = scanner.next();
		System.out.println("请输入作者:");
		book[index][4] = scanner.next();
		
		System.out.println("添加成功！！！:");
		
	}

	private static void homepage() {
		System.out.println(loginuser+",欢迎您使用闽大书籍管理系统！");
	}

	private static boolean login(String userName, String password) {
		Boolean flag = false;
		for (int i = 0; i < user.length; i++) {
			if (userName.equals(user[i][1])&&password.equals(user[i][2])) {
				flag = true;
			}
		}
		return flag;
	}
}
                        








