import java.util.Scanner;

public class Yhldca {
	static String user[][] = new String[10][4];
	static String book[][] = new String[10][5];
	static String press[][] = new String[10][3];
	static Scanner sc = new Scanner(System.in);
	private static int mon;
	public static void main(String[] args) {
		init();
		operation();
	}
	private static void operation() {
		System.out.println("欢迎来到闽大图书管理系统");
		System.out.println("1登录" + "\t" + "2注册" + "\t" + "3退出" + "\t");
		int chosee = sc.nextInt();
		switch (chosee) {
		case 1:
			login();
			break;
		case 2:
			register();
			break;
		case 3:
			quit();
			break;
		default:
			break;
		}
	}
	private static void quit() {
		// 退出系统
		System.out.println("退出成功！");
		System.out.println("欢迎您的下次光临！");
	}
	private static void register() {
		// 注册账号
		int index = -1;
		for (int i = 0; i < user.length; i++) {
			if (user[i][0] == null) {
				index = i;
				break;
			}
		}
		System.out.println("请输入所属部门：");
		user[index][0] = sc.next();
		System.out.println("请输入用户名：");
		user[index][1] = sc.next();
		System.out.println("请输入密码：");
		user[index][2] = sc.next();
		System.out.println("请输入声份：");
		user[index][3] = sc.next();
		System.out.println("注册成功！");
		System.out.println("请登录你的新账号：");
		operation();
	}
	private static void login() {
		// 登录账号
		System.out.println("请输入用户名：");
		String username = sc.next();
		System.out.println("请输入密码：");
		String password = sc.next();
		boolean flag = false;
		for (int i = 0; i < user.length; i++) {
			if (username.equals(user[i][1]) && password.equals(user[i][2])) {
				flag = true;
			}
		}
		if (flag) {
			System.out.println("登入成功！");
			homepage();
		} else {
			System.out.println("输入错误：");
			System.out.println("请重新输入：");
			login();
		}
	}
	private static void homepage() {
		// 系统菜单
		System.out.println("欢迎您使用闽大管理系统");
		System.out.println("请选择你的操作：1图书管理 2出版社管理 3退出登录 4退出系统");
		int KEY = sc.nextInt();
		switch (KEY) {
		case 1:
			// 图书管理
			manage();
			break;
		case 2:
			// 出版社管理
			PressManagement();
			break;
		case 3:
			System.out.println("退出登录成功！");
			operation();
			// 退出登录
			break;
		case 4:
			// 退出系统
			quit();
			break;
		default:
			break;
		}
	}
/**********************************出版社管理操作*****************************************/
	private static void PressManagement() {
		//出版社管理操作
		System.out.println("请输入：1.增加 2.删除 3.更新 4.根据出版社名称查询 5.查询所有出版社 6.返回上一级菜单");
		int key11 = sc.nextInt();
		switch (key11) {
		case 1:
			addpress();// 增加出版社
			break;
		case 2:
			droppress();// 删除出版社
			break;
		case 3:
			updatepress();// 更新出版社
			break;
		case 4:
			inquirepress();// 查询出版社
			break;
		case 5:
			System.out.println("\t" + "出版社名称" + "\t" + "地址" + "\t" + "联系人");
			for (int i = 0; i < press.length; i++) {
				if (press[i][0] != null) {
					System.out.println("\t" + press[i][0] + "\t" + press[i][1] + "\t" + press[i][2] + "\t");
				}
			}
			homepage();// 查询所有出版社
			break;
		case 6:
			homepage();// 返回菜单
		default:
			break;
		}
	}
private static void addpress() {
	// 增加出版社
	int index20 = -1;
	for (int i = 0; i < press.length; i++) {
		if (press[i][0] == null) {
			index20 = i;
			break;
		}
	}
	System.out.println("出版社名称:");
	press[index20][0] = sc.next();
	System.out.println("请输入地址:");
	press[index20][1] = sc.next();
	System.out.println("请输入联系人:");
	press[index20][2] = sc.next();

	System.out.println("添加成功：");
	homepage();
}
private static void droppress() {
	// 删除出版社
	System.out.println("请输入要删除的出版社名称：");
	String pressname22 = sc.next();
	for (int i = 0; i < press.length; i++) {
		if (pressname22!=null && pressname22.equals(book[i][3])) {
			System.out.println("该出版社有相关图书信息存在！不可以删除！如果要删除请先删除所有相关图书信息！！！");
			System.out.println("删除失败");
			break;
		}
	if (pressname22.equals(press[i][0])) {
		press[i][0] = null;
		press[i][1] = null;
		press[i][2] = null;
		System.out.println("删除成功");
		break;
	}
	}
	homepage();
}
private static void updatepress() {
	// 更新出版社
	System.out.println("请输入要更新的出版社名称：");
	String pressname01 =sc.next();
	System.out.println("\t" + "出版社名称" + "\t" + "地址" + "\t" + "联系人");
	for (int i = 0; i < press.length; i++) {
		if (pressname01.equals(press[i][0])) {
			System.out.println("\t" + press[i][0] + "\t" + press[i][1] + "\t" + press[i][2] + "\t");
		}
	}
	for (int i = 0; i < press.length; i++) {
		if (pressname01.equals(press[i][0])) {
			System.out.println("请输入要更新的地址：");
			press[i][1] =sc.next();
			System.out.println("请输入要更新的联系人姓名：");
			press[i][2] =sc.next();
			
			System.out.println("更新成功!");
			System.out.println("\t" + "出版社名称" + "\t" + "地址" + "\t" + "联系人");
			System.out.println("\t" + press[i][0] + "\t" + press[i][1] + "\t" + press[i][2] + "\t");
		}
	}
	homepage();
}
private static void inquirepress() {
	// 查询出版社
	System.out.println("请输入出版社名称：");
	String pressname =sc.next();
	int a =0;
	System.out.println("\t" + "出版社名称" + "\t" + "地址" + "\t" + "联系人");
	for (int i = 0; i < press.length; i++) {
		if (pressname.equals(press[i][0])) {
			a=1;
			System.out.println("\t" + press[i][0] + "\t" + press[i][1] + "\t" + press[i][2] + "\t");
		}
	}
	if(a==0){
		System.out.println("该出版社不存在！！！");
	}
	homepage();
}
/**********************************出版社管理操作*****************************************/
/**********************************图书管理操作*****************************************/
	private static void manage() {
		// 图书管理操作
		System.out.println("请输入：1.增加 2.删除 3.更新 4.查询 5.返回上一级菜单");
		int key = sc.nextInt();
		switch (key) {
		case 1:
			addbook();// 增加书籍
			break;
		case 2:
			dropbook();// 删除书籍
			break;
		case 3:
			updatebook();// 更新书籍
			break;
		case 4:
			inquire();// 查询书籍
			break;
		case 5:
			homepage();// 返回菜单
		default:
			break;
		}
	}
	private static void inquire() {
		System.out.println("请输入查询种类：1.isbn 2.书名（模糊） 3.出版社 4. 作者 5. 价格范围 6.查询所有 7.返回上一级");
		int key1 = sc.nextInt();
		switch (key1) {
		case 1:
			/* 查询isbn */abbr();
			break;
		case 2:
			/* 书名（模糊） */bookname();
			break;
		case 3:
			/* 查询出版社 */press();
			break;
		case 4:
			/* 查询作者 */author();
			break;
		case 5:
			/* 查询价格范围 */money();
			break;
		case 6:
			/* 查询所用 */ all();
			break;
		case 7:
			/* 返回上一级 */ manage();
			break;
		default:
			break;
		}
	}
	private static void abbr() {
		// 查询isbn
		System.out.println("请输入ISBN号：");
		String isbnn = sc.next();
		System.out.println("\t" + "isbn" + "\t" + "书名" + "\t" + "价格" + "\t" + "出版社" + "\t" + "作者");
		for (int i = 0; i < book.length; i++) {
			if (isbnn.equals(book[i][0])) {
				System.out.println("\t" + book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4]);
			}
		}
		inquire();
	}
	private static void bookname() {
		// 书名（模糊）
		System.out.println("请输入书名：");
		String bname = sc.next();
		System.out.println("\t" + "isbn" + "\t" + "书名" + "\t" + "价格" + "\t" + "出版社" + "\t" + "作者");
		for (int i = 0; i < book.length; i++) {
			if (book[i][1] != null && book[i][1].indexOf(bname) != -1) {
				System.out.println("\t" + book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4]);
			}
		}
		inquire();
	}
	private static void press() {
		// 查询出版社
		System.out.println("请输入出版社：");
		String ban = sc.next();
		System.out.println("\t" + "isbn" + "\t" + "书名" + "\t" + "价格" + "\t" + "出版社" + "\t" + "作者");
		for (int i = 0; i < book.length; i++) {
			if (ban.equals(book[i][3])) {
				System.out.println("\t" + book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4]);
			}
		}
		inquire();
	}
	private static void author() {
		// 查询作者
		System.out.println("请输入作者名：");
		String auu = sc.next();
		System.out.println("\t" + "isbn" + "\t" + "书名" + "\t" + "价格" + "\t" + "出版社" + "\t" + "作者");
		for (int i = 0; i < book.length; i++) {
			if (auu.equals(book[i][4])) {
				System.out.println("\t" + book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4]);
			}
		}
		inquire();
	}
	private static void money() {
		// 查询价格范围
		System.out.println("请输入最低价格：");
		int minmoney = sc.nextInt();
		System.out.println("请输入最高价格：");
		int maxmoney = sc.nextInt();
		for (int i = 0; i < book.length; i++) {
			if (book[i][2] != null) {
				int prees = Integer.parseInt(book[i][2]);
				if (prees < maxmoney && prees > minmoney) {
					System.out.println("\t" + book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3]
							+ "\t" + book[i][4]);
				}
			}
		}
		inquire();
	}
	private static void all() {
		// 查询所用
		System.out.println("\t" + "isbn" + "\t" + "书名" + "\t" + "价格" + "\t" + "出版社" + "\t" + "作者");
		for (int i = 0; i < book.length; i++) {
			if (book[i][0] != null) {
				System.out.println("\t" + book[i][0] + "\t" + book[i][1] + "\t" + book[i][2] + "\t" + book[i][3] + "\t"
						+ book[i][4]);
			}
		}
		inquire();
	}
	private static void updatebook() {
		// 更新书籍

		System.out.println("请输入ISBN号：");
		String xin = sc.next();
		for (int i = 0; i < book.length; i++) {
			if (xin.equals(book[i][0])) {
				System.out.println("请输入新图书ISBN:");
				book[i][0] = sc.next();
				System.out.println("请输入新书名:");
				book[i][1] = sc.next();
				System.out.println("请输入新价格:");
				book[i][2] = sc.next();
				System.out.println("请输入新出版社:");
				book[i][3] = sc.next();
				System.out.println("请输入新作者:");
				book[i][4] = sc.next();
				System.out.println("更新成功!");
			}
		}
		manage();
	}
	private static void dropbook() {
		// 删除书籍
		boolean flag = true ;
		System.out.println("请输入要删除的书本名称：");
		String bookname = sc.next();
		for (int i = 0; i < book.length; i++) {
			if (bookname.equals(book[i][1])) {
				book[i][0] = null;
				book[i][1] = null;
				book[i][2] = null;
				book[i][3] = null;
				book[i][4] = null;
				flag = false;
				System.out.println("删除成功");
				break;
			}
		}
		if (flag) {
			System.out.println("没有找到该书，删除失败！");
		}
		manage();
	}
	private static void addbook() {
		// 增加书籍
		int index2 = -1;
		for (int i = 0; i < book.length; i++) {
			if (book[i][0] == null) {
				index2 = i;
				break;
			}
		}
		System.out.println("请输入图书ISBN:");
		book[index2][0] = sc.next();
		System.out.println("请输入书名:");
		book[index2][1] = sc.next();
		System.out.println("请输入价格:");
		book[index2][2] = sc.next();
		System.out.println("请输入出版社:");
		book[index2][3] = sc.next();
		System.out.println("请输入作者:");
		book[index2][4] = sc.next();
		System.out.println("添加成功：");
		manage();
	}
/**********************************图书管理操作*****************************************/
	private static void init() {
		user[0][0] = "软件部门";
		user[0][1] = "admin";
		user[0][2] = "123";
		user[0][3] = "管理员";

		book[0][0] = "001";
		book[0][1] = "钢铁是怎样炼成的";
		book[0][2] = "37";
		book[0][3] = "新华出版社";
		book[0][4] = "尼古拉.阿列克谢耶维奇.奥斯特洛夫斯基";

		book[1][0] = "002";
		book[1][1] = "永夜君王";
		book[1][2] = "99";
		book[1][3] = "南方出版社";
		book[1][4] = "烟雨江南";

		book[2][0] = "003";
		book[2][1] = "平凡的世界";
		book[2][2] = "26";
		book[2][3] = "闽西出版社";
		book[2][4] = "路遥";

		book[3][0] = "004";
		book[3][1] = "角铭隽的生活";
		book[3][2] = "16";
		book[3][3] = "陕西出版社";
		book[3][4] = "阿军";

		book[4][0] = "005";
		book[4][1] = "鲁宾逊漂流记";
		book[4][2] = "46";
		book[4][3] = "英国皇家出版社";
		book[4][4] = "丹尼尔.笛福";
		
		press[0][0] = "新华出版社";
		press[0][1] = "北京大学斜对面";
		press[0][2] = "骄子";
		
		press[1][0] = "南方出版社";
		press[1][1] = "南开大学傍";
		press[1][2] = "明子";
		
		press[2][0] = "闽西出版社";
		press[2][1] = "闽西职业技术学院";
		press[2][2] = "骏子";
		
		press[3][0] = "陕西出版社";
		press[3][1] = "西安大学";
		press[3][2] = "李可心";
		
		press[4][0] = "英国皇家出版社";
		press[4][1] = "英国驻华大使馆边上";
		press[4][2] = "罗南.安德森";
	}
}
