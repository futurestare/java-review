package AJ;

import java.util.Scanner;

public class A {

	public static void main(String[] args) {
/*-   需求：在编程竞赛中，有6个评委为参赛的选手打分，分数为0-100的整数分。
选手的最后得分为：去掉一个最高分和一个最低分后 的4个评委平均值 (不考虑小数部分)。
		Scanner scanner = new Scanner(System.in);
		int []arr = new int [6];
		int max = arr[0];
		int min = 100;
		int sum = 0;
		
		for (int i = 0; i < arr.length; i++) {
			System.out.println("第"+(i+1)+"位评委的评分：");
			arr[i]=scanner.nextInt();
			if (arr[i]<0 || arr[i]>100 ) {
				System.out.println("输入错误！请重新输入！");
				i--;
			}
		}
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]>max) {
				max=arr[i];
			}
		}
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]<min) {
				min=arr[i];
			}
		}
		for (int i = 0; i < arr.length; i++) {
			sum=sum+arr[i];
		}
		System.out.println("最大值："+max);
		System.out.println("最小值："+min);
		System.out.println("总和："+sum);
		System.out.println("选手得分："+(sum-max-min)/4);
	}

}