package xuan;

import java.util.Scanner;

public class Z1 {
	public static void main(String[] args) {
		
//		-   需求：在编程竞赛中，有6个评委为参赛的选手打分，分数为0-100的整数分。
//        选手的最后得分为：去掉一个最高分和一个最低分后 的4个评委平均值 (不考虑小数部分)。
//
//-   思路：
//1.定义一个数组，用动态初始化完成数组元素的初始化，长度为6
//2.键盘录入评委分数
//3.由于是6个评委打分，所以，接收评委分数的操作，用循环
//4.求出数组最大值
//5.求出数组最小值
//6.求出数组总和
//7.按照计算规则进行计算得到平均分
//8.输出平均分
		int max=0;
		int min=100;
		int sum=0;
		int arr[] = new int [6]; 
		Scanner scan = new Scanner(System.in);
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请打分"+arr[i]);
			arr[i]=scan.nextInt();
			sum=sum+arr[i];
			if (max<arr[i]) {
				max=arr[i];
			}else if (min>arr[i]) {
				min=arr[i];
			}
			
		}
		System.out.println("和"+sum);
		System.out.println("最大"+max);
		System.out.println("最小"+min);
		System.out.println("平均"+(sum/6));
		
	}

}
