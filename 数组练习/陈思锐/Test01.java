package com.md.java1;

//在编程竞赛中，有6个评委为参赛的选手打分，分数为0-100的整数分。
//选手的最后得分为：去掉一个最高分和一个最低分后 的4个评委平均值 (不考虑小数部分)。


import java.util.Scanner;

public class Test01 {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		int[] arr = new int[6];
		
		//数组赋值
		for(int i = 0;i < arr.length;) {
			System.out.println("请第" + (i+1) + "个评委输入分数");
			arr[i] = s.nextInt();
			
			if(arr[i] > 100 || arr[i] < 0) {
				System.out.println("请重新输入");
				continue;
			}
			i++;
		}
		
		int max = arr[0];
		int min = arr[0];
		int maxindex = 0;
		int minindex = 0;
		int sum = 0;
		
		//遍历数组
		for(int i = 0;i < arr.length;i++) {
			System.out.print(arr[i] + "|");
		}
		System.out.println();
		
		//找最大最小，求和
		for(int i = 0;i < arr.length;i++) {
			if(arr[i] > max) {
				max = arr[i];
				maxindex = i;
			}
			if(arr[i] < min) {
				min = arr[i];
				minindex = i;
			}
			
			sum += arr[i];
		}
		
		
		double avg = (sum - arr[maxindex] - arr[minindex])/(double)(arr.length - 2);
		System.out.println("平均分是:" + avg);
		
	}

}
