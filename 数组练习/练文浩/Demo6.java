package Lesson1;
import java.util.Scanner;

public class Demo6 {
	public static void main(String[] args) {
		Scanner sca = new Scanner(System.in);
		int arr [] = new int [6];
		int max = arr[0];
		int min = arr[0];
		int sum = 0 ;
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入你的评分：");
			arr[i] = sca.nextInt();
			if(arr[i]>=0&&arr[i]<=100) {
				sum += arr[i];
			}else {
				System.out.println("你输入的分数超出，请重新输入！");
				i = (arr.length+1);
			}
			if(arr[i]>max) {
				max = arr[i];
			}
			if(arr[i]<min) {
				min = arr[i];
			}
		}
		System.out.println("最高分："+max+"最低分："+min+"总分："+sum);
		System.out.println("平均分："+(sum-max-min)/4);
	}
}