package LLL;

import java.util.Scanner;

public class KKK {
	public static void main(String[] args) {
		//思路：
	      //1.定义一个数组，用动态初始化完成数组元素的初始化，长度为6
	      //2.键盘录入评委分数
	      //3.由于是6个评委打分，所以，接收评委分数的操作，用循环
	      //4.求出数组最大值
	      //5.求出数组最小值
	     
	      //7.按照计算规则进行计算得到平均分
	      //8.输出平均分
		int arr []=new int[6];
		System.out.println("打分：");
		Scanner scan = new Scanner (System.in);
		for (int i = 0; i <6; i++) {
			System.out.println("第"+(i+1)+"位评委打分");
			arr[i]=scan.nextInt();
		}
		int max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]>max) {
				max=arr[i];
			}
		}
		System.out.println("最大"+max);
		int min = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]<min) {
				min=arr[i];
			}
		}
		System.out.println("最小："+min);
		 //6.求出数组总和
		int sum=0;
		for (int i = 0; i < arr.length; i++) {
			sum+=arr[i];
		}
		System.out.println("总和："+sum);
		//7.按照计算规则进行计算得到平均分
		System.out.println("平均分："+(sum-max-min)/4);
		
		
	}
}
