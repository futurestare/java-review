import java.util.Scanner;

public class Test{
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int [] a = new int [6];
        int i = -1; // default index

        while(true){
            if(i == (a.length - 1)) break; // break while loop
            System.out.println("请输入六个数:");
            int b = scanner.nextInt();
            if( b < 0 || b > 100){ // if not in, it loop
                System.out.println("请重新输入：");
            }else{
                ++i;
                a[i] = b; // add number
            }
        }
        int max = 0;
        int min = 100;
        float sum = 0;

        for(i = 0; i < a.length; i ++){
            if(max < a[i]) max = a[i]; // change max
            if(min > a[i]) min = a[i]; // change min
            sum += a[i]; // sum a[] number
        }
        float avg = sum / a.length; // change to float
        // final console out
        System.out.println("最大值:" + max);
        System.out.println("最小值:" + min);
        System.out.println("总和:" + sum);
        System.out.println("平均值:" + avg);
    }
}