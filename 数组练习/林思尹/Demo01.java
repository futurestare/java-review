package lesson01.md.cn;

	import java.util.Scanner;

	public class Demo01 {
	//   需求：在编程竞赛中，有6个评委为参赛的选手打分，分数为0-100的整数分。
//	    选手的最后得分为：去掉一个最高分和一个最低分后 的4个评委平均值 (不考虑小数部分)。
	//
	//-   思路：
	//1.定义一个数组，用动态初始化完成数组元素的初始化，长度为6
	//2.键盘录入评委分数
	//3.由于是6个评委打分，所以，接收评委分数的操作，用循环
	//4.求出数组最大值
	//5.求出数组最小值
	//6.求出数组总和
	//7.按照计算规则进行计算得到平均分
	//8.输出平均分
		public static void main(String[] args) {
			Scanner s = new Scanner(System.in);

			int[] arr = new int[6];
			
		
			for(int i = 0;i < arr.length;) {
				System.out.println("请第" + (i+1) + "个评委输入分数");
				arr[i] = s.nextInt();
				
				if(arr[i] > 100 || arr[i] < 0) {
					System.out.println("请重新输入");
					continue;
				}
				i++;
			}
			
			int max = arr[0];
			int min = arr[0];
			int maxindex = 0;
			int minindex = 0;
			int sum = 0;
			
		
			for(int i = 0;i < arr.length;i++) {
				System.out.print(arr[i] + "|");
			}
			System.out.println();
			
		
			for(int i = 0;i < arr.length;i++) {
				if(arr[i] > max) {
					max = arr[i];
					maxindex = i;
				}
				if(arr[i] < min) {
					min = arr[i];
					minindex = i;
				}
				
				sum += arr[i];
			}
			
			
			double avg = (sum - arr[maxindex] - arr[minindex])/(double)(arr.length - 2);
			System.out.println("平均分是:" + avg);
			
		}

	}