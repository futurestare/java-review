package ycccc;

import java.util.Scanner;

public class ArrayDemo {
	public static void main(String[] args) {
		Scanner scan =new Scanner(System.in);
		int[] num =new int[6];
		for (int i = 0; i < num.length; i++) {
			System.out.println("请第"+(i+1)+"个评委打分");
			num[i]=scan.nextInt();
		}
		System.out.println("评委打分结果：");
		for (int i : num) {
			System.out.print(i+"\t");
		}
		int max=num[0];
		int min=num[0];
		int sum=0;
		for (int i = 0; i < num.length; i++) {
			if (num[i]>max) {
				max=num[i];
			}
			if (num[i]<max) {
				min=num[i];
			}
			sum+=num[i];
		}
		System.out.println();
		System.out.println("最高分为："+max);
		System.out.println("最低分为："+min);
		System.out.println("总分为："+sum);
		System.out.println("平均分为"+(sum-max-min)/4);
	}
}
