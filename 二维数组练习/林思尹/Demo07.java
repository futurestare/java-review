package com.md.cn;

import java.util.Arrays;

public class Demo07 {
//	定义一个方法，用来实现如下功能：
//	​	已知一个数组 arr = {19, 28, 37, 46, 50}; 用程序实现把数组中的元素值交换，
//
//	​	交换后的数组 arr = {50, 46, 37, 28, 19}; 并在控制台输出交换后的数组元素
	public static void main(String[] args) {
		// TODO Auto-generated method stub
        int[] arr = {19, 28, 37, 46, 50};
        arr= temp(arr);
        System.out.println(Arrays.toString(arr));
	}
	public static int[] temp(int[] arr) {
		int temp ;
		for (int i = 0; i < arr.length/2; i++) {
			temp = arr[arr.length-i-1];
			arr[arr.length-i-1]	=arr[i];
			arr[i]=temp;
		}
	return arr;
	}
}
