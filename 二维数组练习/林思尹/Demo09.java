package com.md.cn;

import java.util.Scanner;

public class Demo09 {
//	接收并输出某公司本月的考勤和扣款信息， 假设公司有5个员工，
//	每个员工的考勤项有上下班忘打卡、 迟到、 早退、 旷工，
//	其中上下班忘打卡扣款10元/次， 迟到和早退扣款为20元/次， 旷工100元/天， 参考图如下(参考图中是以3个员工为例)： 
//

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[][] arr = new int[5][6];
		Scanner s = new Scanner(System.in);
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				if (j == 0) {
					System.out.println("请输入" + (i + 1) + "个员工的编号");
					arr[i][j] = s.nextInt();
				} else if (j == 1) {
					System.out.println("请输入" + (i + 1) + "个员工的忘打卡次数");
					arr[i][j] = s.nextInt();
				}

				else if (j == 2) {
					System.out.println("请输入" + (i + 1) + "个员工的迟到次数");
					arr[i][j] = s.nextInt();
				} else if (j == 3) {
					System.out.println("请输入" + (i + 1) + "个员工的早退次数");
					arr[i][j] = s.nextInt();
				} else if (j == 4) {
					System.out.println("请输入" + (i + 1) + "个员工的旷工次数");
					arr[i][j] = s.nextInt();
				} else {
					arr[i][j] = arr[i][1] * 10 + (arr[i][2] + arr[i][3] * 20 + arr[i][4] * 100);
				}

			}
		}
		System.out.println();
		System.out.println("********************本月考勤信息**********************************************");
		System.out.println("员工编号" + "\t" + "忘打卡" + "\t" + "迟到" + "\t" + "早退" + "\t" + "旷工" + "\t" + "总罚款（单位元）" + "\t");
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + "\t");
			}
			System.out.println();
		}
	}
}
