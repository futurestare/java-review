package com.md.lession2;

import java.util.Arrays;

public class Demo03 {

	public static void main(String[] args) {
	//定义一个方法，用来实现如下功能：
	//​已知一个数组 arr = {19, 28, 37, 46, 50}; 用程序实现把数组中的元素值交换，
	//交换后的数组 arr = {50, 46, 37, 28, 19}; 并在控制台输出交换后的数组元素
		int[] arr= {19, 28, 37, 46, 50};
		//System.out.println("交换前:");
		changeArray(arr);
	}
	public static void changeArray(int[] arr) {
		for (int i = 0; i < arr.length/2; i++) {
			int temp=arr[arr.length-1-i];
			arr[arr.length-1-i]=arr[i];
			arr[i]=temp;
		}
		System.out.println("交换后:"+Arrays.toString(arr));
	}
}
