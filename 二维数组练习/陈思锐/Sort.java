package com.md.review3;

import java.util.Arrays;

//冒泡排序
public class Sort {

    public static void main(String[] args) {
        int[] arr = { 4, 8, 7, 6, 1, 5, 10, 0 };

        //调用冒泡
        arr = sort(arr);
        System.out.print(Arrays.toString(arr));

        System.out.println();

        //调用选择
        int[] arr1 = selection(arr);
        System.out.print(Arrays.toString(arr1));
                
    }

    public static int[] sort(int[] arr) {
        int temp;
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] < arr[j + 1]) {
                    temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }

        return arr;
    }

    //选择排序
    public static int[] selection(int[] arr) {

        for (int i = 0; i < arr.length - 1; i++) {
            int max = i;

            for (int j = i + 1; j < arr.length; j++) {
                if (arr[max] < arr[j]) {
                    max = j;
                }
            }
            int temp;
            temp = arr[i];
            arr[i] = arr[max];
            arr[max] = temp;
        }

        return arr;
    }

}
