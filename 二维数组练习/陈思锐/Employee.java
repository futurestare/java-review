package com.md.review3;

import java.util.Scanner;

public class Employee {
	public static void main(String[] args) {
		int[][] arr = new int[5][6];
		
		Scanner s = new Scanner(System.in);
		
		for(int i = 0;i < arr.length;i++) {
			for(int j = 0;j < 6;j++) {
				if(j == 0) {
					System.out.println("请输入" + (i+1) + "个员工编号");	
					arr[i][j] = s.nextInt();		
				}else if(j == 1) {
					System.out.println("请输入" + (i+1) + "个员工的忘记打卡次数");
					arr[i][j] = s.nextInt();
				}else if(j == 2) {
					System.out.println("请输入" + (i+1) + "个员工的迟到次数");
					arr[i][j] = s.nextInt();
				}else if(j == 3) {
					System.out.println("请输入" + (i+1) + "个员工的早退次数");
					arr[i][j] = s.nextInt();
				}else if(j == 4){
					System.out.println("请输入" + (i+1) + "个员工的矿工次数");
					arr[i][j] = s.nextInt();
				}else{
					arr[i][j] = arr[i][1]*10 + (arr[i][2] + arr[i][3])*20 + arr[i][4]*100;
				}
			}
		}
		
		System.out.println();
		
		System.out.println("********************本月考勤信息*******************");
		
		System.out.println("员工编号 " + "\t" + "忘打卡 " + "\t" +"迟到 " + "\t" +"早退 " + "\t" +"矿工 " + "\t" + "总罚款（单位元） " + "\t");
		for(int i = 0;i < arr.length;i++) {
			for(int j = 0;j < arr[i].length;j++) {
				System.out.print(arr[i][j] + "\t");
			}
		System.out.println();
		}
	}
	
}
