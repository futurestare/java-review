package lessio;

import java.util.Arrays;
import java.util.Random;

public class Demo2 {

	public static void main(String[] args) {
		Random random = new Random();
			int	arr[] = new int [5];
        for (int i = 0; i < arr.length; i++) {
        	arr[i]= random.nextInt(100);
		}
        
        for (int i = 0; i < arr.length; i++) {
        	System.out.print(arr[i]+"|");
		}
        //System.out.println(Arrays.toString(arr));
        System.out.println(" ");
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = 0; j < arr.length - i - 1; j++) {
                if (arr[j] < arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
            System.out.println(Arrays.toString(arr));
        }
    }
}
