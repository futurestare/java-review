package DAY03;

import java.lang.reflect.Array;
import java.util.Arrays;
/*/
 * 用方法实现数组的排序。
定义一个函数，实现数组从大到小排序，传入一个乱序数组，返回一个从大到小排序的数组。
 */
public class DEMO01 {
	public static void main(String[] args) {
		int arr[] = new int [] {22,33,44,11,55};
		yin(arr);
		System.out.println(Arrays.toString(yin(arr)));
	}
	
	public static int[] yin(int arr[]) {
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				if (arr[j]<arr[j+1]) {
					int tmpt=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=tmpt;
				}
			}
			
		}
		
		return arr;
	}
}
