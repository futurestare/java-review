package Demo;

import java.util.Arrays;

//用方法实现数组的排序。
//定义一个函数，实现数组从大到小排序，传入一个乱序数组，返回一个从大到小排序的数组。
public class Demo01 {
	public static void main(String[] args) {
		int[] num = {56,78,34,100,99,85,456,12,98,66};
		System.out.println(Arrays.toString(changearr(num)));
	}
	public static int[] changearr(int[] arr) {
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				if (arr[j]<arr[j+1]) {
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1]=temp;
				}
			}
		}
		return arr;
	}
}
