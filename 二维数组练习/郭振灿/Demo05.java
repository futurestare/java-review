package Demo;

import java.util.Scanner;

public class Demo05 {
	public static void main(String[] args) {
		int [][] arr = new int[5][6];
		for (int i = 0; i < arr.length; i++) {
			Scanner scanner = new Scanner(System.in);
			System.out.println("请输入第"+(i+1)+"个员工的编号");
			arr[i][0]=scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的忘打卡次数");
			arr[i][1]=scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的迟到次数");
			arr[i][2]=scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的早退次数");
			arr[i][3]=scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的旷工次数");
			arr[i][4]=scanner.nextInt();
			arr[i][5]=(arr[i][1]*10+arr[i][2]*20+arr[i][3]*20+arr[i][4]*100);
		
		}
		System.out.println("*********************"+"本月考勤信息"+"*********************");
		System.out.print("员工编号"+"\t"+"忘打卡"+"\t"+"迟到"+"\t"+"早退"+"\t"+"旷工"+"\t"+"总罚款");
		for (int i = 0; i < arr.length; i++) {
			System.out.println();
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]+"\t");
			}
		}
	}
}