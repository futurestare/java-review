package java.dd;

import java.util.Arrays;
import java.util.Scanner;

public class Demo04 {

	public static void main(String[] args) {
		int arr[][]=new int [5][6];
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < 5; i++) {
			System.out.println("请输入第"+(i+1)+"个员工的编号");
			arr[i][0]=scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的上下班忘打卡次数");
			arr[i][1]=scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的迟到次数");
			arr[i][2]=scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的早退次数");
			arr[i][3]=scanner.nextInt();
			System.out.println("请输入第"+(i+1)+"个员工的旷工天数");
			arr[i][4]=scanner.nextInt();
			arr[i][5]=arr[i][1]*10+(arr[i][2]+arr[i][3])*20+arr[i][4]*100;
		}
		System.out.println("**********************本月考勤信息**************************");
		System.out.println("员工编号"+'\t'+"忘打卡"+'\t'+"迟到次数"+'\t'+"早退次数"+'\t'+"旷工天数"+'\t'+"总罚款（单位元）"+'\t');
		for (int i = 0; i < arr.length; i++) {
			System.out.println(""+arr[i][0]+'\t'+arr[i][1]+'\t'+arr[i][2]+'\t'+arr[i][3]+'\t'+arr[i][4]+'\t'+arr[i][5]);
		}
	}