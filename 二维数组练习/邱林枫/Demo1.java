package com.md.demo;

import java.util.Scanner;

public class Demo1 {
				/*
				 * 1、在主函数中从键盘接收X, Y , Z 3个数，
				 * 编写函数计算这3个数的立方和并返回计算结果：S=X3+Y3+Z3
				 */
				public static void num(Double X,Double Y, Double Z) {
					double a =X*X*X;
					double b =Y*Y*Y;
					double c =Z*Z*Z;
					System.out.println("立方和为："+(a+b+c));
				}
				public static void main(String[] args) {
					Scanner scanner = new Scanner(System.in);
					
					System.out.println("请输入X的值");
					double X = scanner.nextDouble(); 
					
					System.out.println("请输入Y的值");
					double Y = scanner.nextDouble(); 
					
					System.out.println("请输入Z的值");
					double Z = scanner.nextDouble(); 
					
					num(X, Y, Z);
				}
}
