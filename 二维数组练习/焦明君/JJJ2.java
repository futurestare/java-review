package JJJ;

import java.util.Scanner;

public class JJJ2 {

	public static void main(String[] args) {
		/*
		 * 接收并输出某公司本月的考勤和扣款信息， 假设公司有5个员工， 每个员工的考勤项有上下班忘打卡、 迟到、 早退、 旷工， 其中上下班忘打卡扣款10元/次，
		 * 迟到和早退扣款为20元/次， 旷工100元/天， 参考图如下(参考图中是以3个员工为例)：
		 */
		Scanner scanner = new Scanner(System.in);
		int[][] arr = new int[5][6];
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第" + (i + 1) + "个员工的编号");
			arr[i][0] = scanner.nextInt();
			System.out.println("请输入第" + (i + 1) + "个员工的忘打卡次数");
			arr[i][1] = scanner.nextInt();
			System.out.println("请输入第" + (i + 1) + "个员工的迟到次数");
			arr[i][2] = scanner.nextInt();
			System.out.println("请输入第" + (i + 1) + "个员工的早退次数");
			arr[i][3] = scanner.nextInt();
			System.out.println("请输入第" + (i + 1) + "个员工的旷工次数");
			arr[i][4] = scanner.nextInt();
			arr[i][5]=(arr[i][1]*10)+(arr[i][2]*20)+(arr[i][3]*20)+(arr[i][4]*100);

		}
		System.out.println("*********本月考勤信息*********");
		System.out.println("员工编号" + "\t" + "忘打卡" + "\t" + "迟到" + "\t" + "早退" + "\t"+ "旷工" + "\t" + "总罚款（单位元）");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i][0]+"\t"+arr[i][1]+"\t"+arr[i][2]+"\t"+arr[i][3]+"\t"
					+arr[i][4]+"\t"+arr[i][5]);
		}
	}

}
