package GG.kk.LBW;

import java.util.Arrays;
import java.util.Scanner;

public class Demo04 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner (System.in);
		int arr[][] = new int [3][6];
		for (int i = 0; i < arr.length; i++) {
				System.out.println("请输入第"+(i+1)+"个职员的编号：");
				arr[i][0] = scanner.nextInt();
				System.out.println("请输入第"+(i+1)+"个员工忘打卡次数：");
				arr[i][1] = scanner.nextInt();
				System.out.println("请输入第"+(i+1)+"个员工迟到次数：");
				arr[i][2] = scanner.nextInt();
				System.out.println("请输入第"+(i+1)+"个员工早退次数：");
				arr[i][3] = scanner.nextInt();
				System.out.println("请输入第"+(i+1)+"个员工旷工次数：");
				arr[i][4] = scanner.nextInt();
				System.out.println("请输入第"+(i+1)+"个员工总罚款：");
				arr[i][5]= scanner.nextInt();
		}
		System.out.println("**********本月考勤**********");
		System.out.println("员工编号"+'\t'+"忘打卡次数"+'\t'+"迟到次数"+'\t'+"早退次数"+'\t'+"旷工次数"+'\t'+"总罚款");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(""+arr[i][0]+'\t'+arr[i][1]+'\t'+arr[i][2]+'\t'+arr[i][3]+'\t'+arr[i][4]+'\t'+arr[i][5]);
		}
	}
}