package GG.kk.LBW;

import java.util.Arrays;
import java.util.Scanner;
/*
用方法实现数组的排序。
定义一个函数，实现数组从大到小排序，传入一个乱序数组，返回一个从大到小排序的数组。
*/
public class Demo01 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arr [] = new int [5];
		for (int i = 0; i < arr.length; i++) {
			System.out.println("第"+(i+1)+"的值为：");
			arr[i] = scanner.nextInt();
		}
		get(arr);
		System.out.println(Arrays.toString(arr));
	}
	public static void get (int arr[]) {
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				if (arr[j]<arr[j+1]) {
					int a = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = a ;
				}
			}
		}
	}
}