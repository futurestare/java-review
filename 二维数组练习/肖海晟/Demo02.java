package GG.kk.LBW;

import java.util.Arrays;
/*
定义一个方法，用来实现如下功能：
​已知一个数组 arr = {19, 28, 37, 46, 50}; 用程序实现把数组中的元素值交换，
​交换后的数组 arr = {50, 46, 37, 28, 19}; 并在控制台输出交换后的数组元素
*/
public class Demo02 {
	public static void main(String[] args) {
		int arr[] = {19,28,37,46,50};
		get(arr);
		System.out.println(Arrays.toString(arr));
	}
	public static void get (int arr[]) {
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				int a =arr[j];
				arr[j]=arr[j+1];
				arr[j+1]=a;
			}
		}
	}
}