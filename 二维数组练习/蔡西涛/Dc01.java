package Djl;

import java.util.Arrays;

public class Dc01 {
	public static void main(String[] args) {
		//用方法实现数组的排序。
		//定义一个函数，实现数组从大到小排序，传入一个乱序数组，返回一个从大到小排序的数组。
		int [] a= {60,91,64,24,70};
		System.out.println(Arrays.toString(array(a)));
	}
	public static int [] array(int[] a) {
		for (int i = 0; i < a.length-1; i++) {
			for (int j = 0; j < a.length-1-i; j++) {
				if (a[j]<a[j+1]) {
					int temp=a[j];
					a[j]=a[j+1];
					a[j+1]=temp;
				}
			}
		}
		return a;
	}
}
