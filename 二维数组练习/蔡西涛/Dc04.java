package Djl;

public class Dc04 {
//已知两个整数变量a = 10，b = 20，使用程序实现这两个变量的数据交换
//	​        最终输出a = 20，b = 10;
	public static void main(String[] args) {
		int a=10;
		int b=20;
		System.out.println("交换前为"+a+"\t"+b);
		int temp=a;
		a=b;
		b=temp;
		System.out.println("交换后为"+a+"\t"+b);
	}
}
