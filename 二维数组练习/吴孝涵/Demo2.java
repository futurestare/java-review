package com.day3.demo;

import java.util.Arrays;

public class Demo2 {
	public static void main(String[] args) {
		/*定义一个方法，用来实现如下功能：
		​	已知一个数组 arr = {19, 28, 37, 46, 50}; 用程序实现把数组中的元素值交换，

		​	交换后的数组 arr = {50, 46, 37, 28, 19}; 并在控制台输出交换后的数组元素*/
		int arr[]= {19, 28, 37, 46, 50};
		
		for (int i = 0; i < arr.length; i++) {
			
			int start=i;
			int end=arr.length-1-i;
			
			int temp = arr[start];
			arr[start]=arr[end];
			arr[end]=temp;
			
			if (arr.length%2 != 0 && start==end) {
				break;
			}/*else if(arr.length%2 == 0 && start==end){
				temp = arr[start];
				arr[start]=arr[end];
				arr[end]=temp;
				break;
			}*/
		}
		System.out.println(Arrays.toString(arr));
	}
}
