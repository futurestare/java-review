package com.day3.demo;

import java.util.Arrays;

public class Demo1 {
	public static void main(String[] args) {
		int[] arr = {12,20,3,5,15};
		int[] newArr=GetArray(arr);
		
		System.out.println(Arrays.toString(newArr));
	}
	public static int[] GetArray(int []arr) {

		for (int i = 0; i < arr.length-1; i++) {
			for (int j = 0; j < arr.length-1-i; j++) {
				if (arr[j]<arr[j+1]) {
					int temp=arr[j+1];
					arr[j+1]=arr[j];
					arr[j]=temp;
				}
			}
		}
		return arr;
	}
}
