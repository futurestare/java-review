package com.day.demo;

import java.util.Scanner;

public class Demo1 {
	private static int[][] arr = new int[5][5];
    private static String[] ids = new String[5];
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        setArr();
        GetArr(ids,arr);
    }

    private static void setArr() { lo:
    for (int i = 0; i < arr.length; i++) {
        while (true) {
            System.out.println("请输入员工编号");
            String id = sc.next();
            boolean exists = isExists(id);
            if (exists) {
                System.out.println("您输入的员工编号已存在，请重新输入！");
            } else {
                ids[i] = id;
                break;
            }
        }
        System.out.println("请输入员工忘记打卡次数");
        int dk = sc.nextInt();
        arr[i][0] = dk;

        System.out.println("请输入员工迟到次数");
        int cd = sc.nextInt();
        arr[i][1] = cd;

        System.out.println("请输入员工早退次数");
        int zt = sc.nextInt();
        arr[i][2] = zt;

        System.out.println("请输入员工旷工次数");
        int kg = sc.nextInt();
        arr[i][3] = kg;

        int lost = (dk * 10) + (cd * 20) + (zt * 20) + (kg * 100);
        arr[i][4] = lost;

        loz:
        while (true) {
            System.out.println("继续录入数据吗? 输入数字1将继续 其它任意内容停止 ");
            String choice = sc.next();
            switch (choice) {
                case "1":
                    break loz;
                default:
                    break lo;
            }
        }
    }
    }

    private static void GetArr(String[] ids, int[][] arr){
        System.out.println("编号\t忘记打卡\t迟到\t早退\t旷工\t总罚款");
        for (int i = 0; i < arr.length; i++) {
            if (ids[i] == null) {
                break;
            }
            System.out.print(ids[i] + "\t\t");
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + "\t");
            }
            System.out.println("\n");
        }
    }

    private static boolean isExists(String id) {
        boolean exists = false;
        for (int i = 0; i < ids.length; i++) {
            if (ids[i] != null && ids[i].equals(id)) {
                exists = true;
                break;
            }
        }
        return exists;
    }
}
