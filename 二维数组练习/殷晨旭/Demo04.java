package Lession1;

import java.util.Scanner;

public class Demo04 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arr [][] = new int [3][6];
		for (int i = 0; i < arr.length; i++) {
				System.out.println("请输入第"+(i+1)+"个员工的编号");
				arr[i][0] = scanner.nextInt();
				System.out.println("请输入该员工忘打卡的次数：");
				arr[i][1] = scanner.nextInt();
				System.out.println("请输入该员工迟到的次数：");
				arr[i][2] = scanner.nextInt();
				System.out.println("请输入该员工早退的次数：");
				arr[i][3] = scanner.nextInt();
				System.out.println("请输入该员工旷工的次数：");
				arr[i][4] = scanner.nextInt();
				arr[i][5] = (arr[i][1]*10)+((arr[i][2]+arr[i][3])*20)+(arr[i][4]*100);
		}
		System.out.println("******************本月考勤******************");
		System.out.println("员工编号"+'\t'+"忘打卡"+'\t'+"迟到"+'\t'+"早退"+'\t'+"旷工"+'\t'+"总罚款");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(""+arr[i][0]+'\t'+arr[i][1]+'\t'+arr[i][2]+'\t'+arr[i][3]+'\t'+arr[i][4]+'\t'+arr[i][5]);
		}
	}
}
