package yccccc;
/*
 * - 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值

	- 注意: return语句, 只能带回一个结果.

	- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
 */
public class Demo3 {
	public static void main(String[] args) {
		int[] arr= {13,25,67,46,87,56};
		int[] MaxandMin=getMaxandMin(arr);
		System.out.println("最小值是："+MaxandMin[0]);
		System.out.println("最大值是："+MaxandMin[1]);
	}
	public static int[] getMaxandMin(int[] arr) {
		int max = arr[0];
		int min = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if(max < arr[i]){
                max = arr[i];
            }
            if(min > arr[i]){
                min = arr[i];
            }
        }
        int[] maxAndMin = {min, max};

        return maxAndMin;
	}
}
