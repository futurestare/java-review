package com.day2.demo;

import java.util.Scanner;

public class Demo3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		int[] arr=new int[5];
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入数组中的第"+(i+1)+"个数");
			arr[i]=sc.nextInt();
		}
		int[] arr1=getMaxMin(arr);
		System.out.println("数组中最大值为"+arr1[0]+",数组中最小值为"+arr1[1]);
	}

	public static int[] getMaxMin(int[] arr) {
		// TODO Auto-generated method stub
		int[] arr1=new int[2];
		int max=arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]>max) {
				max=arr[i];
			}
		}
		int min=arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]<min) {
				min=arr[i];
			}
		}
		arr1[0]=max;
		arr1[1]=min;
		return arr1;
	}

}
