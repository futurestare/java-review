package java复习;
import java.util.Scanner;
public class 数组最大值 {
	public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	int[] arr=new int[5];
	for (int i = 0; i < arr.length; i++) {
		System.out.println("请输入数组中的第"+(i+1)+"个数");
		arr[i]=sc.nextInt();
	}
	int max =findMax(arr);
	System.out.println("数组中最大的值为"+max);
}

public static int findMax(int[] arr) {
	int max =arr[0];
	for (int i = 0; i < arr.length; i++) {
		if (arr[i]>max) {
			max=arr[i];
		}
	}
	return max;
}
	
	
	
}
//* 需求：设计一个方法用于获取数组中元素的最大值 
//
//* 思路：
//
//  * ①定义一个数组，用静态初始化完成数组元素初始化
//  * ②定义一个方法，用来获取数组中的最大值，最值的认知和讲解我们在数组中已经讲解过了
//  * ③调用获取最大值方法，用变量接收返回结果
//  * ④把结果输出在控制台