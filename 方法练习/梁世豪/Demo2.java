package ASD;

import java.util.Scanner;

public class Demo2 {


	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int[] arr=new int[5];
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入数组中的第"+(i+1)+"个数");
			arr[i]=sc.nextInt();
		}
		int max =findMax(arr);
		System.out.println("数组中最大的值为"+max);
	}

	public static int findMax(int[] arr) {
		int max =arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]>max) {
				max=arr[i];
			}
		}
		return max;
	}
}
