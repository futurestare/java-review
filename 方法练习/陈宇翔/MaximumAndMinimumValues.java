package CyX;

public class MaximumAndMinimumValues {
		public static void main(String[] args) {
        int[] arr = {1,11,111,1111,11111};

        int[] getMaxAndMinValues = getMaxAndMinValues(arr);

        System.out.println("该数组中最大的数是:"+getMaxAndMinValues[0]);
        System.out.println("该数组中最小的数是:"+getMaxAndMinValues[1]);

    }

		public static int[] getMaxAndMinValues(int[] arr) {
			int max = arr[0];
			for (int i = 1; i < arr.length; i++) {
				if (max < arr[i]) {
					max = arr[i];
				}
			}

			int min = arr[0];
			for (int i = 1; i < arr.length; i++) {
				if (min > arr[i]) {
					min = arr[i];
				}
			}
	
			int[] getMaxAndMinValues = {max, min};
	
			return getMaxAndMinValues;
	}


}

