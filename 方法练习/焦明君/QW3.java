package QW;

public class QW3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值

		- 注意: return语句, 只能带回一个结果.

		- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值*/
		int [] arr= {11,52,23,45,63};
	
		int max=arr1(arr);
		int min=arr1(arr);
	}
	public static int arr1(int[]arr) {
		int max;
		max=arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (arr[i]>max) {
				max=arr[i];
			}
		}
		System.out.println("数组中的最大值为"+max);
		int min;
		min=arr[0];
		for (int i = 0; i < arr.length; i++) {
			
			if (arr[i]<min) {
				min=arr[i];
			}
		}
		System.out.println("数组中的最小值为"+min);
		return 0;
	}
	
}
