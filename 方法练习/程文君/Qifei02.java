package Qifei1;

public class Qifei02 {
/*
 * - 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值

- 注意: return语句, 只能带回一个结果.

- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a= {29,32,43,46,76};
		int[] sum=name(a);
		int b=sum[0];
		int c=sum[1];
		System.out.println("最大"+b);
		System.out.println("最小"+c);
	}
	public static int[] name(int[] a) {
		int max= a[0];
		int min=a[0];
		for (int i = 0; i < a.length; i++) {
			if (max<a[i]) {
				max = a[i];
			}
			if (min>a[i]) {
				min=a[i];
			}
		}
		int sum[]= {max,min};
		return sum;
	}
}
