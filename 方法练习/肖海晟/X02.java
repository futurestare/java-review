package md.af;

import java.util.Scanner;
/*
* 需求：设计一个方法用于获取数组中元素的最大值 
* 思路：
* ①定义一个数组，用静态初始化完成数组元素初始化
* ②定义一个方法，用来获取数组中的最大值，最值的认知和讲解我们在数组中已经讲解过了
* ③调用获取最大值方法，用变量接收返回结果
* ④把结果输出在控制台
*/
public class X02 {
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner (System.in);
		int arr[] = new int [5];
		for (int i = 0; i < arr.length; i++) {
			System.out.println("请输入第"+(i+1)+"个数的值：");
			arr[i] = scanner.nextInt();
		}
		int a = arr[0];
		int b = arr[0];
		LBW(arr,a,b);
	}
	public static void LBW (int arr[],int a,int b) {
		for (int i = 0; i < arr.length; i++) {
			if (a>arr[i]) {
				a=arr[i];
			}else if (b<arr[i]) {
				b=arr[i];	
			}
		}
		System.out.println("最小值为："+a);	
		System.out.println("最大值为："+b);
	}
}
