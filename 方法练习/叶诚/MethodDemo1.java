package ycccc;

import java.sql.Array;
import java.util.Arrays;

//需求：设计一个方法用于数组遍历，
//要求遍历的结果是在一行上的。例如：[11, 22, 33, 44, 55] 
public class MethodDemo1 {
	public static void main(String[] args) {
		int[] arr= {11, 22, 33, 44, 55};
		System.out.println(Arrays.toString(arr));
	}
	public static void num(int[] arr) {
		for (int i = 0; i < arr.length; i++) {
			if (i==arr.length-1) {
				System.out.print(arr[i]);}
				else {
					System.out.print(arr[i]+",");
				}
			}
	}
}
