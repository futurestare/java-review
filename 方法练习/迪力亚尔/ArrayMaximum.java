package CyX;

public class ArrayMaximum {
	public static void main(String[] args) {
		
		int []arr = {1,11,111,1111,11111}; 
		int number = max(arr);
		System.out.println(number);

	}
	
	public static int max(int[] arr) {
		
		int max = arr[0];
		
		for (int i = 0; i < arr.length; i++) {
			if ( arr[i] > max ) {
				max = arr[i];
			}
		}
		return max;
	}
	
}
