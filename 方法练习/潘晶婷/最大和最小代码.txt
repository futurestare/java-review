package Demo;

public class D01 {
		public static void main(String[] args) {
			//- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值

			//- 注意: return语句, 只能带回一个结果.

			//- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值
			int [] arr= {10,20,30,40,50};
			int [] arr1=num(arr);
			System.out.print("["+arr1[0]+","+arr1[1]+"]");
		}
		public static int[] num(int [] arr) {
			int max=arr[0];
			int min=arr[0];
			
			for (int i = 0; i < arr.length; i++) {
				if (arr[i]>max) {
					max=arr[i];
				}
			}
			
			for (int i = 0; i < arr.length; i++) {
				if (arr[i]<min) {
					min=arr[i];
				}
			}
			int arr1[]= {max,min};
			return arr1;
		}
	
}

