package com.md.review2;

import java.util.Random;

//如果一个数组保存元素是有序的（从大到小），向这个数组中插入一个数，使得插入后的数组元素仍然保持有序。

public class Method5 {

	public static void main(String[] args) {
		int[] arr = {7,6,5,4,3,1};
		
		int[] newArr = newArr(arr);
		
		for(int i = 0;i < newArr.length;i++) {
			System.out.print(newArr[i] + "|");
		}

	}
	
	public static int[] newArr(int[] arr) {
		
		Random r = new Random(); 
		
		int newNum = r.nextInt(10);
		
		int[] newArr = new int[arr.length + 1];
		
		for(int i = 0;i < newArr.length;i++) {
			if(i < arr.length) {
				newArr[i] = arr[i];
			}else {
				newArr[i] = newNum;
			}
		}
		

		for(int i = newArr.length - 1;i > 0;i--) {
			for(int j = 0;j < i;j++) {
				if(newArr[j] < newArr[j + 1]) {
					int temp;
					temp = newArr[j];
					newArr[j] = newArr[j + 1];
					newArr[j + 1] = temp;
				}
			}
		}
		
		return newArr;
	}

}
