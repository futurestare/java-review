package com.md.review2;

//- 需求：设计一个方法，该方法能够同时获取数组的最大值，和最小值
//
//- 注意: return语句, 只能带回一个结果.
//
//- 提示:返回一个数组，数组里第一个元素是最大值，第二个元素是最小值

public class Method3 {

	public static void main(String[] args) {
		int[] arr = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };

		int[] arr2 = getMaxMin(arr);

		System.out.println("最大值是：" + arr2[0] + "  " + "最小值是：" + arr2[1]);

	}

	public static int[] getMaxMin(int[] arr) {

		int max = arr[0];
		int min = arr[0];

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > max) {
				max = arr[i];
			}
			if (arr[i] < min) {
				min = arr[i];
			}
		}
		
		int[] arr2 = {max,min};

		return arr2;
	}

}
