package student;

import java.util.Scanner;

public class demo02 {

		/*
		 * * 需求：设计一个方法用于获取数组中元素的最大值 

	* 思路：

	  * ①定义一个数组，用静态初始化完成数组元素初始化
	  * ②定义一个方法，用来获取数组中的最大值，最值的认知和讲解我们在数组中已经讲解过了
	  * ③调用获取最大值方法，用变量 接收返回结果
	  * ④把结果输出在控制台
		 */
		public static void main(String[] args) {
			int[] a =new int[6];
			Scanner scanner =new Scanner(System.in);
			for (int i = 0; i < a.length; i++) {
				System.out.println("请输入"+i+"个数");
				a[i]=scanner.nextInt();
			}
			int d =isgex(a);
			System.out.println(d);
			
			
		}
		public static int isgex(int[] arr) {
			int max=arr[0];
			for (int i = 0; i < arr.length; i++) {
				if (arr[i]>max) {
					max=arr[i];
				}
				
			}
			
			return max;

	}

}
