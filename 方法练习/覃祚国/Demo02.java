package sdgljn.md.com;

public class Demo02 {

	public static void main(String[] args) {
		int a [] = {10,20,30,40};
		name(a);
		int b = name(a);
		System.out.println(b);
	}
	public static int name(int arr[]) {
		int max = arr[0];
		for (int i = 0; i < arr.length; i++) {
			if (max<arr[i]) {
				max=arr[i];
			}
		}
		return max;
	}
	/**
	 * * 需求：设计一个方法用于获取数组中元素的最大值 
	* 思路：
	  * ①定义一个数组，用静态初始化完成数组元素初始化
	  * ②定义一个方法，用来获取数组中的最大值，最值的认知和讲解我们在数组中已经讲解过了
	  * ③调用获取最大值方法，用变量接收返回结果
	  * ④把结果输出在控制台
	 */
}
